﻿using UnityEngine;
using UnityEngine.Events;

namespace DPPoolObject
{
	// This component will automatically reset a Rigidbody when it gets spawned/despawned
	public class DPPoolPoolable : MonoBehaviour
	{
		// Called when this poolable object is spawned
		public UnityEvent OnSpawn;

		// Called when this poolable object is despawned
		public UnityEvent OnDespawn;
	}
}