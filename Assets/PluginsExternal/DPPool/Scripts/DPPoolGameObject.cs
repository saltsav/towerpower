﻿using UnityEngine;
using System.Collections.Generic;

namespace DPPoolObject {
    // This component allows you to pool GameObjects, allowing for a very fast instantiate/destroy alternative
    public class DPPoolGameObject : MonoBehaviour {
        [System.Serializable]
        public class Clone {
            public GameObject GameObject;
            public Transform Transform;
            public DPPoolPoolable Poolable;
        }

        [System.Serializable]
        public class Delay {
            public GameObject GameObject;
            public float Life;
        }

        // All activle and enabled pools in the scene
        public static List<DPPoolGameObject> Instances = new List<DPPoolGameObject>();

        [Tooltip("The prefab this pool controls")]
        public GameObject Prefab;

        [Tooltip("Should this pool preload some clones?")]
        public int Preload;

        [Tooltip("Should this pool have a maximum amount of spawnable clones?")]
        public int Capacity;

        [Tooltip("If the pool reaches capacity, should new spawns force older ones to despawn?")]
        public bool Recycle;

        [Tooltip("Should this pool be marked as DontDestroyOnLoad?")]
        public bool Persist;

        [Tooltip("Should the spawned cloned have the clone index appended to their name?")]
        public bool Stamp;

        // All the currently spawned prefab instances
        [SerializeField]
        private List<Clone> spawnedClones;

        // All the currently despawned prefab instances
        [SerializeField]
        private List<Clone> despawnedClones;


        // Find the pool responsible for handling the specified prefab
        public static DPPoolGameObject FindPoolByPrefab(GameObject prefab) {
            for (var i = Instances.Count - 1; i >= 0; i--) {
                var pool = Instances[i];

                if (pool.Prefab == prefab) {
                    return pool;
                }
            }

            return null;
        }

        public static DPPoolGameObject FindPoolByClone(GameObject gameObject) {
            for (var i = Instances.Count - 1; i >= 0; i--) {
                var pool = Instances[i];

                if (pool.spawnedClones != null) {
                    for (var j = pool.spawnedClones.Count - 1; j >= 0; j--) {
                        var clone = pool.spawnedClones[j];

                        if (clone.GameObject == gameObject) {
                            return pool;
                        }
                    }
                }
            }

            return null;
        }

        // Returns the amount of spawned clones
        public int Spawned {
            get {
                return spawnedClones != null ? spawnedClones.Count : 0;
            }
        }

        // Returns the amount of despawned clones
        public int Despawned {
            get {
                return despawnedClones != null ? despawnedClones.Count : 0;
            }
        }

        // Returns the total amount of spawned and despawned clones
        public int Total {
            get {
                return Spawned + Despawned;
            }
        }

        // This will either spawn a previously despanwed/preloaded clone, recycle one, create a new one, or return null
        public GameObject Spawn(Vector3 position, Quaternion rotation, Transform parent = null) {
            if (Prefab != null) {
                // Spawn a previously despanwed/preloaded clone?
                if (despawnedClones != null) {
                    // Loop through all despawnedClones until one is found
                    while (despawnedClones.Count > 0) {
                        var index = despawnedClones.Count - 1;
                        var clone = despawnedClones[index];

                        despawnedClones.RemoveAt(index);

                        if (clone.GameObject != null) {
                            SpawnClone(clone, position, rotation, parent);

                            return clone.GameObject;
                        }
                    }
                }

                // Make a new clone?
                if (Capacity <= 0 || Total < Capacity) {
                    var clone = CreateClone(position, rotation, parent);

                    // Add clone to spawned list
                    if (spawnedClones == null) {
                        spawnedClones = new List<Clone>();
                    }

                    spawnedClones.Add(clone);

                    return clone.GameObject;
                }

                // Recycle?
                if (Recycle == true && spawnedClones != null) {
                    // Loop through all spawnedClones from the front (oldest) until one is found
                    while (spawnedClones.Count > 0) {
                        var clone = spawnedClones[0];

                        spawnedClones.RemoveAt(0);

                        if (clone != null) {
                            clone.GameObject.SetActive(false);

                            SpawnClone(clone, position, rotation, parent);

                            return clone.GameObject;
                        }
                    }
                }
            }

            return null;
        }

        // This allows you to access the spawned clone at the specified index
        public GameObject GetSpawned(int index) {
            return index >= 0 && index < spawnedClones.Count ? spawnedClones[index].GameObject : null;
        }

        // This allows you to access the despawned clone at the specified index
        public GameObject GetDespawned(int index) {
            return index >= 0 && index < despawnedClones.Count ? despawnedClones[index].GameObject : null;
        }

        [ContextMenu("Despawn All")]
        public void DespawnAll() {
            for (var i = spawnedClones.Count - 1; i >= 0; i--) {
                DespawnNow(spawnedClones[i].GameObject);
            }
        }

        // This will either instantly despawn the specified gameObject, or delay despawn it after t seconds
        public void Despawn(GameObject cloneGameObject) {
            if (cloneGameObject != null) {
                DespawnNow(cloneGameObject);
            }
        }

        // This method will create an additional prefab clone and add it to the despawned list
        [ContextMenu("Preload One More")]
        public void PreloadOneMore() {
            if (Prefab != null) {
                // Create clone
                var clone = CreateClone(Vector3.zero, Quaternion.identity, null);

                // Add clone to despawned list
                if (despawnedClones == null) {
                    despawnedClones = new List<Clone>();
                }

                despawnedClones.Add(clone);

                // Deactivate it
                clone.GameObject.SetActive(false);

                // Move it under this GO
                clone.Transform.SetParent(transform, false);
            }
        }

        // This will preload the pool until the 
        [ContextMenu("Preload All")]
        public void PreloadAll() {
            if (Preload > 0) {
                if (Prefab != null) {
                    for (var i = Total; i < Preload; i++) {
                        PreloadOneMore();
                    }
                }
            }
        }

        protected virtual void Awake() {

            PreloadAll();

            if (Persist == true) {
                transform.SetParent(null);
                DontDestroyOnLoad(this);
            }
        }

        protected virtual void OnEnable() {
            Instances.Add(this);
        }

        protected virtual void OnDisable() {
            Instances.Remove(this);
        }

        private void DespawnNow(GameObject cloneGameObject) {
            // Find the clone associated with this gameObject
            if (spawnedClones != null) {
                for (var i = spawnedClones.Count - 1; i >= 0; i--) {
                    var clone = spawnedClones[i];

                    if (clone.GameObject == cloneGameObject) {
                        // Remove clone from spawned list
                        spawnedClones.RemoveAt(i);

                        // Add clone to despawned list
                        if (despawnedClones == null) {
                            despawnedClones = new List<Clone>();
                        }

                        despawnedClones.Add(clone);

                        // Move it under this GO
                        clone.Transform.SetParent(transform, false);

                        // Deactivate it
                        clone.GameObject.SetActive(false);

                        return;
                    }
                }
            }
        }

        private Clone CreateClone(Vector3 position, Quaternion rotation, Transform parent) {
            var clone = new Clone();

            clone.GameObject = (GameObject) Instantiate(Prefab, position, rotation);
            clone.Transform = clone.GameObject.transform;

            if (Stamp == true) {
                clone.GameObject.name = Prefab.name + " " + Total;
            } else {
                clone.GameObject.name = Prefab.name;
            }

            clone.Transform.SetParent(parent, false);
            return clone;
        }

        private void SpawnClone(Clone clone, Vector3 position, Quaternion rotation, Transform parent) {
            // Add clone to spawned list
            if (spawnedClones == null) {
                spawnedClones = new List<Clone>();
            }

            spawnedClones.Add(clone);

            // Update transform of clone
            var cloneTransform = clone.Transform;

            cloneTransform.localPosition = position;
            cloneTransform.localRotation = rotation;

            cloneTransform.SetParent(parent, false);

            // Activate clone
            clone.GameObject.SetActive(true);
        }

        private static DPPoolPoolable GetOrCachePoolable(Clone clone) {
            var poolable = clone.Poolable;

            if (clone.Poolable == null) {
                poolable = clone.Poolable = clone.GameObject.GetComponent<DPPoolPoolable>();
            }

            return poolable;
        }
    }
}