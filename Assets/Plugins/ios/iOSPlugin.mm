#import "iOSPlugin.h"

@implementation iOSPlugin

// Открытие браузера
- (void)openBrowserNative:(NSString *)url {
    
    NSURLRequest *request = [NSURLRequest requestWithURL: [NSURL URLWithString:url]];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIViewController* viewController = window.rootViewController;
    //UIView *topView = window.rootViewController.view;
    
    
    SFSafariViewController *safafi = [[SFSafariViewController alloc] initWithURL:request.URL];
    safafi.delegate = self;
    
    safafi.modalPresentationStyle = UIModalPresentationFormSheet;
    safafi.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [viewController presentViewController:safafi animated:true completion:nil];
}

// Удаление сафари
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController dismissViewControllerAnimated:true completion:nil];
}
@end

static iOSPlugin* plugin = nil;

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

extern "C" {
    
    void OpenBrowserNative (const char* url) {
        
        if(plugin == NULL) plugin = [[iOSPlugin alloc] init];
        [plugin openBrowserNative:CreateNSString(url)];
    }
}
