#import <Foundation/Foundation.h>
#import <SafariServices/SafariServices.h>


@interface iOSPlugin : NSObject <SFSafariViewControllerDelegate>{
    
}

- (void)openBrowserNative:(NSString *)url;

@end
