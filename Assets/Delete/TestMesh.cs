﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMesh : MonoBehaviour {

	private void OnValidate() {
		Start();
	}

	void Start() {
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		Vector3[] vertices = mesh.vertices;

		Debug.Log(mesh.GetTriangles(0).Length);
		Debug.Log(mesh.GetTriangles(1).Length);
		Debug.Log(mesh.GetTriangles(2).Length);
		Debug.Log(mesh.GetTriangles(3).Length);

		// create new colors array where the colors will be created.
		Color[] colors = new Color[vertices.Length];
		Debug.Log(vertices.Length);
		for (int i = 0; i < vertices.Length; i++) {
			colors[i] = Color.Lerp(Color.red, Color.green, vertices[i].y);
			Debug.Log(i);
		}
		// assign the array of colors to the Mesh.
		mesh.colors = colors;
	}
}
