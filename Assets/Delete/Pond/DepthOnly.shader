﻿Shader "Custom/DepthOnly"
{
    Properties
    {
        // Specular vs Metallic workflow
        [HideInInspector] _WorkflowMode("WorkflowMode", Float) = 1.0

         _Color("Color", Color) = (0.5,0.5,0.5,1)
        [HideInInspector] _MainTex("Albedo", 2D) = "white" {}

        [HideInInspector] _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        [HideInInspector] _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
        [HideInInspector] _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        [HideInInspector] _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0

        [PerRendererData] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        [HideInInspector]  _MetallicGlossMap("Metallic", 2D) = "white" {}

        [HideInInspector] _SpecColor("Specular", Color) = (0.2, 0.2, 0.2)
        [HideInInspector] _SpecGlossMap("Specular", 2D) = "white" {}

        [ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
        [HideInInspector] _GlossyReflections("Glossy Reflections", Float) = 0.0

        [HideInInspector] _BumpScale("Scale", Float) = 1.0
        [HideInInspector] _BumpMap("Normal Map", 2D) = "bump" {}

        [HideInInspector] _OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
        [HideInInspector] _OcclusionMap("Occlusion", 2D) = "white" {}

        [HideInInspector] _EmissionColor("Color", Color) = (0,0,0)
        [HideInInspector] _EmissionMap("Emission", 2D) = "white" {}

         [ToggleOff] _ReceiveShadows("Receive Shadows", Float) = 1.0
    }

    SubShader
    {
        // Lightweight Pipeline tag is required. If Lightweight render pipeline is not set in the graphics settings
        // this Subshader will fail. One can add a subshader below or fallback to Standard built-in to make this
        // material work with both Lightweight Render Pipeline and Builtin Unity Pipeline
        Tags{"RenderType" = "Geometry" "RenderPipeline" = "LightweightPipeline" "IgnoreProjector" = "True"}
        LOD 300

        Pass
        {
            Name "DepthOnly"
            Tags{"LightMode" = "DepthOnly"}

			ZTest Always
            ZWrite On

            HLSLPROGRAM

            #pragma vertex DepthOnlyVertex
            #pragma fragment DepthOnlyFragment

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #include "Assets/Materials/Universal/Shaders/LitInput.hlsl"
            #include "Assets/Materials/Universal/Shaders/DepthOnlyPass.hlsl"
            ENDHLSL
        }

    }
}
