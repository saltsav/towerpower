﻿
Shader "Custom/GammaShader" 
	{
	Properties 
	{
		_MainTex ("Diffuse (RGBA)", 2D) = "white"{}
		_DissolveValue ("Value", Range(0,2)) = 1.0
	}
	SubShader 
	{
		Tags {"Queue" = "Transparent"  "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" } // "RenderType"="Opaque"
		
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off 
		Lighting Off 
		ZWrite Off

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull back
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			sampler2D _MainTex;
			fixed _DissolveValue;
			
			struct vIN
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};
			
			struct vOUT
			{
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};
			
			vOUT vert(vIN v)
			{
				vOUT o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				o.color = v.color;
				return o;
			}

			half3 AdjustContrast(half3 color, half contrast) {
				return saturate(lerp(half3(0.5, 0.5, 0.5), color, contrast));
			}

			half3 AdjustContrastCurve(half3 color, half contrast) {
				return pow(abs(color * 2 - 1), 1 / max(contrast, 0.0001)) * sign(color - 0.5) + 0.5;
			}

			
			fixed4 frag(vOUT i) : COLOR
			{
				fixed4 mainTex = tex2D(_MainTex, i.uv);
				half3 newColor = mainTex.rgb * i.color;
				newColor = AdjustContrast(newColor, _DissolveValue);

				float3 intensity = dot(newColor.rgb, float3(0.299, 0.587, 0.114));
                newColor.rgb = lerp(intensity, newColor.rgb, _DissolveValue);

				if(mainTex.r  < 0.4) {
					newColor.rgb *= 0.95;
				} else
				if(mainTex.r > 0.8) {
					newColor.rgb *= 1.15;
				} else
				if(mainTex.r > 0.6) {
					newColor.rgb *= 1.07;
				}  

				mainTex.rgb = newColor.rgb;
				mainTex.a *= i.color.a;

				return mainTex;
			}
			
			ENDCG
		}
	} 
}