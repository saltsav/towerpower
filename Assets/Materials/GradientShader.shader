﻿
Shader "Custom/SpriteGradient" {

	Properties {
	    [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
	    _Color2 ("End Color", Color) = (1,1,1,1)
	    _Color ("Start Color", Color) = (1,1,1,1)
	    _Scale ("Scale", Float) = 1
	     
	 	// These six unused properties are required when a shader is used in the UI system, or you get a warning.
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
		_ColorMask ("Color Mask", Float) = 15
	}
	  
	SubShader {
	    Tags {"Queue"="Background"  "IgnoreProjector"="True"}
	    LOD 100
	    ZWrite On
	  
	    Pass {
	        CGPROGRAM
	        #pragma vertex vert  
	        #pragma fragment frag
	        #include "UnityCG.cginc"
	  
	        fixed4 _Color2;
	        fixed4 _Color;
	        fixed  _Scale;
	  
	        struct v2f {
	            float4 pos : SV_POSITION;
	            fixed4 col : COLOR;
	        };
	  
	        v2f vert (appdata_full v){
	            v2f o;
	            o.pos = UnityObjectToClipPos (v.vertex);
	            o.col = lerp(_Color2,_Color, v.texcoord.y );
				//o.col = half4( v.vertex.y, 0, 0, 1);
	            return o;
	        }
	        
	        float4 frag (v2f i) : COLOR {
	            float4 c = i.col;
	            c.a = 1;
	            return c;
	        }
	         
	        ENDCG
	    }
	}
}