﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class MaterialBlock {
    public Color ColorShader;
    [Range(0, 1)] public float Metallic;
}

[ExecuteInEditMode]
public class MaterialPropertyBlock : MonoBehaviour {

    [SerializeField] Material UniversalMaterial;
    [SerializeField] Material UniversalTextureMaterial;

    [SerializeField] MaterialBlock[] Materials;

    // При изменении
    private void OnValidate() {
        SetColor();
    }

    // Стартовая настройка
    private void Start() {
        SetColor();
    }

    // Установка цветов
    void SetColor() {
        Renderer _renderer = GetComponent<Renderer>();
        if (_renderer != null) {

#if UNITY_EDITOR
            if (_renderer.sharedMaterials[0] != UniversalMaterial && _renderer.sharedMaterials[0] != UniversalTextureMaterial) {
                Materials = new MaterialBlock[_renderer.sharedMaterials.Length];
                Material[] sharedMaterialsCopy = _renderer.sharedMaterials;
                for (int i = 0; i < _renderer.sharedMaterials.Length; i++) {
                    Materials[i] = new MaterialBlock();
                    Materials[i].ColorShader = _renderer.sharedMaterials[i].GetColor("_Color");
                    Materials[i].Metallic = _renderer.sharedMaterials[i].GetFloat("_Metallic");
                    sharedMaterialsCopy[i] = UniversalMaterial;
                }
                _renderer.sharedMaterials = sharedMaterialsCopy;
            }
#endif

            UnityEngine.MaterialPropertyBlock _propBlock;
            for (int i = 0; i < Materials.Length; i++) {
                _propBlock = new UnityEngine.MaterialPropertyBlock();
                _renderer.GetPropertyBlock(_propBlock, i);
                _propBlock.SetColor("_Color", Materials[i].ColorShader);
                _propBlock.SetFloat("_Metallic", Materials[i].Metallic);
                _renderer.SetPropertyBlock(_propBlock, i);
            }
        }
    }
}