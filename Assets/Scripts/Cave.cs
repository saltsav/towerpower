﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cave : MonoBehaviour
{
    public float minScale;
    public float maxScale;

    [EasyButtons.Button]
    public void SetRandomSizeAndAngle() {
        HelpsSizeCube[] helpsSizeCubeArr = transform.GetComponentsInChildren<HelpsSizeCube>();
        foreach (HelpsSizeCube helpsSizeCube in helpsSizeCubeArr) {
            helpsSizeCube.SetRandomSizeAndAngle(minScale, maxScale);
        }
    }
}
