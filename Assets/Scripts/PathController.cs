﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class PathController : MonoSingleton<PathController> {
        public float minScaleRocks;
        public float maxScaleRocks;
        public Transform pointSpawnEnemy;
        public Transform pointDespawnEnemy;

        public List<RoadOnePart> listRoadOnePart = new List<RoadOnePart>();

        public float disBeetwinRoads;

        public void Setup() {
            for (int i = 0; i < listRoadOnePart.Count; i++) {
                listRoadOnePart[i].SetRandomSizeAndAngle(minScaleRocks, maxScaleRocks);
                listRoadOnePart[i].index = i;
            }

            disBeetwinRoads = Vector3.Distance(listRoadOnePart[0].transform.position, listRoadOnePart[1].transform.position);
        }

        public void AddRoadOnePart(RoadOnePart newRoadOnePart) {
            newRoadOnePart.index = listRoadOnePart.Count;
            listRoadOnePart.Add(newRoadOnePart);
            
        }

        public RoadOnePart GetRoadOnePartByIndex(int var) {
            foreach (RoadOnePart roadOnePart in listRoadOnePart) {
                if (roadOnePart.index == var) {
                    return roadOnePart;
                }
            }

            return null;
        }
    }
}