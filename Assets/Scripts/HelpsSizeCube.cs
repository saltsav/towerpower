﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpsSizeCube : MonoBehaviour {


    [EasyButtons.Button]
    public void SetRandomSizeAndAngle(float minScale, float maxScale) {
        transform.localEulerAngles = new Vector3(Random.Range(0, 360),Random.Range(0, 360),Random.Range(0, 360));
        transform.localScale = Vector3.one * Random.Range(minScale, maxScale);

    }
}
