﻿using System.Collections.Generic;
using Assets.Scripts;
using DG.Tweening;
using EasyButtons;
using UnityEngine;

public class Tower : MonoBehaviour {
    public Transform pointShot;
    public float strength;
    public Bullet bulletPrefab;

    public List<Enemy> nearestEnemy;

    [ReadOnly]public float timeForNextShot;

    public void Start() {
        timeForNextShot = 0;
        TowerController.Instance.towerList.Add(this);
    }

    [Button]
    public void Test() {
        transform.DOShakeScale(0.5f, strength);
    }

    public void Shot() {
        Bullet b = Instantiate(bulletPrefab);
        b.transform.position = pointShot.position;
        b.enemyTarget = nearestEnemy[0];
    }

    public void Update() {
        if (nearestEnemy.Count == 0) {
            return;
        }

        if (nearestEnemy[0] == null) {
            UpdateEnemyList();
            return;
        }

        timeForNextShot -= Time.deltaTime;
        if (timeForNextShot < 0) {
            timeForNextShot = MainLogic.Instance.gs.speedAttack;
            Shot();
        }
    }

    public void UpdateEnemyList() {
        bool flag = true;

        while (true) {
            flag = true;
            for (int i = 0; i < nearestEnemy.Count; i++) {
                if (nearestEnemy[i] == null) {
                    nearestEnemy.RemoveAt(i);
                    flag = false;
                }
            }

            if (flag) {
                break;
            }
        }
    }

    public void AddOrRemoveEnemy(Enemy enemy, bool isAdd) {
        if (isAdd) {
            nearestEnemy.Add(enemy);
        } else {
            for (int i = 0; i < nearestEnemy.Count; i++) {
                if (nearestEnemy[i].index == enemy.index) {
                    nearestEnemy.RemoveAt(i);
                }
            }
        }

        UpdateEnemyList();
    }

    public void OnTriggerEnter(Collider collider) {
        Enemy enemy = collider.transform.parent.gameObject.GetComponent<Enemy>();
        if (enemy != null) {
            //Debug.Log("OnTriggerEnter " + collider.gameObject.name);
            AddOrRemoveEnemy(enemy, true);
        }
    }


    public void OnTriggerExit(Collider collider) {
        Enemy enemy = collider.transform.parent.gameObject.GetComponent<Enemy>();
        if (enemy != null) {
            //Debug.Log("OnTriggerExit " + collider.gameObject.name);
            AddOrRemoveEnemy(enemy, false);
        }
    }
}