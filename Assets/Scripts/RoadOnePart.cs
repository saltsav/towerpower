﻿using Assets.Scripts;
using EasyButtons;
using UnityEngine;

public class RoadOnePart : MonoBehaviour {
    public int index;

    public void SetRandomSizeAndAngle(float minScale, float maxScale) {
        HelpsSizeCube[] helpsSizeCubeArr = transform.GetComponentsInChildren<HelpsSizeCube>();
        foreach (HelpsSizeCube helpsSizeCube in helpsSizeCubeArr) {
            helpsSizeCube.SetRandomSizeAndAngle(minScale, maxScale);
        }
    }
}