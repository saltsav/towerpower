﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class EnemyController : MonoSingleton<EnemyController> {
        public Enemy prefabEnemy;
        public int maxCount;
        public int currentCount;

        public float speed;

        public List<Enemy> listEnemy = new List<Enemy>();

        public Transform parentEnemy;

        [ReadOnly] public int index;

        public Transform test;

        public void Setup() {
            StartCoroutine(IEnumSpawnEnemy());
        }

        public IEnumerator IEnumSpawnEnemy() {
            currentCount = 0;
            while (true) {
                Spawn();
                currentCount++;
                if (currentCount >= maxCount) {
                    break;
                }
                yield return new WaitForSeconds(Random.Range(1f, 3f));
            }
        }

        [EasyButtons.Button]
        public void Spawn() {
            Enemy enemy = Instantiate(prefabEnemy);
            enemy.transform.SetParent(parentEnemy);
            enemy.transform.position = PathController.Instance.pointSpawnEnemy.position;
            enemy.Setup(index);
            listEnemy.Add(enemy);
            index++;
        }

        public void UpdateEnemyList() {
            bool flag = true;

            while (true) {
                flag = true;
                for (int i = 0; i < listEnemy.Count; i++) {
                    if (listEnemy[i] == null) {
                        listEnemy.RemoveAt(i);
                        flag = false;
                    }
                }

                if (flag) {
                    break;
                }
            }
        }
    }
}