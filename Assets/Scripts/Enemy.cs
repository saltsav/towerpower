﻿using Assets.Scripts;
using DG.Tweening;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [ReadOnly] public int index;
    public RoadOnePart moveRoadOnePart;
    public Transform looker;
    public Vector3 pointToMove;
    public Vector3 needAngle;

    public float lastDis;
    public float currentDis;

    public float life = 10;

    public void Setup(int newIndex) {
        index = newIndex;
        MoveToPoint(PathController.Instance.GetRoadOnePartByIndex(0));
    }

    public void MoveToPoint(RoadOnePart nextRoadOnePart) {
        if (nextRoadOnePart == null) {
            moveRoadOnePart = null;
            return;
        }

        moveRoadOnePart = nextRoadOnePart;

        pointToMove.x = nextRoadOnePart.transform.position.x + Random.Range(-0.5f, 0.5f);
        pointToMove.y = transform.position.y;
        pointToMove.z = nextRoadOnePart.transform.position.z + Random.Range(-0.5f, 0.5f);
        looker.position = transform.position;
        looker.LookAt(pointToMove);
        //transform.LookAt(pointToMove);
        needAngle = looker.eulerAngles;

        transform.DOLocalRotate(needAngle, PathController.Instance.disBeetwinRoads / EnemyController.Instance.speed * 0.5f).SetEase(Ease.Linear);

        currentDis = Vector3.Distance(transform.position, pointToMove);
        lastDis = currentDis;
    }

    public void Update() {
        if (moveRoadOnePart == null) {
            return;
        }

        transform.Translate(Vector3.forward * EnemyController.Instance.speed * Time.deltaTime);
        currentDis = Vector3.Distance(transform.position, pointToMove);

        if (currentDis < 0.5f || currentDis > lastDis) {
            MoveToPoint(PathController.Instance.GetRoadOnePartByIndex(moveRoadOnePart.index + 1));
            lastDis = float.MaxValue;
        } else {
            lastDis = currentDis;
        }
    }

    public void TakeDamage(float powerDamage) {
        life -= powerDamage;
        if (life < 0) {
            Destroy(gameObject);
            TowerController.Instance.UpdateAll();
            EnemyController.Instance.UpdateEnemyList();
        }
    }
}