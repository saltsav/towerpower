﻿using System;
using UnityEngine;

#if UNITY_IOS
using UnityEngine.iOS;
#endif


public class PushNotification : MonoSingleton<PushNotification> {

    [NonSerialized] public bool OnScene;

    // Локальные пуши
    void Start() {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Приложение уходит/выходит из фона
    void OnApplicationPause(bool pause) {

        /// На паузе или нет
        if (pause) {

            // Добавление нотификаций
#if UNITY_IOS
			UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
			UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
			AddNotifications();
#elif UNITY_ANDROID

            Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
            AddNotifications();

#endif
        } else {

            // Очистка
#if UNITY_IOS
			UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
			UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#elif UNITY_ANDROID
            Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
#endif
        }
    }

    // Добавиление нотификаций <день> * <часы> * <минуты>
    void AddNotifications() {

        // Призывные пуши
        ScheduleNotification("Let’s catch some animals!", "Your dog is waiting for you!", 3600000);
        ScheduleNotification("Where are you?", "The weather is perfect to herd your cattle.", 86400000);
        ScheduleNotification("What does the fox say?", " It's time to scare all foxes", 259200000);
        ScheduleNotification("Come back!", "Your dog miss you!", 604800000);
    }

    // Создание пуша
    void ScheduleNotification(string title, string text, double milliseconds) {

#if UNITY_IOS
		// Инициализация
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();

		// Добавление данных
		notif.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
		notif.fireDate = DateTime.Now.AddMilliseconds(milliseconds);
		notif.alertTitle = title;
		notif.alertBody = text;
		notif.hasAction = false;

		// Добавление
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);

#elif UNITY_ANDROID

        // Добавление данных
        var notificationParams = new Assets.SimpleAndroidNotifications.NotificationParams {

            Id = UnityEngine.Random.Range(0, int.MaxValue),
            Delay = TimeSpan.FromMilliseconds(milliseconds),
            Title = title,
            Message = text,
            Ticker = "Ticker",
            Sound = true,
            Vibrate = true,
            Light = true,
            SmallIcon = Assets.SimpleAndroidNotifications.NotificationIcon.Clock,
            SmallIconColor = new Color(233f / 255f, 145f / 255f, 52f / 255f),
            LargeIcon = "main_icon"
        };

        // Добавление
        //Assets.SimpleAndroidNotifications.NotificationManager.SendCustom(notificationParams);
#endif
    }

    // Добавление
    public void Add() {
        OnScene = true;
    }
}