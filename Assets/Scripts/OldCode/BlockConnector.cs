﻿using System;
using Assets.Scripts.ScriptsHerd;
using UnityEngine;

public class BlockConnector : MonoBehaviour {
    public int index;
    [ReadOnly] public InfoOneBigBlockMap currentInfoOneBigBlockMap;
    public int x;
    public int z;

    public float worldX;
    public float worldZ;

    public Bridges currentBridge;

    public bool iUsed;

    public void UpdateInfo() {
        x = (int) (transform.localPosition.x + 30) / 12; //блок изначально 6 единиц!
        z = (int) (transform.localPosition.z + 30) / 12;


        if (currentInfoOneBigBlockMap == null || transform.parent.gameObject.name != currentInfoOneBigBlockMap.gameObject.name) {
            currentInfoOneBigBlockMap = transform.parent.GetComponent<InfoOneBigBlockMap>();
        }

        if (currentInfoOneBigBlockMap == null) {
            Debug.LogError("parent don`t have InfoOneBigBlockMap!");
            return;
        }

        index = currentInfoOneBigBlockMap.AddBlockConnector(this);
        name = "BlockConnector" + index;
    }

    public void Setup() {
        worldX = transform.position.x;
        worldZ = transform.position.z;
        if (currentInfoOneBigBlockMap.listBridges == null) {
            return;
        }

        foreach (Bridges bridge in currentInfoOneBigBlockMap.listBridges) {
            if (Math.Abs(Math.Abs(bridge.x) - Math.Abs(worldX)) < 0.1f && Math.Abs(Math.Abs(bridge.z) - Math.Abs(worldZ)) < 0.1f) {
                currentBridge = bridge;
            }
        }
    }
}