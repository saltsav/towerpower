﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FearIconController : MonoBehaviour {

    [SerializeField] SpriteRenderer Icon;
    Sequence _seq;
    float _time;

    private void OnEnable() {
        _seq.Kill();
        Icon.gameObject.SetActive(false);
    }


    // Показ иконки
    public void ShowIcon() {
        Icon.transform.localScale = Vector3.zero;
        Icon.gameObject.SetActive(true);
        Icon.color = new Color(255f / 255f, 0f / 255f, 75f / 255f, 1);

        DOTween.Sequence()
            .Append(Icon.transform.DOScale(2.2f, 0.35f))
            .AppendCallback(() => {
                _time = 0.3f;
                LoopScale();
            });
    }

    // Пассивное состояние иконки
    public void PassivIcon() {
        _time = 0.8f;
        Icon.DOColor(Color.black, 0.7f);
    }

    // Скрытие иконки
    public void HiddenIcon() {
        _seq.Kill();
        Icon.transform.DOScale(0, 0.15f).OnComplete(() => {
            Icon.gameObject.SetActive(false);
        });
    }

    // Быстрый показ иконки
    public void ShowIconWarning() {
        Icon.transform.localScale = Vector3.zero;
        Icon.gameObject.SetActive(true);
        Icon.color = Color.black;

        DOTween.Sequence()
            .Append(Icon.transform.DOScale(2.4f, 0.25f))
            .Append(Icon.transform.DOScale(1.3f, 0.25f))
            .Append(Icon.transform.DOScale(1.8f, 0.15f))
            .Append(Icon.transform.DOScale(1.3f, 0.25f))
            .Append(Icon.transform.DOScale(1.8f, 0.15f))
            .AppendCallback(() => {
                Icon.transform.DOScale(0.8f, 0.25f);
                Icon.DOFade(0, 0.2f);
                Icon.gameObject.SetActive(false);
            });
    }

    // Анимация скейла
    void LoopScale() {
        _seq = DOTween.Sequence()
                   .Append(Icon.transform.DOScale(1.3f, _time).SetEase(Ease.Linear))
                   .Append(Icon.transform.DOScale(1.8f, _time).SetEase(Ease.Linear))
                   .AppendCallback(() => {
                       LoopScale();
                   });
    }
}
