﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using EasyButtons;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Maze {
    public enum TypeCell {
        none,
        wall,
        road
    }

    public class GenerationMaze : MonoSingleton<GenerationMaze> {
        private int constHeight;
        private int constWidth;
        public CellMaze[,] mazeArray;
        public List<CellMaze> lineMazeArray = new List<CellMaze>();

        [TextArea(10, 50)] public string arrayStr;

        [Button]
        public void GenerationBaseMaze() {
            
            //constWidth = Mathf.Clamp(PlayerInfoManager.Instance.countBlocks * 2 + 1, 7, 21);
            //constHeight = Mathf.Clamp(PlayerInfoManager.Instance.countBlocks * 2 + 1, 7, 21);
            constWidth = 27;//обязательно должно быть не чётным!
            constHeight = 27;//обязательно должно быть не чётным!
            mazeArray = new CellMaze[constWidth, constHeight];
            GenerationDefault();
            GenerationMazeFull();
            SetTypeConnectionSide();
            ShowThisMaze();
        }

        public void SetTypeConnectionSide() {
            for (int x = 1; x < constHeight; x += 2) {
                for (int z = 1; z < constWidth; z += 2) {
                    CellMaze cellMaze = mazeArray[x, z];
                    mazeArray[x, z].listTypeConnectionSide.Clear();
                    if (cellMaze.x + 1 < constHeight && mazeArray[cellMaze.x + 1, cellMaze.z].typeCell == TypeCell.road) {
                        mazeArray[x, z].listTypeConnectionSide.Add(TypeConnectionSide.down);
                    }

                    if (cellMaze.x - 1 > 0 && mazeArray[cellMaze.x - 1, cellMaze.z].typeCell == TypeCell.road) {
                        mazeArray[x, z].listTypeConnectionSide.Add(TypeConnectionSide.up);
                    }

                    if (cellMaze.z + 1 < constWidth && mazeArray[cellMaze.x, cellMaze.z + 1].typeCell == TypeCell.road) {
                        mazeArray[x, z].listTypeConnectionSide.Add(TypeConnectionSide.right);
                    }

                    if (cellMaze.z - 1 > 0 && mazeArray[cellMaze.x, cellMaze.z - 1].typeCell == TypeCell.road) {
                        mazeArray[x, z].listTypeConnectionSide.Add(TypeConnectionSide.left);
                    }
                }
            }
        }

        public void GenerationDefault() {
            for (int i = 0; i < constHeight; i++) {
                for (int j = 0; j < constWidth; j++) {
                    mazeArray[i, j] = new CellMaze {z = j, x = i};
                    if (i % 2 != 0 && j % 2 != 0 && i < constHeight - 1 && j < constWidth - 1) {
                        mazeArray[i, j].typeCell = TypeCell.road;
                    } else {
                        mazeArray[i, j].typeCell = TypeCell.wall;
                    }
                }
            }
        }

        public void GenerationMazeFull() {
            //центрируем начало лабиринта
            int startI = constWidth / 2;
            int startJ = constHeight / 2;


            startI = startI % 2 == 0 ? startI + 1 : startI;
            startJ = startJ % 2 == 0 ? startJ + 1 : startJ;
            CellMaze currentCell = mazeArray[startI, startJ];
            lineMazeArray.Clear();
            AddCellInLineList(currentCell);
            bool isFirstCell = true;
            int index = 0;
            do {
                bool flag = false;
                do {
                    flag = true;
                    SetNeighbours(currentCell, isFirstCell);
                    isFirstCell = false;
                    currentCell.isVisited = true;
                    if (currentCell.cellsNeighbours.Count == 0) {
                        flag = false;
                    } else {
                        int randomIndex = Random.Range(0, currentCell.cellsNeighbours.Count);
                        DestroyWall(currentCell, currentCell.cellsNeighbours[randomIndex], index);
                        CellMaze newCurrentCell = currentCell.cellsNeighbours[randomIndex];
                        currentCell.cellsNeighbours.RemoveAt(randomIndex);
                        currentCell = newCurrentCell;
                        AddCellInLineList(currentCell);
                        if (lineMazeArray.Count >= PlayerInfoManager.Instance.countBlocks) {
                            flag = false;
                        }
                    }
                } while (flag);

                currentCell = GetNextCellMaze();
                if (currentCell == null || lineMazeArray.Count >= PlayerInfoManager.Instance.countBlocks) {
                    break;
                }

                AddCellInLineList(currentCell);
                index++;
            } while (HaveCellWithDontVisitNeighbours());
            //Debug.Log("лист либиринта  = " + lineMazeArray.Count);
        }

        public void AddCellInLineList(CellMaze cellMaze) {
            foreach (CellMaze cell in lineMazeArray) {
                if (cell.x == cellMaze.x && cell.z == cellMaze.z) {
                    return;
                }
            }

            lineMazeArray.Add(cellMaze);
        }

        public bool HaveCellWithDontVisitNeighbours() {
            for (int i = 1; i < constHeight; i = i + 2) {
                for (int j = 1; j < constWidth; j = j + 2) {
                    if (mazeArray[i, j].cellsNeighbours.Count != 0) {
                        return true;
                    }
                }
            }

            return false;
        }

        public CellMaze GetNextCellMaze() {
            List<CellMaze> listNeighbours = new List<CellMaze>();
            for (int i = 1; i < constHeight; i = i + 2) {
                for (int j = 1; j < constWidth; j = j + 2) {
                    SetNeighbours(mazeArray[i, j], false);
                    if (mazeArray[i, j].cellsNeighbours.Count != 0 && mazeArray[i, j].isVisited) {
                        listNeighbours.Add(mazeArray[i, j]);
                        //return mazeArray[i, j];
                    }
                }
            }

            if (listNeighbours.Count > 0) {
                return listNeighbours[Random.Range(0, listNeighbours.Count - 1)];
            }

            return null;
        }

        public void DestroyWall(CellMaze first, CellMaze second, int index) {
            if (first.x == second.x) {
                if (first.z < second.z) {
                    mazeArray[first.x, first.z + 1].typeCell = TypeCell.road;
                } else {
                    mazeArray[first.x, first.z - 1].typeCell = TypeCell.road;
                }
            }

            if (first.z == second.z) {
                if (first.x < second.x) {
                    mazeArray[first.x + 1, first.z].typeCell = TypeCell.road;
                } else {
                    mazeArray[first.x - 1, first.z].typeCell = TypeCell.road;
                }
            }

            second.beforeCellMazeX = first.x;
            second.beforeCellMazeZ = first.z;
        }

        public void SetNeighbours(CellMaze cellMaze, bool isFirstCell) {
            cellMaze.cellsNeighbours.Clear();
            //первый элемент всегда смотрит выходом вверх и соседние ячейки считаются посещёнными
            if (isFirstCell) {
                //cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x + 2, cellMaze.z]);
                mazeArray[cellMaze.x + 2, cellMaze.z].isVisited = true;

                cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x - 2, cellMaze.z]);
                //mazeArray[cellMaze.x - 2, cellMaze.z].isVisited = true;

                //cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x, cellMaze.z + 2]);
                mazeArray[cellMaze.x, cellMaze.z + 2].isVisited = true;

                //cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x, cellMaze.z - 2]);
                mazeArray[cellMaze.x, cellMaze.z - 2].isVisited = true;
            } else {
                if (cellMaze.x + 2 < constHeight && !mazeArray[cellMaze.x + 2, cellMaze.z].isVisited) {
                    cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x + 2, cellMaze.z]);
                }

                if (cellMaze.x - 2 > 0 && !mazeArray[cellMaze.x - 2, cellMaze.z].isVisited) {
                    cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x - 2, cellMaze.z]);
                }

                if (cellMaze.z + 2 < constWidth && !mazeArray[cellMaze.x, cellMaze.z + 2].isVisited) {
                    cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x, cellMaze.z + 2]);
                }

                if (cellMaze.z - 2 > 0 && !mazeArray[cellMaze.x, cellMaze.z - 2].isVisited) {
                    cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.x, cellMaze.z - 2]);
                }
            }
        }

        [Button]
        public void ShowThisMaze() {
            CellMaze[,] maze = mazeArray;
            string s = string.Empty;

            int startI = constWidth / 2;
            int startJ = constHeight / 2;
            startI = startI % 2 == 0 ? startI + 1 : startI;
            startJ = startJ % 2 == 0 ? startJ + 1 : startJ;
            for (int x = 0; x < constWidth; x++) {
                for (int z = 0; z < constHeight; z++) {
                    if (x == startI && z == startJ) {
                        s += "2";
                    } else {
                        if (maze[x, z].typeCell == TypeCell.wall) {
                            s += "0";
                        }
                        if (maze[x, z].typeCell == TypeCell.road) {
                            s += "1";
                        }
                    }
                }

                s += "\n";
            }

            arrayStr = s;
            //Debug.Log(s);
        }
    }

    [Serializable]
    public class CellMaze { //вспомогательный класс для лабиринта
        public bool isVisited;
        public int x;
        public int z;
        public TypeCell typeCell = TypeCell.none;
        [NonSerialized] public List<CellMaze> cellsNeighbours = new List<CellMaze>();
        public List<TypeConnectionSide> listTypeConnectionSide = new List<TypeConnectionSide>();

        public int beforeCellMazeX;
        public int beforeCellMazeZ;
    }
}