﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartGame : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    // Нажал
    public void OnPointerDown(PointerEventData evd) {
       // MainLogic.Instance.StartGame();
        gameObject.SetActive(false);
    }

    // Отпустил
    public void OnPointerUp(PointerEventData evd) {

    }
}
