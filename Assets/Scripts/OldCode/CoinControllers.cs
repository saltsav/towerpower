﻿using System.Collections.Generic;
using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class CoinControllers : MonoSingleton<CoinControllers> {
        int CountCoins;

        public Coin prefabCoin;
        public Transform parentCoin;
        public List<Coin> AllCoins;

        public void DestroyCoins() {
            foreach (Coin allCoin in AllCoins) {
                if (allCoin != null) {
                    Destroy(allCoin.gameObject);
                }
            }

            AllCoins.Clear();
        }

        public void GenerationCoins(bool isStart) {
            CountCoins = PlayerInfoManager.Instance.countCoins;

            if (isStart) {
                DestroyCoins();
                for (int i = 0; i < CountCoins; i++) {
                    GameObject goCoin = PoolsController.Instance.poolCoins.Spawn(MapManager.Single.RandomNavmeshLocation(1), Quaternion.identity, parentCoin);
                    Coin coin = goCoin.GetComponent<Coin>();
                    coin.transform.position = new Vector3(coin.transform.position.x, 2f, coin.transform.position.z);
                    AllCoins.Add(coin);

                    // Рандомный поворот
                    Vector3 rotate = Vector3.zero;
                    rotate.y = Random.Range(0, 360);
                    coin.transform.localEulerAngles = rotate;

                    MapManager.Single.UpdateNavMeshMap();
                }
            } else {
                // Убираем все зоны "NotSpawn"
                foreach (Coin coin in AllCoins) {
                    coin.SetStart();
                }
                  
                //MapManager.Single.UpdateNavMesh();
            }
        }

        public void TakeThisCoin(int count) {
            CoinCountManager.Instance.UpdateCount(count);
        }
    }
}