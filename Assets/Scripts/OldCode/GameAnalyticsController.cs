﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAnalyticsController : MonoBehaviour {


	// Проверка на наличия
	public static bool _onScene;
	public static void CheckOnScreen() {
		if (!_onScene) {
			GameObject newLoadScreen = Resources.Load<GameObject>("Prefabs/Source/GameAnalytics");
			Instantiate(newLoadScreen, Vector3.zero, Quaternion.identity);
			_onScene = true;
		}
	}
}
