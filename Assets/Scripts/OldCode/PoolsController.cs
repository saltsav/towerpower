﻿using System;
using System.Collections.Generic;
using DPPoolObject;
using EasyButtons;
using UnityEngine;

namespace Assets.Scripts.Core {
    public class PoolsController : MonoSingleton<PoolsController> {
        public DPPoolGameObject poolCoins;
        public DPPoolGameObject poolBigDog;
        public DPPoolGameObject poolHaskyDog;
        public DPPoolGameObject poolCow;
        public DPPoolGameObject poolOvcharkaDog;
        public DPPoolGameObject poolPig;
        public DPPoolGameObject poolWhiteSheep;
        public DPPoolGameObject poolBrownSheep;
        public DPPoolGameObject poolBlackSheep;
        public DPPoolGameObject poolBadCow;
        public DPPoolGameObject poolFoxView;
        public DPPoolGameObject poolAnimal;
        public DPPoolGameObject poolFox;

        public DPPoolGameObject poolFearIcon;

        public List<PoolsLocalBlocks> listPoolsLocalBlocks;

        private void Awake() {
            DontDestroyOnLoad(gameObject);
        }

        public GameObject GetLocalBlocksFromPoolByType(TypeBigBlockMap typeBigBlockMap) {
            foreach (PoolsLocalBlocks poolLocalBlock in listPoolsLocalBlocks) {
                if (poolLocalBlock.typeBigBlockMap == typeBigBlockMap) {
                    if (poolLocalBlock.pool.Prefab == null) {
                        Debug.LogError(typeBigBlockMap + " NOT FOUND");
                    }
                    return poolLocalBlock.pool.Spawn(Vector3.zero, Quaternion.identity);
                }
            }

            return null;
        }

        [Button]
        public void CreatePoolsLocalBlocks() {
            listPoolsLocalBlocks.Clear();
            for (int i = 0; i < 23; i++) {

                TypeBigBlockMap eCurrent = (TypeBigBlockMap) i;
                listPoolsLocalBlocks.Add(new PoolsLocalBlocks() {
                    nameType = eCurrent.ToString(),
                    typeBigBlockMap = eCurrent,


                }
                );
            }
        }
    }

    [Serializable]
    public class PoolsLocalBlocks {
        [ReadOnly] public string nameType;
        public TypeBigBlockMap typeBigBlockMap;
        public DPPoolGameObject pool;
    }
}