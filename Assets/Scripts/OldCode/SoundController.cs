﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public enum SoundsList {
        Button,
        Unavail,
        PurchaseSound,
        Money,
        Sheep,
        Window,
        FinishWindow,
        Checkbox,
        FailWindow,
        Fail,
        Barks,
        Fox,
        MoneySlider,
        FinishSound
    }

    public class SoundController : MonoSingleton<SoundController> {
        [SerializeField] private AudioSource AudioSourceSound;
        [SerializeField] private AudioClip[] Sounds;

        public void OnEnable() {
            //MainLogic.Instance.actionOnPause += SetPause;
        }

        public void StartFromController(string nameTracks) {

        }


        // Запуск звука
        public void PlaySound(SoundsList sound, float volume) {
            if (!GUIOptions.isEnableMusic) return;
            AudioSourceSound.PlayOneShot(Sounds[(int) sound], volume);
        }
    }
}