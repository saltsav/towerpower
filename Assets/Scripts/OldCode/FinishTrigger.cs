﻿using Assets.Scripts.Core;
using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts {
    public class FinishTrigger : MonoBehaviour {

        private void OnEnable() {
            SetRotate();
            Invoke("SetRotate", 0.1f);
        }

        void SetRotate() {
            transform.parent.rotation = Quaternion.identity;
        }

        // При косании
        private void OnTriggerEnter(Collider other) {
            if (other.gameObject.tag == "Dog") {
                //GameManager.Single.LevelCompleted();
            }
        }
    }
}


