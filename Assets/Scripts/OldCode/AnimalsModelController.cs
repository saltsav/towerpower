﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Core;
using UnityEngine;

    public class AnimalsModelController : MonoSingleton<AnimalsModelController> {
        public List<AnimalView> listViewAnimal;

        public AnimalView GetAnimalViewByType(TypeAnimal typeAnimal) {
            if (listViewAnimal == null || listViewAnimal.Count == 0) {
                Debug.LogError(" массив моделей пуст");
                return null;
            }
            GameObject go = null;
            switch (typeAnimal) {
                case TypeAnimal.bigDog:
                    go = PoolsController.Instance.poolBigDog.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.haskyDog:
                    go = PoolsController.Instance.poolHaskyDog.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.cow:
                    go = PoolsController.Instance.poolCow.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.ovcharkaDog:
                    go = PoolsController.Instance.poolOvcharkaDog.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.pig:
                    go = PoolsController.Instance.poolPig.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.whiteSheep:
                    go = PoolsController.Instance.poolWhiteSheep.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.brownSheep:
                    go = PoolsController.Instance.poolBrownSheep.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.blackSheep:
                    go = PoolsController.Instance.poolBlackSheep.Spawn(Vector3.zero, Quaternion.identity);
                    break;
                case TypeAnimal.badCow:
                    go = PoolsController.Instance.poolBadCow.Spawn(Vector3.zero, Quaternion.identity);
                    break;

                case TypeAnimal.fox:
                    go = PoolsController.Instance.poolFoxView.Spawn(Vector3.zero, Quaternion.identity);
                    break;

            }

            AnimalView animalView = go.GetComponent<AnimalView>();
            if (animalView != null) {
                return animalView;
            }

            Debug.LogError(typeAnimal + " typeAnimal not found ");
            return null;
        }

    }
