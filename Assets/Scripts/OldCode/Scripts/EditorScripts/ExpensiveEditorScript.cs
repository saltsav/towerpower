﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;


// Кнопки привязки
[CanEditMultipleObjects, CustomEditor(typeof(SnapToGround))]
public class SnapVerticalObjectEditor : Editor {
	public override void OnInspectorGUI() {


		SnapToGround _script = (SnapToGround)target;

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("↑", GUILayout.ExpandWidth(false), GUILayout.Width(60), GUILayout.Height(26))) {
			_script.SnapUp();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();


		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.FlexibleSpace();

		if (GUILayout.Button("←", GUILayout.ExpandWidth(false), GUILayout.Width(50), GUILayout.Height(26))) {
			_script.SnapLeft();
		}
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("→", GUILayout.ExpandWidth(false), GUILayout.Width(50), GUILayout.Height(26))) {
			_script.SnapRight();
		}
		GUILayout.FlexibleSpace();
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();


		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("↓", GUILayout.ExpandWidth(false), GUILayout.Width(60), GUILayout.Height(26))) {
			_script.SnapDown();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

	}
}

#endif