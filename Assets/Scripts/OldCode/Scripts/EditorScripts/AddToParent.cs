﻿using UnityEngine;

[ExecuteInEditMode]
public class AddToParent : MonoBehaviour {
#if UNITY_EDITOR

	[SerializeField] string NameParent;
	void Awake() {
			bool isNull = gameObject.transform.parent == null;
			if (isNull || gameObject.transform.parent.name != NameParent) {
				GameObject parent = GameObject.Find(NameParent);
				if (parent == null) parent = new GameObject(NameParent);


				if (!isNull && gameObject.transform.parent.name.Contains("MoveBox")) {
					gameObject.transform.parent.transform.SetParent(parent.transform);
				} else {
					gameObject.transform.SetParent(parent.transform);
				}
			}
	}

#endif
}

