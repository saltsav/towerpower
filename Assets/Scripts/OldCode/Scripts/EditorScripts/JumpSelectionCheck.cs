﻿using UnityEngine;

//#if UNITY_EDITOR
using UnityEditor;
//#endif


[ExecuteInEditMode]
public class JumpSelectionCheck : MonoBehaviour {
	[ReadOnly, SerializeField] JumpTrajectory Trajectory;

#if UNITY_EDITOR
	[SerializeField] bool LockLine = false;
#endif
	void Start() {
		Trajectory.gameObject.SetActive(false);

#if UNITY_EDITOR
		LockLine = false;
		Selection.selectionChanged += Update;
#endif
	}

#if UNITY_EDITOR

	// При изменении значений
	void OnValidate() {
		Update();
	}

	private void OnDestroy() {
		Selection.selectionChanged -= Update;
	}

	void Update() {
		if (Selection.Contains(gameObject) && (!Trajectory.gameObject.activeSelf || transform.hasChanged)) {
			Trajectory.gameObject.SetActive(true);
			Trajectory.SetTrajectoryPoints();
		} else if (!Selection.Contains(gameObject) && Trajectory.gameObject.activeSelf) {
			Trajectory.gameObject.SetActive(LockLine);
		}
	}
#endif
}
