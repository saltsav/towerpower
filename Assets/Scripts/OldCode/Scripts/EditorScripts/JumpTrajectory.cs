﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class JumpTrajectory : MonoBehaviour {

#if UNITY_EDITOR

	[SerializeField] LineRenderer lineRendererCenter;
	[SerializeField] LineRenderer lineRendererLeft;
	[SerializeField] LineRenderer lineRendererRight;



	//[SerializeField] JumpUp jump;


	static float min = 3.65f;
	static float max = 4.04f;

	static float powerMax = 83.19255f;
	static float powerMin = 42.72002f;

	static float yOffsetMax = 3.53f;
	static float yOffsetMin = 0.65f;

	static float maxY = 17.19672f;
	static float minY = 6.119745f;

	static int numPoints = 40;

	static float xOffsetLeft = -3.31f;
	static float xOffsetRight = 1.86f;


	void Start() {
		Vector3 locPos = transform.localPosition;
		locPos.x = Math.Abs(locPos.x);//jump.BackJump ? Math.Abs(locPos.x) : -Math.Abs(locPos.x);
		transform.localPosition = locPos;
	}
#endif

	// Установка точек траектории
	public void SetTrajectoryPoints() {
#if UNITY_EDITOR

		Vector3 startPosition = transform.position;
		Vector3 pVelocity = Vector3.zero; //new Vector2(jump.BackJump ? -jump.ForceX : jump.ForceX, jump.ForceY);

		float velocityTest = Mathf.Sqrt((pVelocity.x * pVelocity.x) + (pVelocity.y * pVelocity.y));
		float percent = (powerMax - powerMin) / 100f;
		float del = min + (((max - min) / 100) * ((velocityTest - powerMin) / percent));

		pVelocity = pVelocity / del;

		float velocity = Mathf.Sqrt((pVelocity.x * pVelocity.x) + (pVelocity.y * pVelocity.y));
		float angle = Mathf.Rad2Deg * (Mathf.Atan2(pVelocity.y, pVelocity.x));
		float fTime = 0.1f;

		for (int i = 0; i < numPoints; i++) {

			float dx = velocity * fTime * Mathf.Cos(angle * Mathf.Deg2Rad);
			float dy = velocity * fTime * Mathf.Sin(angle * Mathf.Deg2Rad) - (Physics2D.gravity.magnitude * fTime * fTime / 2.0f);

			Vector3 pos = new Vector3(startPosition.x + dx, startPosition.y + dy, 0);
			float percent2 = (maxY - minY) / 100f;
			pos.y -= yOffsetMin + (((yOffsetMax - yOffsetMin) / 100) * ((dy - minY) / percent2));

			if (lineRendererLeft != null) {
				Vector3 newPos = pos;
				newPos.x += xOffsetLeft;
				lineRendererLeft.positionCount = numPoints;
				lineRendererLeft.SetPosition(i, newPos);
			}
			if (lineRendererCenter != null) {
				lineRendererCenter.positionCount = numPoints;
				lineRendererCenter.SetPosition(i, pos);
			}
			if (lineRendererRight != null) {
				Vector3 newPos = pos;
				newPos.x += xOffsetRight;
				lineRendererRight.positionCount = numPoints;
				lineRendererRight.SetPosition(i, newPos);
			}

			fTime += 0.1f;
		}
#endif
	}
}


