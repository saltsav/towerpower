﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToGround : MonoBehaviour {
#if UNITY_EDITOR

	// Прикрепить к верху
	public void SnapUp() {
		RaycastHit2D check = Physics2D.Raycast(transform.position, Vector2.up, 25f, LayerMask.GetMask("Ground"));
		if (check.collider != null) {
			Vector3 pos = transform.position;
			pos.y = check.point.y;
			transform.position = pos;
		}
	}

	// Прикрепить к низу
	public void SnapDown() {
		RaycastHit2D check = Physics2D.Raycast(transform.position, Vector2.down, 25f, LayerMask.GetMask("Ground"));
		if (check.collider != null) {
			Vector3 pos = transform.position;
			pos.y = check.point.y;
			transform.position = pos;
		}
	}

	// Прикрепить к левой стороне
	public void SnapLeft() {
		RaycastHit2D check = Physics2D.Raycast(transform.position, Vector2.left, 25f, LayerMask.GetMask("Ground"));
		if (check.collider != null) {
			Vector3 pos = transform.position;
			pos.x = check.point.x;
			transform.position = pos;
		}
	}

	// Прикрепить к правой стороне
	public void SnapRight() {
		RaycastHit2D check = Physics2D.Raycast(transform.position, Vector2.right, 25f, LayerMask.GetMask("Ground"));
		if (check.collider != null) {
			Vector3 pos = transform.position;
			pos.x = check.point.x;
			transform.position = pos;
		}
	}

#endif
}
