﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class SetStaticOclluderFlags {
	private const int TransparentQueue = 3000;
	const int OverlayQueue = 4000;
	public void Execute() {
		var scene = Object.FindObjectsOfType(typeof(GameObject));
		var transparents = scene.Where(o => IsStatic(o) && IsTransparent((GameObject)o)).ToArray();
		var occluders = scene.Where(o => IsStatic(o) && !IsTransparent((GameObject)o)).ToArray();
		SceneModeUtility.SetStaticFlags(transparents, (int)StaticEditorFlags.OccluderStatic, false);
		SceneModeUtility.SetStaticFlags(transparents, (int)StaticEditorFlags.OccludeeStatic, true);
		SceneModeUtility.SetStaticFlags(occluders, (int)StaticEditorFlags.OccluderStatic, true);
		SceneModeUtility.SetStaticFlags(occluders, (int)StaticEditorFlags.OccludeeStatic, true);
		Debug.Log("SetStaticOclluderFlagsCmd done");


	}
	private bool IsStatic(Object obj) {
		GameObject gameObject = obj as GameObject;
		if (gameObject == null)
			return false;
		return GameObjectUtility.AreStaticEditorFlagsSet(gameObject, StaticEditorFlags.BatchingStatic);
	}

	private bool IsTransparent(GameObject obj) {

		return obj != null
			&& obj.GetComponent<Renderer>() != null
				  && obj.GetComponent<Renderer>().sharedMaterials.Any(x => x.renderQueue >= TransparentQueue && x.renderQueue < OverlayQueue);
	}
}