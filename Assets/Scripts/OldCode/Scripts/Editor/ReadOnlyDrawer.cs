﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer {

	//static bool DeveloperMode = true;
	static bool DeveloperMode = false;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		return EditorGUI.GetPropertyHeight(property, label, true);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		if (!DeveloperMode) GUI.enabled = false;
		EditorGUI.PropertyField(position, property, label, true);
		if (!DeveloperMode) GUI.enabled = true;
	}
}