﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


// Типы событий управления
public enum ControlAction {
	upSwipe,
	downSwipe,
	rightSwipe,
	leftSwipe,
	tap
}


public class ControlManager : MonoSingleton<ControlManager> {
	private ControlManager() { }


	public static event Action<ControlAction> IsSwipe;
	public static event Action<Vector2, bool> IsTouch;
	public static event Action<Vector2> IsMove;

	// Чуствительность свайпа
	float SensetiveSwipe = 120.0f;      // По расстоянию
	float SensetiveSwipeDelta = 450f;   // По скорости

	// Последний свайп
	ControlAction _lastSwipe = ControlAction.tap;

	// Включено управление или нет
	public bool Enable = false;

	bool _isTouch;

	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;
	Vector2 _cacheVector;

	void Start() {
		_cacheVector = Vector2.zero;
		UpdateManager.UpdateThis += UpdateThis;
	}

	protected override void OnDestroy() {
		UpdateManager.UpdateThis -= UpdateThis;
		IsSwipe = null;
		IsTouch = null;
		IsMove = null;
		base.OnDestroy();
	}


	// Обновление
	void UpdateThis() {

		if (!Enable) {
			firstPressPos = Vector2.zero;
			return;
		}


#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR

		if (Input.touchCount > 0) {
			if (Input.GetTouch(0).position.x != _cacheVector.x || Input.GetTouch(0).position.y != _cacheVector.y) {
				_cacheVector.x = Input.GetTouch(0).position.x;
				_cacheVector.y = Input.GetTouch(0).position.y;

				// Движение
				if (_isTouch) {
					EventMoveScreen(_cacheVector);
					TouchSwipe();
				}
			}
		}

		// Отжатие
		if (_isTouch && Input.touchCount == 0) {
			_isTouch = false;
			EventTapToScreen(_cacheVector);
		}

		// Нажатие
		if (!_isTouch && Input.touchCount > 0) {
			_isTouch = true;
			EventTapToScreen(_cacheVector);
		}
#endif


#if UNITY_STANDALONE || UNITY_EDITOR
		MouseSwipe();
		KeyControl();

		if (Input.mousePosition.x != _cacheVector.x || Input.mousePosition.y != _cacheVector.y) {
			_cacheVector.x = Input.mousePosition.x;
			_cacheVector.y = Input.mousePosition.y;

			// Движение
			if (_isTouch) EventMoveScreen(_cacheVector);
		}

		// Отжатие
		if (_isTouch && Input.GetMouseButtonUp(0)) {
			_isTouch = false;
			EventTapToScreen(_cacheVector);
		}

		// Нажатие
		if (!_isTouch && Input.GetMouseButtonDown(0)) {
			_isTouch = true;
			EventTapToScreen(_cacheVector);
		}
#endif
	}

	// Нажатия на кнопки
	void KeyControl() {
		if (Input.GetKeyDown(KeyCode.UpArrow)) { // Вверх
			EventSwipeOnScreen(ControlAction.upSwipe);
		} else
			if (Input.GetKeyDown(KeyCode.DownArrow)) { // Вниз
			EventSwipeOnScreen(ControlAction.downSwipe);
		} else
				if (Input.GetKeyDown(KeyCode.LeftArrow)) { // Лево
			EventSwipeOnScreen(ControlAction.leftSwipe);
		} else
					if (Input.GetKeyDown(KeyCode.RightArrow)) { // Право 
			EventSwipeOnScreen(ControlAction.rightSwipe);
		}
		_lastSwipe = ControlAction.tap;
	}

#if UNITY_STANDALONE || UNITY_EDITOR

	// Свайп мышью
	void MouseSwipe() {
		if (Input.GetMouseButtonDown(0)) {
			ReloadFirstPressPos();
		} else if (Input.GetMouseButtonUp(0)) {
			_cacheVector.x = Input.mousePosition.x;
			_cacheVector.y = Input.mousePosition.y;
			secondPressPos = _cacheVector;
			SwipeCheck(Vector2.zero);
			_lastSwipe = ControlAction.tap;
		}
	}
#endif

	// Тач свайпы 
	Vector2 _delta;
	void TouchSwipe() {
		Touch touch = Input.GetTouch(0);

		if (touch.phase == TouchPhase.Began) {
			ReloadFirstPressPos();
		}
		if (touch.phase == TouchPhase.Moved) {
			_delta = touch.deltaPosition / touch.deltaTime;
			_cacheVector.x = touch.position.x;
			_cacheVector.y = touch.position.y;
			secondPressPos = _cacheVector;
			SwipeCheck(_delta);
		}
		if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
			_lastSwipe = ControlAction.tap;
		}
	}

	// Определение свайпа
	Vector2 _normalSwipe;
	void SwipeCheck(Vector2 delta) {
		if (firstPressPos == Vector2.zero) return;

		// Вектор между двумя точками
		currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
		_normalSwipe = new Vector2(currentSwipe.x, currentSwipe.y);
		_normalSwipe.Normalize();

		// Свайпы
		if ((currentSwipe.y > SensetiveSwipe || delta.y > SensetiveSwipeDelta) && _normalSwipe.x > -0.5f && _normalSwipe.x < 0.5f) { // Вверх
			ReloadFirstPressPos();
			EventSwipeOnScreen(ControlAction.upSwipe);
		} else
		if ((currentSwipe.y < -SensetiveSwipe || delta.y < -SensetiveSwipeDelta) && _normalSwipe.x > -0.5f && _normalSwipe.x < 0.5f) { // Вниз
			ReloadFirstPressPos();
			EventSwipeOnScreen(ControlAction.downSwipe);
		} else
		if ((currentSwipe.x < -SensetiveSwipe || delta.x < -SensetiveSwipeDelta) && _normalSwipe.y > -0.5f && _normalSwipe.y < 0.5f) { // Лево
			ReloadFirstPressPos();
			EventSwipeOnScreen(ControlAction.leftSwipe);
		} else
		if ((currentSwipe.x > SensetiveSwipe || delta.x > SensetiveSwipeDelta) && _normalSwipe.y > -0.5f && _normalSwipe.y < 0.5f) { // Право 
			ReloadFirstPressPos();
			EventSwipeOnScreen(ControlAction.rightSwipe);
		}
	}

	// Обновление точки контакта
	void ReloadFirstPressPos() {
		_cacheVector.x = Input.mousePosition.x;
		_cacheVector.y = Input.mousePosition.y;
		firstPressPos = _cacheVector;
	}

	//
	public void ResetControl() {
		ReloadFirstPressPos();
	}

	// Движение  
	void EventMoveScreen(Vector2 position) {
		if (!Enable) return;

		IsMove.SafeInvoke(position);
	}

	// Сделан тап 
	void EventTapToScreen(Vector2 position) {
		if (!Enable) return;

		IsTouch.SafeInvoke(position, _isTouch);
	}

	// Сделан свайп
	void EventSwipeOnScreen(ControlAction action) {
		if (!Enable) return;

		if (_lastSwipe != action) IsSwipe.SafeInvoke(action);
		_lastSwipe = action;
	}

}
