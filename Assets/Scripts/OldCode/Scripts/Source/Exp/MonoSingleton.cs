﻿using UnityEngine;
using System.Collections.Generic;


public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour {

	private static T s_Instance;

	public static T Instance {
		get {
			if (s_Instance == null) {
				s_Instance = FindObjectOfType<T>();
				if (s_Instance == null) {
					List<T> list = Extensions.FindObjectsOfTypeAll<T>();
					if (list.Count > 0) s_Instance = list[0];
					if (s_Instance == null) {
						var gameObject = new GameObject(typeof(T).Name);
						s_Instance = gameObject.AddComponent<T>();
					}
				}
			}

			return s_Instance;
		}
	}

	public static T Single {
		get {
			return Instance;
		}
	}

	protected virtual void OnDestroy() {
		if (s_Instance) {
			Destroy(s_Instance);
		}
		s_Instance = null;
	}
}
