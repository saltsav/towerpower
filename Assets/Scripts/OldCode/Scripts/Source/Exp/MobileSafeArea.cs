﻿
using UnityEngine;

public class MobileSafeArea : MonoBehaviour {

	//[SerializeField] bool Auto;
	[Range(-80, 80), SerializeField] float SaveAreaHorizontal;
	[Range(-180, 180), SerializeField] float SaveAreaVetical;

	[Range(-60f, 200f), SerializeField] float SizeOffsetTop;
	[Range(-60f, 200f), SerializeField] float SizeOffsetBottom;

	// Установка отступа
	void Awake() {

		var safeArea = Screen.safeArea;

		// Установка адаптированных позиций
		if (safeArea.x > 0 || safeArea.y > 0) ApplySafeArea();

#if UNITY_EDITOR
		// Если в редакторе, то адаптировать под iPhone X
		if ((Application.platform == RuntimePlatform.OSXEditor ||
			 Application.platform == RuntimePlatform.WindowsEditor))
			if (Screen.width == 2436 || Screen.height == 2436 || Screen.width == 2688 || Screen.height == 2688) ApplySafeArea();

#endif

	}

	// По заданным позициям
	void ApplySafeArea() {

		// Изменение позиции
		RectTransform Panel = gameObject.GetComponent<RectTransform>();
		Panel.localPosition = new Vector3(Panel.localPosition.x + SaveAreaHorizontal,
										  Panel.localPosition.y + SaveAreaVetical,
										  Panel.localPosition.z);

		// Изменение размера
		if (SizeOffsetBottom > 0) Panel.offsetMin = new Vector2(Panel.offsetMin.x, SizeOffsetBottom);
		if (SizeOffsetTop > 0) Panel.offsetMax = new Vector2(Panel.offsetMax.x, SizeOffsetTop);

	}

	//Автоматически
	void ApplySafeAreaAuto(Rect area) {
		RectTransform Panel = gameObject.GetComponent<RectTransform>();

		var anchorMin = area.position;
		var anchorMax = area.position + area.size;
		anchorMin.x /= Screen.width;
		anchorMin.y /= Screen.height;
		anchorMax.x /= Screen.width;
		anchorMax.y /= Screen.height;

		Panel.anchorMin = anchorMin;
		Panel.anchorMax = anchorMax;

	}
}

