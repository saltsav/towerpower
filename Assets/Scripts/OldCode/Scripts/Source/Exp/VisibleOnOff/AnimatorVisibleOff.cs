﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorVisibleOff : MonoBehaviour {

    [SerializeField] Animator AnimatorComponent;
    [SerializeField] float TimeDelay = 0.2f;

    void Start() {
        if (TimeDelay < 0.1f) TimeDelay = 0.2f;
        OnBecameInvisible();
    }

    // Видно или нет
    void OnBecameVisible() {
        if (AnimatorComponent != null) {
            CancelInvoke();
            AnimatorComponent.enabled = true;
        }
    }
    void OnBecameInvisible() {
        if (AnimatorComponent != null) {
            if (TimeDelay > 0) {
                Invoke("DisableAnimator", TimeDelay);
            } else {
                DisableAnimator();
            }
        }
    }

    // Отключение
    void DisableAnimator() {
        AnimatorComponent.enabled = false;
    }
}
