﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System.Text;
using DPPoolObject;
//using System;
using TMPro;

public static class Extensions {

	public static string GetThiusendString(int number, bool doobleSpace = false) {
		return GetThiusendString((long) number, doobleSpace);
	}

	public static string GetThiusendString(long number, bool doobleSpace = false) {
		string cutChar = CutNumber(ref number);
		StringBuilder _heightText = new StringBuilder();

		if (number >= 1000) {

			char[] strChar = number.ToString().ToCharArray();

			int count = 0;
			for (int i = strChar.Length - 1; i >= 0; i--) {

				_heightText.Insert(0, strChar[i]);
				if (count == 2) {
					_heightText.Insert(0, doobleSpace ? "\u200a\u200a" : "\u200a");
					count = 0;
				} else {
					count++;
				}
			}
		} else {
			_heightText.Append(number);
		}

		return _heightText.ToString() + cutChar;
	}

	static string[] character = new string[] { "", "K", "M", "B" };
	public static string CutNumber(ref long number) {
		int count = 0;

		if (number > 10000000000000) {
			number = number / 1000000000;
			count = 3;
		} else if (number > 10000000000) {
			number = number / 1000000;
			count = 2;
		} else if (number > 10000000) {
			number = number / 1000;
			count = 1;
		}
		return character[count];
	}

	public static void LookAt2D(this Transform me, Vector2 eye, Vector2 target) {
		Vector2 look = target - (Vector2) me.position;

		float angle = Vector2.Angle(eye, look);

		Vector2 right = Vector3.Cross(Vector3.forward, look);

		int dir = 1;

		if (Vector2.Angle(right, eye) < 90) {
			dir = -1;
		}

		me.rotation *= Quaternion.AngleAxis(angle * dir, Vector3.forward);
	}

	public static void LookAt2D(this Transform me, Vector2 eye, Transform target) {
		me.LookAt2D(eye, target.position);
	}

	public static void LookAt2D(this Transform me, Vector2 eye, GameObject target) {
		me.LookAt2D(eye, target.transform.position);
	}


	public static List<T> FindObjectsOfTypeAll<T>() {
		List<T> results = new List<T>();
		for (int i = 0; i < SceneManager.sceneCount; i++) {
			var s = SceneManager.GetSceneAt(i);
			if (s.isLoaded) {
				var allGameObjects = s.GetRootGameObjects();
				for (int j = 0; j < allGameObjects.Length; j++) {
					var go = allGameObjects[j];
					results.AddRange(go.GetComponentsInChildren<T>(true));
				}
			}
		}
		return results;
	}


	// Полет иконок
	public static float FlyIcon(this GameObject target, Vector3 ptStart, Vector3 ptEnd, GameObject objectToSpawn, Transform parent, int counts, float time, float scale = 1) {
		ptStart.z = 0; ptEnd.z = 0;

		// Время
		time = 0.7f * time;

		// Создание
		float delay = 0;
		for (int i = 1; i <= counts; i++) {

			float timeCache = time;

			GameObject moveGO = DPPool.Spawn(objectToSpawn, parent);
			moveGO.transform.localScale = new Vector3(0.85f, 0.85f, 1);
			moveGO.transform.position = ptStart;

			Image img = moveGO.GetComponent<Image>();

			// Анимация исчезновения/появления
			DOTween.Sequence().AppendInterval(delay)
					.AppendCallback(() => { img.DOFade(1, 0.15f); })
					.AppendInterval(timeCache - 0.1f)
					.AppendCallback(() => {
						img.DOFade(0, 0.14f);
						moveGO.transform.DOScale(0.5f, 0.14f);
					})
					.AppendInterval(0.12f)
					.AppendCallback(() => {
						moveGO.SetActive(false);
						DPPool.Despawn(moveGO);
					});

			// Анимация увелечения
			if (scale > 1) {
				float currentScale = moveGO.transform.localScale.x;
				DOTween.Sequence().SetEase(Ease.InQuad)
						.Append(moveGO.transform.DOScale(scale, time / 100 * 30))
						.Append(moveGO.transform.DOScale(currentScale, time / 100 * 70));
			}

			// Анимация движения
			DOTween.Sequence().AppendInterval(delay)
					.Append(moveGO.transform.DOLocalMoveX(Random.Range(-70, 70), (timeCache / 5f) * 3.25f))
					 .Append(moveGO.transform.DOLocalMoveX(0, (timeCache / 5f) * 2.1f));

			// Движение до точки
			DOTween.Sequence().AppendInterval(delay)
					.Append(moveGO.transform.DOMove(ptEnd, timeCache).SetEase(Ease.InSine));

			// Изменение времени
			delay += (float) Random.Range(50, 80) / 1000.0f;
		}

		return time;
	}

	/// <summary>
	/// Searchs the child.
	/// </summary>
	/// <returns>The child.</returns>
	/// <param name="target">Target.</param>
	/// <param name="name">Name.</param>
	// Поиск среди детей
	public static Transform SearchChild(this Transform target, string name) {
		if (target.name == name) return target;

		for (int i = 0; i < target.childCount; ++i) {
			var result = SearchChild(target.GetChild(i), name);

			if (result != null) return result;
		}

		return null;
	}

	public static T FindComponentInChildWithTag<T>(this Transform target, string tag) where T : Component {
		foreach (Transform tr in target) {
			if (tr.tag == tag) {
				return tr.GetComponent<T>();
			}
		}
		return null;
	}

	public static void Swap<T>(this IList<T> list, int indexA, int indexB) {
		T tmp = list[indexA];
		list[indexA] = list[indexB];
		list[indexB] = tmp;
	}


	/// <summary>
	/// Changes the number increase (multi step).
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="current">Current.</param>
	/// <param name="newCurrent">New current.</param>
	/// <param name="time">Time.</param>
	// Анимация прибавление чисел
	public static void ChangeNumberIncrease(this TextMeshProUGUI target, long current, long newCurrent, float time) {
		target.ChangeNumberIncrease(current, newCurrent, time, "", "");
	}


	/// <summary>
	/// Changes the number increase (multi step).
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="current">Current.</param>
	/// <param name="newCurrent">New current.</param>
	/// <param name="time">Time.</param>
	/// <param name="countSteps">Count steps.</param>
	public static void ChangeNumberIncrease(this TextMeshProUGUI target, long current, long newCurrent, float time, int countSteps) {
		target.ChangeNumberIncrease(current, newCurrent, time, "", "", countSteps);
	}

	/// <summary>
	/// Changes the number increase (multi step).
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="current">Current.</param>
	/// <param name="newValue">New current.</param>
	/// <param name="time">Time.</param>
	/// <param name="endSymbol">End symbol.</param>
	/// <param name="countSteps">Count steps.</param>
	public static void ChangeNumberIncrease(this TextMeshProUGUI target, long current, long newValue, float time, string firstSymbol, string endSymbol, int countSteps = 15) {
		target.StopAllCoroutines();
		target.StartCoroutine(StartNumber(target, current, newValue, time, firstSymbol, endSymbol, countSteps));
	}
	static IEnumerator StartNumber(TextMeshProUGUI target, long current, long newValue, float time, string firstSymbol, string endSymbol, int countSteps) {

		// Разница в количестве
		long difference = (long) Mathf.Abs(newValue - current);

		// Количество циклов анимации
		long count = difference <= countSteps ? difference : countSteps;

		if (count == 0 || difference == 0) yield return null;

		// На какое число увеличивать
		long stepSolar = difference / count;
		stepSolar = current < newValue ? stepSolar : -stepSolar;

		// Время анимаций
		float timeStep = time / (count > 1 ? ((float) count - 1f) : (float) count);

		// Обновление текста
		for (int i = 0; i <= count; i++) {

			// Обновление значения
			long number = i == count ? newValue : current + stepSolar;
			current = number;

			// Добавление текста
			target.text = firstSymbol + GetThiusendString(number) + endSymbol;

			yield return new WaitForSeconds(timeStep);
		}
		yield return null;
	}


	/// <summary>
	/// Changes the number once (one step).
	/// </summary>
	/// <param name="target">Target.</param>
	/// <param name="newValue">Current.</param>
	public static void ChangeNumberOnce(this Text target, int newValue, string endSymbol = "", string startsymbol = "") {
		ChangeNumberOnce(target, newValue, 0.7f, endSymbol, startsymbol);
	}
	public static void ChangeNumberOnce(this Text target, int newValue, float time, string endSymbol = "", string startsymbol = "") {
		DOTween.defaultTimeScaleIndependent = true;

		// Анимация прибавления
		DOTween.Kill("ChangeNumberOnce" + target.GetInstanceID().ToString());
		Sequence animCount = DOTween.Sequence().SetId("ChangeNumberOnce" + target.GetInstanceID().ToString());
		animCount.Append(target.transform.DOScale(1.35f, (time / 100f) * 43f))
				 .AppendCallback(() => {
					 target.text = startsymbol + newValue.ToString() + endSymbol;
				 })
				 .Append(target.transform.DOScale(1f, (time / 100f) * 57f));
	}



}