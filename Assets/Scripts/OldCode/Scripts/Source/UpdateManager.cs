﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UpdateManager : MonoSingleton<UpdateManager> {

    static public event Action UpdateThis;
    static public event Action FixedUpdateThis;
    static public event Action LateUpdateThis;

    public static bool _onScene;

    // При старте
    void Start() {
        _onScene = true;
        DontDestroyOnLoad(transform.gameObject);
    }

    public void Reset() {
        UpdateThis = null;
        FixedUpdateThis = null;
        LateUpdateThis = null;
    }

    private void Update() {
        UpdateThis.SafeInvoke();
    }

    private void FixedUpdate() {
        FixedUpdateThis.SafeInvoke();
    }

    private void LateUpdate() {
        LateUpdateThis.SafeInvoke();
    }

    // Проверка на наличия
    public static void CheckOnScreen() {
        if (!_onScene) {
            Single.Reset();
        }
    }
}
