﻿using UnityEngine;

namespace Assets.Scripts.Scripts.Source {
    public class LookAtCamera : MonoBehaviour {
        [SerializeField] bool LookX = false;

        public Transform iconTransform;
        private Vector3 angel = new Vector3(0, 0, 0);

        public void OnEnable() {
            UpdateManager.UpdateThis += UpdateThis;
        }

        public void OnDisable() {
            UpdateManager.UpdateThis -= UpdateThis;
        }

        public void UpdateThis() {
            iconTransform.LookAt(CameraController.Instance.transform);
            if (LookX) angel.x = (iconTransform.eulerAngles.x) + 90;
            iconTransform.eulerAngles = angel;
        }
    }
}