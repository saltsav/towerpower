﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworksController : MonoSingleton<FireworksController> {

    [SerializeField] GameObject[] FireWork;

    private void Start() {
        foreach (GameObject go in FireWork) go.SetActive(false);
    }

    public void ShowFireworks() {
        DOTween.Sequence()
                    .AppendCallback(() => {
                        HapticController.UseHaptic(HapticList.ImpactMedium);
                        FireWork[0].SetActive(true);
                    })
                    .AppendInterval(0.1f)
                    .AppendCallback(() => {
                        FireWork[1].SetActive(true);
                    })
                    .AppendInterval(0.05f)
                    .AppendCallback(() => {
                        FireWork[2].SetActive(true);
                    });

    }
}
