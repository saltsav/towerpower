﻿using DPPoolObject;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCanvasIconController : MonoSingleton<WorldCanvasIconController> {

    [SerializeField] GameObject FinalMoneyIcon;

    GameObject _go;
    PlusMoneyIcon _iconMoney;

    // Добавление иконок монеток
    public PlusMoneyIcon AddFinalMoneyIcon(Transform tr) {

        _go = DPPool.Spawn(FinalMoneyIcon, transform);
        _iconMoney = _go.GetComponent<PlusMoneyIcon>();
        _iconMoney.Target = tr;

        return _iconMoney;
    }


    // Убрать иконку
    public void DespawnIcon(GameObject icon) {

        DPPool.Despawn(icon);
    }
}
