﻿
using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class PlayerSaveDataEditor : EditorWindow {

    [MenuItem("Tools/Save Data")]
    public static void openWindow() {

        PlayerSaveDataEditor window = (PlayerSaveDataEditor) EditorWindow.GetWindow(typeof(PlayerSaveDataEditor));
        window.titleContent = new GUIContent("SaveData");
        window.Show();
    }

    public enum FieldType {
        Int, String, Float
    }

    private FieldType fieldType = FieldType.Int;
    private string setKey = "";
    private string setVal = "";
    private string error = null;

    private string lvl1 = "1";
    private string lvl2 = "1";
    private string lvl3 = "1";
    private string lvl4 = "1";
    private string wipe = "0";

    void OnGUI() {

        var oldColor = GUI.backgroundColor;

        var textDimensions = GUI.skin.label.CalcSize(new GUIContent("__________"));
        EditorGUIUtility.labelWidth = textDimensions.x;

        EditorGUILayout.Separator();

        if (error != null && !error.Equals("")) {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField(" (" + error + ")", EditorStyles.boldLabel);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        fieldType = (FieldType) EditorGUILayout.EnumPopup("Key Type", fieldType);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        setKey = EditorGUILayout.TextField("Key", setKey, GUILayout.MinWidth(150));

        if (GUILayout.Button("Get", GUILayout.Width(60), GUILayout.Height(16))) {
            if (fieldType == FieldType.Int) {
                setVal = PlayerPrefs.GetInt(setKey).ToString();
            } else if (fieldType == FieldType.Float) {
                setVal = PlayerPrefs.GetFloat(setKey).ToString();
            } else {
                setVal = PlayerPrefs.GetString(setKey);
            }
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        EditorGUILayout.Separator();


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        setVal = EditorGUILayout.TextField("Value", setVal, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            if (fieldType == FieldType.Int) {
                int result;
                if (!int.TryParse(setVal, out result)) {
                    error = "Invalid input \"" + setVal + "\"";
                    return;
                }
                PlayerPrefs.SetInt(setKey, result);
            } else if (fieldType == FieldType.Float) {
                float result;
                if (!float.TryParse(setVal, out result)) {
                    error = "Invalid input \"" + setVal + "\"";
                    return;
                }
                PlayerPrefs.SetFloat(setKey, result);

            } else {
                PlayerPrefs.SetString(setKey, setVal);
            }
            PlayerPrefs.Save();
            error = null;
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.backgroundColor = new Color(1, 0.7f, 0.7f, 1);
        if (GUILayout.Button("Delete All", GUILayout.Width(90), GUILayout.Height(28))) {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        if (GUILayout.Button("Set Dafault", GUILayout.Width(90), GUILayout.Height(28))) {
            PlayerPrefs.DeleteAll();
            //SaveManager.SetStartSetting();
        }
        GUI.backgroundColor = oldColor;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();



        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        lvl2 = EditorGUILayout.TextField("Lvl Speed", lvl2, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            int result;
            if (!int.TryParse(lvl2, out result)) {
                return;
            }
            //SaveManager.SetSpeedUpdate(result - 1);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        lvl1 = EditorGUILayout.TextField("Lvl Fuel", lvl1, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            int result;
            if (!int.TryParse(lvl1, out result)) {
                return;
            }
            //SaveManager.SetFuelUpdate(result - 1);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        lvl3 = EditorGUILayout.TextField("Lvl Earnings", lvl3, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            int result;
            if (!int.TryParse(lvl3, out result)) {
                return;
            }
            //SaveManager.SetEarnUpdate(result - 1);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        lvl4 = EditorGUILayout.TextField("Lvl Offline", lvl4, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            int result;
            if (!int.TryParse(lvl4, out result)) {
                return;
            }
            //SaveManager.SetOfflineUpdate(result - 1);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        wipe = EditorGUILayout.TextField("Wipe Number", wipe, GUILayout.MinWidth(150));

        if (GUILayout.Button("Set", GUILayout.Width(60), GUILayout.Height(16))) {
            int result;
            if (!int.TryParse(wipe, out result)) {
                return;
            }
            //SaveManager.SetCurrentWipe(result);
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        //GUI.backgroundColor = new Color(0.8f, 1, 0.8f, 1);
        //GUILayout.BeginHorizontal();
        //GUILayout.FlexibleSpace();
        //if (GUILayout.Button("ХХХ", GUILayout.Width(90), GUILayout.Height(20))) {
        //	//
        //}
        //GUILayout.FlexibleSpace();
        //GUILayout.EndHorizontal();



        //GUILayout.BeginHorizontal();
        //GUILayout.FlexibleSpace();
        //if (GUILayout.Button("ХХХ", GUILayout.Width(96), GUILayout.Height(20))) {
        //	//
        //}
        //if (GUILayout.Button("ХХХ", GUILayout.Width(96), GUILayout.Height(20))) {
        //	//
        //}
        //GUILayout.FlexibleSpace();
        //GUILayout.EndHorizontal();
        //GUI.backgroundColor = oldColor;
    }
}
