﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using DPPoolObject;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIStartModal : MonoSingleton<GUIStartModal> {

        [SerializeField] CanvasGroup ToAlpha;
        public Transform Finger;

        public RectTransform panelStartModal;

        public Coroutine coroStartAfterPause;
        [ReadOnly] public bool isFingerShow;
        [HideInInspector] public bool haveFirstStart;

        public void Awake() {
            ToAlpha.alpha = 0;
        }

        public void StopCoroAfterPause() {
            if (coroStartAfterPause != null) {
                StopCoroutine(coroStartAfterPause);
            }
            coroStartAfterPause = null;
        }

        public void StartFromController(bool isFirstStart) {
            isFingerShow = true;
            AndroidBackButton.Instance.SetStateInApplication(StateInApplication.dragToStart);
            GUIUpStars.Instance.SetActiveBtnPause(true);
            ToAlpha.alpha = 0;
            AnimationFinger();
            panelStartModal.anchoredPosition = Vector2.zero;

            StopCoroAfterPause();
            if (isFirstStart) {
                coroStartAfterPause = StartCoroutine(IEnumWaitTouchForStartLevel());
            } else {
                coroStartAfterPause = StartCoroutine(IEnumWaitTouchForResumAfterPause());
            }
        }

        private IEnumerator IEnumWaitTouchForStartLevel() {

           // MainLogic.Instance.SetStateGame(StateGame.pause);
            yield return new WaitForSeconds(0.3f);
            do {
                if (Input.touchCount > 0 || Input.GetKey(KeyCode.Mouse0)) {
                    break;
                }

                yield return null;
            } while (true);

            haveFirstStart = true;
            isFingerShow = false;
            StopFingerAnimation();
            GUIUpStars.Instance.SetActiveBtnPause(true);
            AndroidBackButton.Instance.SetStateInApplication(StateInApplication.game);
        }

        private IEnumerator IEnumWaitTouchForResumAfterPause() {

            yield return new WaitForSeconds(0.3f);
            do {
                if (Input.touchCount > 0 || Input.GetKey(KeyCode.Mouse0)) {
                    break;
                }

                yield return null;
            } while (true);

            isFingerShow = false;
            StopFingerAnimation();
            GUIUpStars.Instance.SetActiveBtnPause(true);

            AndroidBackButton.Instance.SetStateInApplication(StateInApplication.game);
        }

        public void StopFingerAnimation() {
            StopCoroAfterPause();
            isFingerShow = false;

            ToAlpha.DOFade(0, 0.2f).OnComplete(() => {
                ToAlpha.gameObject.SetActive(false);
            }); ;
        }

        public void AnimationFinger() {
            ToAlpha.gameObject.SetActive(true);
            ToAlpha.DOFade(1, 0.2f);
        }
    }
}