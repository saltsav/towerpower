﻿using System.Collections;
using DG.Tweening;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIContinueModal : MonoSingleton<GUIContinueModal> {
        [SerializeField] private CanvasGroup ToAlphaGroup;

        [SerializeField] CanvasGroup FadeWindow;
        [SerializeField] Transform MainBox;
        [SerializeField] RectTransform ViewBlockOptions;

        public Button btnContinueGame;
        public Coroutine coroPanelContinueGame;
        public RectTransform panelContinueGame;
        public Button btnNoThanks;
        public Text textNoThanks;
        public Text textLevel;

        public Text textSheep;
        //public Text textCompleted;
        [HideInInspector] public float currentTime;
        private Sequence _sequenceBtnVideo;
        //public Image imgVideo;
        public Image imgTime;

        private float _timeForCalculationProgress = 2;

        private readonly Color _colorFadeBackground = new Color(0.012f, 0.161f, 0.643f, 0.431f);
        private readonly Color _colorTextNoThanks = new Color(1f, 1f, 1f, 1.000f);

        [HideInInspector] public bool isShow;

        private void Start() {
            FadeWindow.gameObject.SetActive(false);
        }

        [Button]
        public void PlayerIsDeath() {

            textLevel.text = "LEVEL " + PlayerInfoManager.Instance.GetCurrentLvl();

            SoundController.Single.PlaySound(SoundsList.FailWindow, 0.5f);

            ToAlphaGroup.interactable = true;
            isShow = true;
            AndroidBackButton.Instance.SetStateInApplication(StateInApplication.continueGame);
            GUIUpStars.Instance.SetActiveBtnPause(false);
            btnNoThanks.interactable = false;
            SetTextSheepCount(GoodAnimalCountManager.Instance.GetCountSheepInHerd());
            btnContinueGame.onClick.RemoveAllListeners();
            btnContinueGame.onClick.AddListener(() => {
              //  MainLogic.Instance.ResurectDog();
                // Проверка на наличие интернета сразу
                /*if (GUIBadInternet.Instance.CheckConnection(StateInApplication.continueGame, ToAlphaGroup)) {
                    AdsController.Single.VideoStatusAction += RewardForAds;

                    // Попытка показать рекламу
                    if (!AdsController.Single.ShowRewardAd(ToAlphaGroup, StateInApplication.continueGame)) {
                        AdsController.Single.VideoStatusAction -= RewardForAds;
                    }
                }*/
            });

            textNoThanks.color = GP.colorZero;
            SetActivePanelContinueGame(true);
            if (coroPanelContinueGame != null) {
                StopCoroutine(coroPanelContinueGame);
            }
            coroPanelContinueGame = StartCoroutine(IEnumWaitShowNoThanks());
            //StartCoroutine(IEnumCalculationProgress());

            // Сразу показывать процент
            //Debug.Log(SoundController.inst.GetTimeClip() / SoundController.inst.GetTimeLastBit());
            //SetTextCompleted((int) (SoundController.Instance.GetTimeClip() / SoundController.Instance.GetTimeLastBeat() * 100));

            btnNoThanks.onClick.RemoveAllListeners();
            btnNoThanks.onClick.AddListener(() => {
                //SetActivePanelContinueGame(false);
                //GUIResultWindow.Single.ShowResultWindow();
                //GameManager.Single.RestartGame();
            });

            // Показ надпиcи и кнопки отказа от воскрешения
            DOTween.Sequence().AppendInterval(GP.timeForShowNoThanks)
                .AppendCallback(() => {
                    btnNoThanks.interactable = true;
                    textNoThanks.DOColor(_colorTextNoThanks, 0.4f);
                });
        }

        // После завершения рекламы
        private void EndAds(bool status) {
            //AdsController.Single.VideoStatusAction -= EndAds;
            ToAlphaGroup.interactable = true;
            GoToResultGame();
        }

        // Выдача награды
        private void RewardForAds(bool statusAds) {
            //AdsController.Single.VideoStatusAction -= RewardForAds;

            // Получил награду или нет
            if (statusAds) {
                ResumeGame();
            } else {
                SoundController.Instance.PlaySound(SoundsList.Unavail, 0.7f);
            }
        }

        public void ResumeGame() {
            isShow = false;

            GUIUpStars.Instance.SetActiveBtnPause(true);
            _sequenceBtnVideo.Kill();
            if (coroPanelContinueGame != null) {
                StopCoroutine(coroPanelContinueGame);
            }

            coroPanelContinueGame = null;
            //	PlayerController.Instance.SetDefaultLife();
            SetActivePanelContinueGame(false);
            //MainLogic.inst.SetPauseOnGame(false);


            GUIStartModal.Instance.StartFromController(false);
            //SpawnerController.Instance.ClearFlyFood();
        }

        public void GoToResultGame() {
            isShow = false;

            _sequenceBtnVideo.Kill();
            if (coroPanelContinueGame != null) {
                StopCoroutine(coroPanelContinueGame);
            }

            coroPanelContinueGame = null;
            SetActivePanelContinueGame(false);
            GUIResultWindow.Instance.ShowResultWindow();
        }

        public void SetTextLevel(int var) {
            //textScore.text = Helper.ConvertToStringSpace(var.ToString());
        }

        public void SetTextSheepCount(int var) {
            //  textLevel.text = var + " / " + SheepCountManager.Instance.totalCount;
            textSheep.text = var + " / " + GoodAnimalCountManager.Instance.GetTotalCountSheep();
        }

        public void SetActivePanelContinueGame(bool var) {

            if (var) {
                SetActiveViewBlockOptions(true);
            } else {
                SetActiveViewBlockOptions(false);
            }
        }

        public IEnumerator IEnumWaitShowNoThanks() {
            currentTime = GP.timeForRessurect;
            if (_sequenceBtnVideo != null) {
                _sequenceBtnVideo.Kill();
            }

            //AnimationVideoImg();
            while (true) {
                currentTime -= Time.deltaTime;
                imgTime.fillAmount = currentTime / GP.timeForRessurect;
                if (currentTime < 0) {
                    break;
                }

                yield return null;
            }
        }


        /*public void AnimationVideoImg() {
			imgVideo.transform.localScale = Vector3.one;
			imgVideo.transform.eulerAngles = Vector3.zero;
			_sequenceBtnVideo = DOTween.Sequence();
			_sequenceBtnVideo.Append(imgVideo.transform.DOPunchRotation(Vector3.one * 20, 1f));
			_sequenceBtnVideo.Join(imgVideo.transform.DOPunchScale(Vector3.one * 0.2f, 0.5f));
			_sequenceBtnVideo.AppendInterval(Random.Range(2, 3));
			_sequenceBtnVideo.OnComplete(AnimationVideoImg);
		}*/



        public void SetActiveViewBlockOptions(bool var) {
            if (var) {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.continueGame);

                // Начальная настройка
                MainBox.localScale = new Vector3(0.85f, 0.85f, 1);
                FadeWindow.gameObject.SetActive(true);
                ViewBlockOptions.anchoredPosition = Vector2.zero;

                // Анимация размера
                DOTween.Sequence()
                    .Append(MainBox.transform.DOScale(1.07f, 0.25f))
                    .Append(MainBox.transform.DOScale(1f, 0.15f));

                // Анимация прозрачности
                FadeWindow.DOFade(1, 0.2f).OnComplete(() => {
                    // Включение кнопок
                    FadeWindow.interactable = true;
                    FadeWindow.blocksRaycasts = true;
                });
            } else {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.continueGame);

                // Скрытие
                MainBox.transform.DOScale(0.85f, 0.25f);
                FadeWindow.DOFade(0, 0.25f).OnComplete(() => {
                    // Отключение кнопок
                    FadeWindow.interactable = false;
                    FadeWindow.blocksRaycasts = false;

                    // Отключение окна
                    FadeWindow.gameObject.SetActive(false);
                });
            }
        }
    }
}