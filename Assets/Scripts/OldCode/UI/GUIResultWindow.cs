﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIResultWindow : MonoSingleton<GUIResultWindow> {
        [SerializeField] private CanvasGroup ToAlphaGroup;

        [SerializeField] private CanvasGroup FadeWindow;
        [SerializeField] private Transform MainBox;
        [SerializeField] private RectTransform ViewBlockOptions;

        public RectTransform panelResultWindow;

        public Text textLvl;
        public Text textCountSheep;
        public Text textCoins;

        public Button btnCollect;
        public Button btnCollectx2;

        public bool _isGoodFinish;
        bool _rewardOn;

        public void Start() {
            FadeWindow.gameObject.SetActive(false);

            btnCollect.onClick.RemoveAllListeners();
            btnCollect.onClick.AddListener(() => {
                btnCollectx2.interactable = false;
                btnCollect.interactable = false;
                PlayerInfoManager.Instance.SaveCurrentLvl(PlayerInfoManager.Instance.GetCurrentLvl() + 1);
                //GameManager.Single.RestartGame();
            });
            btnCollectx2.onClick.RemoveAllListeners();
            btnCollectx2.onClick.AddListener(() => {
                DoubleScore();
            });
        }



        public void ShowResultWindow(bool isGoodFinish = false) {
            _isGoodFinish = isGoodFinish;
            textLvl.text = "LEVEL " + PlayerInfoManager.Instance.GetCurrentLvl();
            GUIUpStars.Single.SetActiveViewBlockStars(false);
           // MainLogic.Instance.SetStateGame(StateGame.result);
            btnCollectx2.interactable = true;
            btnCollect.interactable = true;
            GUIFadeBack.Instance.SetDisable();
            GUIContinueModal.Instance.SetActivePanelContinueGame(false);
            AndroidBackButton.Instance.SetStateInApplication(StateInApplication.result);
            SetActiveViewBlockOptions(true);

            textCountSheep.text = GoodAnimalCountManager.Instance.GetCountSheepInHerd() + " / " + GoodAnimalCountManager.Instance.GetTotalCountSheep();
            PlayerInfoManager.Instance.AddToSaveCoinCount(CoinCountManager.Instance.GetCoinOnThisLevel());
            textCoins.text = CoinCountManager.Instance.GetCoinOnThisLevel().ToString();
        }


        // Пока окна
        public void SetActiveViewBlockOptions(bool var) {
            if (var) {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.continueGame);

                // Начальная настройка
                MainBox.localScale = new Vector3(0.8f, 0.8f, 1);
                FadeWindow.gameObject.SetActive(true);
                ViewBlockOptions.anchoredPosition = Vector2.zero;

                // Анимация размера
                DOTween.Sequence()
                    .Append(MainBox.transform.DOScale(1.095f, 0.35f))
                    .Append(MainBox.transform.DOScale(1f, 0.25f));

                // Анимация прозрачности
                FadeWindow.DOFade(1, 0.2f).OnComplete(() => {
                    // Включение кнопок
                    FadeWindow.interactable = true;
                    FadeWindow.blocksRaycasts = true;
                });
            } else {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.continueGame);

                // Скрытие
                MainBox.transform.DOScale(0.85f, 0.25f);
                FadeWindow.DOFade(0, 0.25f).OnComplete(() => {
                    // Отключение кнопок
                    FadeWindow.interactable = false;
                    FadeWindow.blocksRaycasts = false;

                    // Отключение окна
                    FadeWindow.gameObject.SetActive(false);
                });
            }
        }


        // Рестарт
        public void Restart(bool isOk) {
            PlayerInfoManager.Instance.SaveCurrentLvl(PlayerInfoManager.Instance.GetCurrentLvl() + 1);
            PlayerInfoManager.Instance.AddToSaveCoinCount(CoinCountManager.Instance.GetCoinOnThisLevel());
            textCoins.text = CoinCountManager.Instance.GetCoinOnThisLevel().ToString();
            //GameManager.Single.RestartGame();
        }

        // Двойная награда
        public void DoubleScore() {
            if (_rewardOn) {
                _rewardOn = false;
                AdRewardController.Single.VideoCompleted -= Restart;
            }
            if (AdRewardController.Single.ShowRewardAd(ListRewardAds.FinishWindow) && !_rewardOn) {
                _rewardOn = true;
                AdRewardController.Single.VideoCompleted += Restart;
            }
        }
    }
}