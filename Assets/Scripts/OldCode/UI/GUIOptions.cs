﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIOptions : MonoSingleton<GUIOptions> {
        public static bool isEnableHaptic;
        private static bool _isEnableMusic;

        public static bool isEnableMusic {
            set {
                _isEnableMusic = value;
                if (actionEnableMusic != null) {
                    actionEnableMusic(value);
                }
            }
            get => _isEnableMusic;
        }

        public static Action<bool> actionEnableMusic;

        public RectTransform viewBlockOptions;
        public Button btnMusic;
        public Button btnVibration;
        public Button btnClose;
        public Button btnPrivacyPolicy;

        [SerializeField] private CanvasGroup MainMenu;
        [SerializeField] private CanvasGroup FadeWindow;
        [SerializeField] private Transform MainBox;

        public void Awake() {
            // Начальная настройка
            FadeWindow.gameObject.SetActive(false);
            FadeWindow.interactable = false;
            FadeWindow.blocksRaycasts = false;
            FadeWindow.alpha = 0;
        }

        public void Start() {
            btnClose.onClick.RemoveAllListeners();
            btnClose.onClick.AddListener(() => { SetActiveViewBlockOptions(false); });

            btnVibration.onClick.RemoveAllListeners();
            btnVibration.onClick.AddListener(() => {
                isEnableHaptic = !isEnableHaptic;
                PlayerPrefs.SetInt("isEnableHaptic", isEnableHaptic ? 0 : 1);
                btnVibration.image.sprite = isEnableHaptic ? Helper.Instance.toggleOn : Helper.Instance.toggleOff;

                SoundController.Instance.PlaySound(SoundsList.Checkbox, 0.7f);
            });

            btnMusic.onClick.RemoveAllListeners();
            btnMusic.onClick.AddListener(() => {
                isEnableMusic = !isEnableMusic;
                PlayerPrefs.SetInt("isEnableMusic", isEnableMusic ? 0 : 1);
                btnMusic.image.sprite = isEnableMusic ? Helper.Instance.toggleOn : Helper.Instance.toggleOff;

                SoundController.Instance.PlaySound(SoundsList.Checkbox, 0.7f);
            });


            btnPrivacyPolicy.onClick.RemoveAllListeners();
            btnPrivacyPolicy.onClick.AddListener(() => { InAppBrowser.OpenBrowser("https://dpspace.com/privacy-policy"); });

            isEnableHaptic = PlayerPrefs.GetInt("isEnableHaptic", 0) == 0;
            isEnableMusic = PlayerPrefs.GetInt("isEnableMusic", 0) == 0;

            btnMusic.image.sprite = isEnableMusic ? Helper.Instance.toggleOn : Helper.Instance.toggleOff;
            btnVibration.image.sprite = isEnableHaptic ? Helper.Instance.toggleOn : Helper.Instance.toggleOff;
        }

        public void SetActiveViewBlockOptions(bool var) {
            if (var) {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.setting);

                // Скрытие меню
                MainMenu.DOFade(0, 0.2f);
                MainMenu.interactable = false;
                MainMenu.blocksRaycasts = false;

                // Начальная настройка
                MainBox.localScale = new Vector3(0.85f, 0.85f, 1);
                FadeWindow.gameObject.SetActive(true);
                viewBlockOptions.anchoredPosition = Vector2.zero;

                // Анимация размера
                DOTween.Sequence()
                    .Append(MainBox.transform.DOScale(1.07f, 0.25f))
                    .Append(MainBox.transform.DOScale(1f, 0.15f));

                // Анимация прозрачности
                FadeWindow.DOFade(1, 0.2f).OnComplete(() => {
                    // Включение кнопок
                    FadeWindow.interactable = true;
                    FadeWindow.blocksRaycasts = true;
                });
            } else {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.mainMenu);
                // Показ меню
                DOTween.Sequence().AppendInterval(0.05f)
                    .Append(MainMenu.DOFade(1, 0.2f))
                    .AppendCallback(() => {
                        // Включение кнопок
                        MainMenu.interactable = true;
                        MainMenu.blocksRaycasts = true;
                    });

                GUIStartModal.Instance.StartFromController(true);

                // Скрытие
                MainBox.transform.DOScale(0.85f, 0.25f);
                FadeWindow.DOFade(0, 0.25f).OnComplete(() => {
                    // Отключение кнопок
                    FadeWindow.interactable = false;
                    FadeWindow.blocksRaycasts = false;

                    // Отключение окна
                    FadeWindow.gameObject.SetActive(false);

                });
            }
        }
    }
}