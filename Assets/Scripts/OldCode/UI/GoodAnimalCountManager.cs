﻿using System;

public class GoodAnimalCountManager : MonoSingleton<GoodAnimalCountManager> {

	[ReadOnly] private int _totalCount;

	private int _currentCount;
	private int currentCount {
		get { return _currentCount; }
		set {
			_currentCount = value;
			ActionCurrentCount.SafeInvoke(value);
		}
	}

	public Action<int> ActionCurrentCount;

	// Установка максимального количества
	public void SetCount() {
		currentCount = 0;
		_totalCount = GoodAnimalManager.Single.AllAnimal.Count;
	}

	public int GetCountSheepInHerd() {
		return currentCount;
	}

	public int GetTotalCountSheep() {
		return _totalCount;
	}

	// Обновление счетчика
	public bool UpdateCount() {
		currentCount = GoodAnimalManager.Instance.AnimalInDog.Count;

		// Все собраны или нет
		return _totalCount == currentCount;
	}

	// Овца потеряна
	public void LostUpdateCount() {
		//currentCount--;
	}

}
