﻿using DG.Tweening;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIFadeBack : MonoSingleton<GUIFadeBack> {
        [SerializeField] private Image imgFadeBackground;
        private Sequence _sequence;
        private readonly Color _colorFadeBackground = new Color(0.012f, 0.161f, 0.643f, 0.431f);

        public void Start() {
            //SetColor(_colorFadeBackground, 0);
        }

     /*   [Button]
        public void TestColor() {
            Debug.LogError(imgFadeBackground.color);
        }*/

        [Button]
        public void SetEnable() {
            Instance.SetColor(_colorFadeBackground, 0.5f);
        }


        [Button]
        public void SetDisable() {
            Instance.SetColor(GP.colorZero, 0.5f);
        }


        public void SetColor(Color colorFadeBackground, float time) {
            if (_sequence != null) {
                _sequence.Kill();
            }

            _sequence = DOTween.Sequence();
            _sequence.Append(imgFadeBackground.DOColor(colorFadeBackground, time));
        }
    }
}
