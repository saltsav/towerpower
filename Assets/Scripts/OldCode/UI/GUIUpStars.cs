﻿using DG.Tweening;
using EasyButtons;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Assets.Scripts.Core {
    public class GUIUpStars : MonoSingleton<GUIUpStars> {

        [SerializeField] bool isTimer = false;

        public RectTransform viewBlockStars;
        public Slider SheepSlider;

        public Button btnPause;

        public int sheepsCount;
        public int maxSheepsCount;
        public int coinsCount;

        private float currentScore {
            get; set;
        }

        private readonly Vector2 hidePosBlockStars = new Vector2(0, 495); // anchorPos
        private readonly Vector2 showPosBlockStars = new Vector2(0, 0); // anchorPos

        public Text textCoins;
        public Text textSheeps;

        public Text textTime;
        public float currentTime;


        public void Start() {

            textTime.transform.parent.gameObject.SetActive(isTimer);

            maxSheepsCount = 20;
            sheepsCount = 1;
            btnPause.onClick.RemoveAllListeners();
            btnPause.onClick.AddListener(() => {
                //MainLogic.Instance.SetStateGame(StateGame.pause);
                SetActiveBtnPause(false);
                GUIModalPause.Instance.SetActiveViewBlockPause(true);
            });

            SetActiveBtnPause(false);
        }

        public void Update() {

            if (isTimer) {
               /* if (MainLogic.Instance.GetStateGame() == StateGame.play) {
                    currentTime -= Time.deltaTime;
                    textTime.text = ConvertSekToMMSS(currentTime);
                    if (currentTime < 0) {
                        GUIResultWindow.Instance.ShowResultWindow();
                    }
                }*/
            }
        }

        public void StartFromController() {
            Resets();
            SetActiveViewBlockStars(true);
            maxSheepsCount = GoodAnimalManager.Instance.GetAllSheep();
            SetTextCoins(0);
            SetTextSheep(0);
            GoodAnimalCountManager.Instance.ActionCurrentCount += SetNewCountSheep;
            CoinCountManager.Instance.ActionOnThisLevel += SetNewCountCoin;
        }

        public void StopFromController() {
            GoodAnimalCountManager.Instance.ActionCurrentCount -= SetNewCountSheep;
            CoinCountManager.Instance.ActionOnThisLevel -= SetNewCountCoin;
            SetActiveViewBlockStars(false);
            SetActiveBtnPause(false);
        }

        public void SetNewCountSheep(int addCount) {
            sheepsCount = addCount;
            SetTextSheep(sheepsCount);

            UpdateLineSheep();
        }

        public void SetNewCountCoin(int addCount) {
            coinsCount = addCount;
            SetTextCoins(coinsCount);
        }


        public void UpdateLineSheep() {

            SheepSlider.DOValue((float) sheepsCount / (float) maxSheepsCount, 0.3f);
        }


        public void SetActiveViewBlockStars(bool var) {
            if (var) {
                viewBlockStars.DOAnchorPos(showPosBlockStars, 0.5f);
            } else {
                viewBlockStars.DOAnchorPos(hidePosBlockStars, 0.5f);
            }
        }

        [Button]
        public void Resets() {
            currentTime = 180;
            SheepSlider.value = 0;
            sheepsCount = 0;
        }

        public void SetTextCoins(int var) {

            DOVirtual.Float(220, 300, 0.075f, (size) => {
                textCoins.fontSize = (int) size;
                textCoins.text = var.ToString();

                DOVirtual.Float(260, 220, 0.085f, (sizeBack) => {
                    textCoins.fontSize = (int) sizeBack;
                });
            });
        }

        public void SetTextSheep(int var) {
            textSheeps.text = var + "/" + GoodAnimalCountManager.Instance.GetTotalCountSheep();
        }


        public void SetActiveBtnPause(bool var) {
            if (var) {
                btnPause.interactable = true;
                //  ButtonImage.DOFade(1, 0.15f);
            } else {
                btnPause.interactable = false;
                // ButtonImage.DOFade(0, 0.15f);
            }
        }

        static public string ConvertSekToMMSS(float x) {
            TimeSpan timerTemp = TimeSpan.FromSeconds(x);
            return string.Format("{0:D2}:{1:D2}", timerTemp.Minutes, timerTemp.Seconds);
        }
    }
}