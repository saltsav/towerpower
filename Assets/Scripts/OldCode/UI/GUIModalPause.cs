﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Core {
    public class GUIModalPause : MonoSingleton<GUIModalPause> {
        public RectTransform viewBlockPause;

        public Button btnQuit;
        public Button btnOk;
        //public Button btnClose;

        [SerializeField] private CanvasGroup FadeWindow;
        [SerializeField] private Transform MainBox;

        public bool isShow;

        public void Awake() {
            // Начальная настройка
            FadeWindow.gameObject.SetActive(false);
            FadeWindow.interactable = false;
            FadeWindow.blocksRaycasts = false;
            FadeWindow.alpha = 0;
        }

        public void Start() {
            btnOk.onClick.RemoveAllListeners();
            btnOk.onClick.AddListener(() => {
                SetActiveViewBlockPause(false);
                GUIStartModal.Instance.StartFromController(false);
                //MainLogic.Instance.SetStateGame(StateGame.play);
            });

            btnQuit.onClick.RemoveAllListeners();
            btnQuit.onClick.AddListener(() => {
                //  Показ рекламы
                //SetActiveViewBlockPause(false);
                //GUIResultWindow.Instance.ShowResultWindow();
                // GUIStartModal.Instance.StartFromController(false);
                // EndAds(true);
                /* AdsController.Single.VideoStatusAction += EndAds;
                 AdsController.Single.ShowInterstitialAd();
                 FadeWindow.interactable = false;*/

                //GameManager.Single.RestartGame();
            });
        }

        // После завершения рекламы
        private void EndAds(bool status) {
            //AdsController.Single.VideoStatusAction -= EndAds;
            //TutorialController.Instance.needShowTutorial = false;
            FadeWindow.interactable = true;
            GUIStartModal.Instance.StopFingerAnimation();
            // MainLogic.Instance.HardStopGame();
            SetActiveViewBlockPause(false, true);

        }

        public void SetActiveViewBlockPause(bool var, bool isEnd = false) {
            if (isShow == var) {
                return;
            }

            isShow = var;
            if (var) {
                AndroidBackButton.Instance.SetStateInApplication(StateInApplication.pause);
                // Начальная настройка
                MainBox.localScale = new Vector3(0.85f, 0.85f, 1);
                FadeWindow.gameObject.SetActive(true);
                viewBlockPause.anchoredPosition = Vector2.zero;

                // Анимация размера
                DOTween.Sequence()
                    .Append(MainBox.transform.DOScale(1.07f, 0.25f))
                    .Append(MainBox.transform.DOScale(1f, 0.15f));

                // Анимация прозрачности
                FadeWindow.DOFade(1, 0.2f).OnComplete(() => {
                    // Включение кнопок
                    FadeWindow.interactable = true;
                    FadeWindow.blocksRaycasts = true;
                });
                //GUIStartModal.Instance.StopFingerAnimation();
                /*if (SpawnerController.Instance.spawnerStrategy != null) {
                    SpawnerController.Instance.spawnerStrategy.StopCoroWaitMoveSandwiches();
                }*/

                //  MainLogic.Instance.SetPauseOnGame(true);
            } else {
                // Скрытие
                MainBox.transform.DOScale(0.85f, 0.25f);
                FadeWindow.DOFade(0, 0.25f).OnComplete(() => {
                    // Отключение кнопок
                    FadeWindow.interactable = false;
                    FadeWindow.blocksRaycasts = false;

                    // Отключение окна
                    FadeWindow.gameObject.SetActive(false);


                    /* if (!isEnd) { // и это ещё не конец игры
                         GUIStartModal.Instance.StartFromController(false);
                     } else {
                     //    MainLogic.Instance.SetPauseOnGame(false, false); //отключить паузу тут а не после пальца
                     }*/


                    // Не включать музыку при выходе
                    if (isEnd) {
                        //  SoundController.Instance.StopSound();
                    }
                });
            }
        }
    }
}