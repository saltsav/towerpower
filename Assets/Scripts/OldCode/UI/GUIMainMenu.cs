﻿using Assets.Scripts.Core;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class GUIMainMenu : MonoSingleton<GUIMainMenu> {
        public Button btnOptions;
        public Text textCoins;
        public Text textLevel;
        public RectTransform panelMainMenu;
        public CanvasGroup ToAlpha;

        public Button plus;
        public Button minus;

        public void Start() {
            plus.onClick.RemoveAllListeners();
            plus.onClick.AddListener(() => {
                PlayerInfoManager.Instance.SaveCurrentLvl(PlayerInfoManager.Instance.GetCurrentLvl() + 1);
                //GameManager.Single.RestartGame();
            });

            minus.onClick.RemoveAllListeners();
            minus.onClick.AddListener(() => {
                if (PlayerInfoManager.Instance.GetCurrentLvl() == 1) {
                    return;
                }

                PlayerInfoManager.Instance.SaveCurrentLvl(PlayerInfoManager.Instance.GetCurrentLvl() - 1);
                //GameManager.Single.RestartGame();
            });


            btnOptions.onClick.RemoveAllListeners();
            btnOptions.onClick.AddListener(() => { GUIOptions.Instance.SetActiveViewBlockOptions(true); });
            UpdateInfo();
        }

        public void UpdateInfo() {
            textCoins.text = PlayerInfoManager.Instance.GetCoinCount().ToString();
            textLevel.text = "LEVEL " + PlayerInfoManager.Instance.GetCurrentLvl();
        }

        public void HiddenMenu() {
            ToAlpha.blocksRaycasts = false;
            ToAlpha.interactable = false;
            ToAlpha.DOFade(0, 0.25f).OnComplete(() => {
                ToAlpha.gameObject.SetActive(false);
            });
        }
    }
}