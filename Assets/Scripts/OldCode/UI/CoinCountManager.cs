﻿using System;
using Assets.Scripts;

public class CoinCountManager : MonoSingleton<CoinCountManager> {

    int _onThisLevel;
    int onThisLevel {
        get {
            return _onThisLevel;
        }
        set {
            _onThisLevel = value;
            ActionOnThisLevel?.Invoke(value);
        }
    }

    public Action<int> ActionOnThisLevel;

    public void StartFromController() {
        onThisLevel = 0;
    }

    public int GetCoinOnThisLevel() {
        return onThisLevel;
    }

    public void UpdateCount(int count) {
        onThisLevel += count;
    }
}
