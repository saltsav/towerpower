﻿using UnityEngine;
using UnityEngine.EventSystems;
using Assets.Scripts.Core;

public class SoundOnButton : MonoBehaviour, IPointerUpHandler {
	public void OnPointerUp(PointerEventData evd) {
		SoundController.Instance.PlaySound(SoundsList.Button, 0.7f);
	}
}
