﻿using Assets.Scripts.Core;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusMoneyIcon : MonoBehaviour {

    static List<PlusMoneyIcon> ListIcons;

    [SerializeField] GameObject IconGO;
    [SerializeField] CanvasGroup ToAlpha;

    [NonSerialized] public Transform Target;

    // Добавление в стек
    void OnEnable() {
        if (ListIcons == null) ListIcons = new List<PlusMoneyIcon>();
        IconGO.SetActive(false);
        ListIcons.Add(this);
    }

    // Удалить из стека
    void OnDisable() {
        if (ListIcons != null) ListIcons.Remove(this);
        UpdateManager.UpdateThis -= UpdateThis;
    }

    // Перемещение за целью
    void UpdateThis() {
        transform.position = Target.position;
    }

    // Анимация
    public void ShowIcon(float time) {
        UpdateManager.UpdateThis += UpdateThis;
        IconGO.transform.localScale = Vector3.zero;
        IconGO.SetActive(true);
        ToAlpha.alpha = 0;

        DOVirtual.DelayedCall(time, () => {
            CoinCountManager.Single.UpdateCount(10);

            IconGO.transform.DOMove(IconGO.transform.position + Vector3.up * 2, 0.8f);

            DOTween.Sequence()
                .Append(ToAlpha.DOFade(1, 0.25f))
                .AppendInterval(0.3f)
                .Append(ToAlpha.DOFade(0, 0.25f)).OnComplete(() => {
                    IconGO.SetActive(false);
                });

            DOTween.Sequence()
                .Append(IconGO.transform.DOScale(Vector3.one * 1.5f, 0.35f))
                .AppendInterval(0.2f)
                .Append(IconGO.transform.DOScale(Vector3.zero, 0.55f));

        });
    }

    // Запуск звука монетки
    public void PlaySoundMoney(float timeCancel, float delay) {
        InvokeRepeating("Sound", delay, 0.18f);
        DOVirtual.DelayedCall(timeCancel + 0.25f, () => {
            CancelInvoke();
        });
    }
    void Sound() {
        SoundController.Instance.PlaySound(SoundsList.MoneySlider, 0.3f);
    }

    // Показать на финише
    public static float ShowIcons() {

        float iconsCount = ListIcons.Count;
        if (iconsCount == 0) return 0.25f;

        float delay = 0.35f;
        float time = 0;
        float timeToAdd = 1.5f / iconsCount;
        timeToAdd = Mathf.Clamp(timeToAdd, 0.03f, 0.15f);

        // Запуск звука
        ListIcons[0].PlaySoundMoney(timeToAdd * iconsCount, delay);

        // Запуск анимации
        foreach (PlusMoneyIcon icon in ListIcons) {
            icon.ShowIcon(delay + time);
            time += timeToAdd;
        }
        ListIcons = null;
        return 0.35f + timeToAdd * iconsCount;
    }

    // Очистка
    public static void ResetIcons() {
        ListIcons = null;
    }
}
