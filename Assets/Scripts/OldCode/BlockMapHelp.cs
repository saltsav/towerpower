﻿using System;
using System.Collections.Generic;
using Assets.Maze;
using UnityEngine;

[Serializable]
public class BlockMapHelp {
    public bool iHaveInfo;

    public int x;
    public int z;
    public CellMaze cellMaze;
    public InfoOneBigBlockMap infoOneBigBlockMap;

    public TypeBigBlockMap typeBigBlockMap;

    public int finalRotate;
    public List<TypeConnectionSide> listType;

    public List<TypeConnectionSide> usedSides = new List<TypeConnectionSide>();

    /*public void RotateLastBlockForBefore() {
        TypeConnectionSide t1 = MapGenerator.Instance.GetBeforeBlockSide(cellMaze.x, cellMaze.z, cellMaze.beforeCellMazeX, cellMaze.beforeCellMazeZ);
        switch (t1) {
            case TypeConnectionSide.up:
                finalRotate = -90;
                break;
            case TypeConnectionSide.down:
                finalRotate = -270;
                break;
            case TypeConnectionSide.left:
                finalRotate = -180;
                break;
            case TypeConnectionSide.right:
                finalRotate = 0;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }*/

    public void RotateThisBlockFor(int cellMazeX, int CellMazeZ) {
        TypeConnectionSide t1 = MapGenerator.Instance.GetBeforeBlockSide(cellMaze.x, cellMaze.z, cellMazeX, CellMazeZ);
        switch (t1) {
            case TypeConnectionSide.up:
                finalRotate = -90;
                break;
            case TypeConnectionSide.down:
                finalRotate = -270;
                break;
            case TypeConnectionSide.left:
                finalRotate = -180;
                break;
            case TypeConnectionSide.right:
                finalRotate = 0;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void AddForUsedSideList(TypeConnectionSide typeConnectionSide) {
        foreach (TypeConnectionSide connectionSide in usedSides) {
            if (connectionSide == typeConnectionSide) {
                return;
            }
        }
        usedSides.Add(typeConnectionSide);
    }
}