﻿using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Core {
	public class Helper : MonoSingleton<Helper> {
	    public Sprite toggleOn;
	    public Sprite toggleOff;

		public static void WriteString(string str, string nameFile) {
			if (Application.platform == RuntimePlatform.WindowsEditor) {
				string path = "Assets/Resources/" + nameFile + ".txt";

				if (!File.Exists(path)) {
					using (FileStream fs = File.Create(path)) {
						byte[] info = new UTF8Encoding(true).GetBytes("");
						//Add some information to the file.
						fs.Write(info, 0, info.Length);
					}

					//File.WriteAllText(path, "");
				}

				using (FileStream stream = new FileStream(path, FileMode.Truncate)) {
					using (StreamWriter writer1 = new StreamWriter(stream)) {
						writer1.Write(str);
						writer1.Close();
					}
				}
			}
		}

		public static string LoadString(string nameFile) {
			if (Application.platform == RuntimePlatform.WindowsEditor) {
				string path = "Assets/Resources/" + nameFile + ".txt";
				if (File.Exists(path)) {
					string str = File.ReadAllText(path, Encoding.UTF8);
					return str;
				}
			}

			return string.Empty;
		}


		public static string ConvertToStringSpace(string var) {
			string str = string.Empty;
			int index = 1;
			for (int i = var.Length - 1; i >= 0; i--) {
				str += var[i];
				if (index % 3 == 0 && index != 0 && i != 0 && var[i + 1].ToString() != "-") {
					str += " ";
				}

				index++;
			}

			string str1 = string.Empty;

			for (int i = str.Length - 1; i >= 0; i--) {
				str1 += str[i];
			}

			return str1;
		}

	    public static void SaveStringKey(string key, string str) {
            PlayerPrefs.SetString(key, str);
	    }

	    public static string LoadStringKey(string key) {
	       return PlayerPrefs.GetString(key, "");
	    }

	    public static void SaveFloatKey(string key, float var) {
	        PlayerPrefs.SetFloat(key, var);
	    }

	    public static float LoadFloatKey(string key) {
	        return PlayerPrefs.GetFloat(key, 0.3f);
	    }


	    public static void WriteStringFullPath(string str, string nameFile, string path1) {
	            string path = path1 + "/" + nameFile;
                //Debug.LogError(path);
	            if (!File.Exists(path)) {
	                using (FileStream fs = File.Create(path)) {
	                    byte[] info = new UTF8Encoding(true).GetBytes("");
	                    //Add some information to the file.
	                    fs.Write(info, 0, info.Length);
	                }

	                //File.WriteAllText(path, "");
	            }

	            using (FileStream stream = new FileStream(path, FileMode.Truncate)) {
	                using (StreamWriter writer1 = new StreamWriter(stream)) {
	                    writer1.Write(str);
	                    writer1.Close();
	                }
	            }
	    }
	}

}