﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Constant {

    public static float floatZero = 0.00001f;

    // Главная камера
    private static Camera _getMainCamera;
    public static Camera GetMainCamera {
        get {
            if (_getMainCamera == null) _getMainCamera = Camera.main;
            return _getMainCamera;
        }
    }

    // Эвент систем
    private static GraphicRaycaster _eventSystem;
    public static void EventSystem(bool status) {
        if (_eventSystem == null) _eventSystem = GameObject.Find("MainMenu").GetComponent<GraphicRaycaster>();
        _eventSystem.enabled = status;
    }
}


