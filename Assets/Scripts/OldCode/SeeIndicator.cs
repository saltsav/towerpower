﻿using System;
using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts {
    public class SeeIndicator : MonoBehaviour {
        public SphereCollider targer;
        public float lastRadius;

        public void Start() {
            lastRadius = targer.radius;
            transform.localScale = Vector3.one * 2 * lastRadius;

            transform.position = targer.transform.position;

            OptionsController.Instance.actionShowSphere += (var) => { GetComponent<MeshRenderer>().enabled = var; };
            GetComponent<MeshRenderer>().enabled = OptionsController.Instance.typeShowSphere;
        }

        public void Update() {
            if (!OptionsController.Instance.typeShowSphere) {
                return;
            }
            if (targer != null && Math.Abs(targer.radius - lastRadius) > GP.floatZero) {
                lastRadius = targer.radius;
                transform.localScale = Vector3.one * 2 * lastRadius; 
            }

            if (targer != null && Math.Abs(targer.transform.position.z - transform.position.z) > GP.floatZero) {
                transform.position = targer.transform.position;
            }
        }
    }
}