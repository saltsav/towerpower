﻿using System;
using Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
   /* public enum ParamsGP {
        speedDog, //скррость собаки
        sizeDogView, //размер зоны видимости собаки
        sheepRadiusView, //радиус зрения овцы
        timeRememberDog, //время памяти о собаке
        speedSheepMoveIdle, //скорость овцы в спокойном состоянии
        timeOnScared, //время в испуге
        distanceCamera,
        scaleMap
    }*/

    public class BlockChangeParams : MonoBehaviour {
       /* public ParamsGP paramsGp = ParamsGP.speedDog;

        public InputField inputField;
        public Text nameParams;
        public Text defaultParams;

        public int value;

        public void Awake() {
            defaultParams.resizeTextForBestFit = true;
            defaultParams.resizeTextMaxSize = 30;
            nameParams.fontSize = 30;
            nameParams.resizeTextMaxSize = 30;
            nameParams.text = GetNameParamsByType();
            defaultParams.text = "(" + GetDefaultParams() + ")";

            value = LoadParams();
            inputField.placeholder.GetComponent<Text>().text = value.ToString();

            SetNewParams(value);

            inputField.onValueChanged.AddListener(var => {
                int i;
                if (int.TryParse(var, out i)) {
                    SetNewParams(i);
                    SaveParams(i);
                } else {
                    SetNewParams(value);
                    SaveParams(value);
                }
            });
        }

        public int LoadParams() {
            return PlayerPrefs.GetInt(paramsGp.ToString(), GetDefaultParams());
        }

        public void SaveParams(int newValue) {
            PlayerPrefs.SetInt(paramsGp.ToString(), newValue);
        }

        public string GetNameParamsByType() {
            switch (paramsGp) {
                case ParamsGP.speedDog:
                    return "Скорость собаки";
                case ParamsGP.sheepRadiusView:
                    return "Радиус зрения овцы";
                /*case ParamsGP.timeRememberDog:
                    return "Время памяти о собаке";
                case ParamsGP.speedSheepMoveIdle:
                    return "Скорость спокойной овцы";
                case ParamsGP.distanceCamera:
                    return "Расстояние до камеры";
                case ParamsGP.scaleMap:
                    return "Масштаб карты";
                case ParamsGP.sizeDogView:
                    return "Размер зоны видимости собаки";
                case ParamsGP.timeOnScared:
                    return "Макс время в испуга овцы";
            }

            return "";
        }


        public void SetNewParams(int newValue) {
            switch (paramsGp) {
                case ParamsGP.speedDog:
                    GP.speedDog = newValue;
                    break;
                case ParamsGP.sheepRadiusView:
                    GP.sheepRadiusView = newValue;
                    break;
                /*case ParamsGP.timeRememberDog:
                    GP.timeRememberDog = newValue;
                    break;
                case ParamsGP.speedSheepMoveIdle:
                    GP.speedSheepMoveIdle = newValue;
                    break;
                case ParamsGP.distanceCamera:
                    GP.distanceCamera = newValue;
                    break;
                case ParamsGP.scaleMap:
                    GP.scaleMap = newValue;
                    break;
                case ParamsGP.sizeDogView:
                    GP.sizeDogView = newValue;
                    break;
                case ParamsGP.timeOnScared:
                    GP.timeOnScared = newValue;
                    break;
            }
        }

        public int GetDefaultParams() {
            switch (paramsGp) {
                case ParamsGP.speedDog:
                    return GP.speedDog;
                case ParamsGP.sheepRadiusView:
                    return GP.sheepRadiusView;
                /*case ParamsGP.timeRememberDog:
                    return GP.timeRememberDog;
                case ParamsGP.speedSheepMoveIdle:
                    return GP.speedSheepMoveIdle;
                case ParamsGP.distanceCamera:
                    return GP.distanceCamera;
                case ParamsGP.scaleMap:
                    return GP.scaleMap;
                case ParamsGP.sizeDogView:
                    return GP.sizeDogView;
                case ParamsGP.timeOnScared:
                    return GP.timeOnScared;
            }

            return 0;
        }*/
    }
}