﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Assets.Scripts {
	public class BadCow : MonoBehaviour {//Злой бычок - пока не используется
		/*public List<Transform> listPointPath;
		public int i;
		[ReadOnly] public Transform currentPoint;

		public NavMeshAgent navMeshAgent;

		[SerializeField] public Transform viewParent;
		[ReadOnly] public AnimalView currentAnimalView;
		[ReadOnly] public TypeAnimal currentTypeAnimal;
		private Vector3 previousPosition;
		[ReadOnly] public float realSpeed;

		public float timeSwapPoint;

		private float timeForStart = 0.5f;

		public float _timeForChangeState;

		public void Start() {
			gameObject.SetActive(false);

			navMeshAgent.enabled = true;
			SetView(TypeAnimal.badCow);
			i = 0;
			currentPoint = listPointPath[0];
		}

		public void OnEnable() {
			UpdateManager.UpdateThis += OnUpdateFromManager;
		}

		public void OnDisable() {
			UpdateManager.UpdateThis -= OnUpdateFromManager;
		}

		public void SetView(TypeAnimal typeAnimal) {
			currentTypeAnimal = typeAnimal;
			if (currentAnimalView != null) {
				Destroy(currentAnimalView.gameObject);
			}

			currentAnimalView = AnimalsModelController.Instance.GetAnimalViewByType(currentTypeAnimal);
		    currentAnimalView.transform.SetParent(viewParent);
			currentAnimalView.transform.localPosition = Vector3.zero;
		    currentAnimalView.transform.localEulerAngles = Vector3.zero;
			currentAnimalView.gameObject.SetActive(true);
		}

		public bool CheckDistance() {
			if (currentPoint == null) {
				return true;
			}
			float d = (navMeshAgent.transform.position - currentPoint.position).sqrMagnitude;
			return d < 10;
		}

		public void OnUpdateFromManager() {
			timeSwapPoint -= Time.deltaTime;
			if (CheckDistance() && timeSwapPoint < 0) {
				timeSwapPoint = 2;
				i++;
				if (i == listPointPath.Count) {
					i = 0;
				}
				currentPoint = listPointPath[i];
			}

			if (navMeshAgent.gameObject.activeInHierarchy && navMeshAgent.enabled) {
				navMeshAgent.SetDestination(currentPoint.position);
			}


			timeForStart -= Time.deltaTime;
			if (currentAnimalView != null && timeForStart <= 0) {
				if (Math.Abs(realSpeed) > 0.1f) {
					currentAnimalView.SetAnimatorState(GlobalAnimState.run);
				} else {
					currentAnimalView.SetAnimatorState(GlobalAnimState.idle);
				}
			}

			Vector3 curMove = navMeshAgent.transform.position - previousPosition;
			realSpeed = curMove.magnitude / Time.deltaTime;
			previousPosition = navMeshAgent.transform.position;
		}


		public void SetStateOnIdle() {
			_timeForChangeState -= Time.deltaTime;
			if (_timeForChangeState > 0) {
				return;
			}

			_timeForChangeState = Random.Range(4, 8);
			int r = Random.Range(0, 2);
			switch (r) {
				case 0:
					currentAnimalView.SetAnimatorState(GlobalAnimState.idle);
					break;
				case 1:
					currentAnimalView.SetAnimatorState(GlobalAnimState.custom);
					break;
			}
		}


		// При косании
		private void OnTriggerEnter(Collider other) {
			if (other.gameObject.name == "Dog") {
				DogGO.Instance.DogTachedBadCow();
			}
		}*/
	}
}