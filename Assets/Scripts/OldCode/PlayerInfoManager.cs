﻿using UnityEngine;

namespace Assets.Scripts {
    public class PlayerInfoManager : MonoSingleton<PlayerInfoManager> {
        public const string coinCount = "coinCount";
        public const string currentLvl = "currentLvl";

        public int testLvl;

        private int startSheep = 10;
        private float sheepMulti = 0.04f;
        private float badAnimalFactor = 0.13f;
        private float factorUnitOnBlock = 3.5f;
        private int minCountBlocks = 4;

        private int currentLevel;
        public int countBadAnimal;
        public int countGoodAnimal;
        public int countBlocks;
        public int countCoins;

        public void Awake() {
            CalculationData();
        }

        public void CalculationData() {
            currentLevel = GetCurrentLvl();
            countGoodAnimal = GetCountSheepForLvl(currentLevel);
            countBadAnimal = GetCountBadAnimal(currentLevel);
            countBlocks = GetCountBlocks(currentLevel);
            countCoins = countGoodAnimal / 2;
        }

        [EasyButtons.Button]
        public void TestLvl() {
            SaveCurrentLvl(testLvl);
            //GameManager.Single.RestartGame();
        }

        public int GetCurrentLvl() {
            return PlayerPrefs.GetInt(currentLvl, 1);
        }

        public void SaveCurrentLvl(int newLvl) {
            PlayerPrefs.SetInt(currentLvl, newLvl);
        }

        public int GetCoinCount() {
            return PlayerPrefs.GetInt(coinCount, 0);
        }

        public void SaveCoinCount(int newCount) {
            PlayerPrefs.SetInt(coinCount, newCount);
        }

        public void AddToSaveCoinCount(int addCount) {
            PlayerPrefs.SetInt(coinCount, GetCoinCount() + addCount);
        }

        private int GetCountSheepForLvl(int lvl) {
            if (lvl > 100) {
                lvl = 100;
            }

            float t = startSheep + (startSheep * (sheepMulti * lvl));
            return (int) t;
        }


        private int GetCountBadAnimal(int lvl) {
            if (lvl == 1) {
                return 0;
            }
            if (lvl > 100) {
                lvl = 100;
            }
            float b = GetCountSheepForLvl(lvl);
            float t = Mathf.Round(b * badAnimalFactor);
            return (int) t;
        }

        private int GetCountBlocks(int lvl) {
            if (lvl > 100) {
                lvl = 100;
            }
            int s = GetCountSheepForLvl(lvl) + GetCountBadAnimal(lvl);
            int t = (int) (Mathf.Round((float) s / factorUnitOnBlock));
            if (t < minCountBlocks) {
                t = minCountBlocks;
            }

            return t;
        }
    }
}