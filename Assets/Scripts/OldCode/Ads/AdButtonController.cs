﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Specialized;

public enum TypeWindow {
    FinalWindow,
    OfflineWindow
}

public class AdButtonController : MonoBehaviour {

    [SerializeField] ListRewardAds TypeAds;
    [SerializeField] GameObject OnButton;
    [SerializeField] GameObject OffButton;

    // При показе
    void OnEnable() {
        AdRewardController.Single.VideoCompleted += StatusChanged;
        AdRewardController.Single.InternetChanges += StatusChanged;
        InvokeRepeating("CheckButton", 0, 0.9f);
    }

    // Проверка доступности рекламы
    void StatusChanged(bool status) {
        CheckButton();
    }
    public void CheckButton() {
        bool status = true;
        if (AdRewardController.Single.isAvailableReward(TypeAds)) {
            status = true;
        } else {
            status = false;
        }
        OnButton.SetActive(status);
        OffButton.SetActive(!status);
    }
}
