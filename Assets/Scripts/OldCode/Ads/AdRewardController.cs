﻿using System.Collections;
using UnityEngine;
using System;
//using KWCore;

public enum ListRewardAds {
    FinishWindow
}

public class AdRewardController : MonoSingleton<AdRewardController> {
    public event Action<bool> InternetChanges;
    public event Action<bool> VideoCompleted;
    static public bool InternetConnection;
    static public bool RewardAvailable;

    bool _rewardOk;

    // Проверка при старте
    private void Awake() {
#if !UNITY_EDITOR
		AppLovin.SetUnityAdListener("AdController"); // Имя GameObject
#endif
        CheckConnection();
    }

    // Показ банера рекламы
    public virtual void ShowBannerAd(bool enable) {
        if (PlayerPrefs.GetInt("TestF") != 2932) {
            //Umbrella.Instance.Ads.SetBannerAdsEnabled(enable);
        }
    }

    // Показ обязательной рекламы
    public virtual void ShowInterstitialAd(string type) {
        if (PlayerPrefs.GetInt("TestF") != 2932 && isAvailableInterst(type)) {
            //Umbrella.Instance.Ads.ShowInterstitialAd(type);
            Invoke("EnableTouch", 2);
        }
    }


    // Показ награды вознаграждения
    public bool ShowRewardAd(ListRewardAds type) {
        if (!InternetConnection) {
            //NoConnectionController.Single.OpenWindow();
            return false;
        }

#if !UNITY_EDITOR
		if (AppLovin.IsIncentInterstitialReady()) {
			_rewardOk = false;
            Constant.EventSystem(false);
			AppLovin.ShowRewardedInterstitial();
			return true;
		}
#endif
        return false;
    }

    // Выдача награды за вознаграждение
    void SendCompleted() {
        VideoCompleted.SafeInvoke(true);
        VideoCompleted = null;
    }

    // Не удалось посмотреть рекламу
    void AdsFailed() {
        VideoCompleted.SafeInvoke(false);
        VideoCompleted = null;
    }

    // Получение обратной связи
    void onAppLovinEventReceived(string ev) {

        if (ev.Contains("REWARDAPPROVEDINFO")) {
            _rewardOk = true;
        } else if (ev.Contains("HIDDENREWARDED")) {
            if (_rewardOk) {
                SendCompleted();
            } else {
                AdsFailed();
            }
            //AppLovin.LoadRewardedInterstitial();
        } else if (ev.Contains("REWARDREJECTED")
            || ev.Contains("REWARDTIMEOUT")
            || ev.Contains("USERCLOSEDEARLY")) {

            AdsFailed();
        }
    }


    // Доступна реклама или нет
    public bool isAvailableInterst(string type) {
        return false;
    }

    // Доступна реклама или нет
    public bool isAvailableReward(ListRewardAds type) {
        if (!InternetConnection) return true;

#if !UNITY_EDITOR
		RewardAvailable = AppLovin.IsIncentInterstitialReady();
		return RewardAvailable;
#endif
        return false;
    }


    //
    protected override void OnDestroy() {
        this.StopAllCoroutines();
        base.OnDestroy();
    }

    //
    void EnableTouch() {
        Constant.EventSystem(true);
    }

    // Проверка соединения
    public void CheckConnection() {
        this.StartCoroutine("checkInternetConnection");
    }
    private IEnumerator checkInternetConnection() {
        bool status;
        do {
            try {
                status = Application.internetReachability != NetworkReachability.NotReachable;
                if (InternetConnection != status) {
                    InternetConnection = status;
                    InternetChanges.SafeInvoke(status);
                }

                if (InternetConnection && !RewardAvailable) {
#if !UNITY_EDITOR
					if(AppLovin.IsIncentInterstitialReady()){
						InternetChanges.SafeInvoke(status);
					}
#endif
                }
            } finally { }
            yield return new WaitForSecondsRealtime(1.5f);

        } while (true);
    }
}
