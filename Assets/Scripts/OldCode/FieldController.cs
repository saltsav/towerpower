﻿using Assets.Scripts;
using UnityEngine;

namespace Assets.Core.ScriptsHerd {
	public class FieldController : MonoSingleton<FieldController> {
		public Transform upLeftCorner;
		public Transform downRightCorner;

		public Transform pointForDis1;
		public Transform pointForDis2;

		public float disForFindPath;

		private bool m_HitDetect;
		private RaycastHit m_Hit;

		private Vector3 v3 = new Vector3(0, 0, 0);

		public void Start() {
		}

		public void CalculationDis() {
			disForFindPath = (pointForDis1.position - pointForDis2.position).magnitude * 2;
		}

		public Vector3 GetRandomPosOnField() {
			v3.y = 10;
			float x = 0;
			float z = 0;


			float startX = upLeftCorner.position.x;
			float endX = downRightCorner.position.x;

			float startZ = downRightCorner.position.z;
			float endZ = upLeftCorner.position.z;

			int j = 0;
			while (true) {
				x = startX + Random.value * (endX - startX);
				z = startZ + Random.value * (endZ - startZ);
				v3.x = x;
				v3.z = z;
				j++;
				/*if (CheckFloorUnder(v3) && Corral.Instance.CheckPoint(v3)) {
					break;
				}*/

				if (j > 1000) {
					Debug.LogError("BAD");
					break;
				}
			}

			return v3;
		}

		public bool CheckFloorUnder(Vector3 v3) { //проверка наличия пола под этой точкой
			bool p1 = false;
			bool p2 = false;
			bool p3 = false;
			bool p4 = false;
			float m_MaxDistance = 200f;
			m_HitDetect = Physics.BoxCast(v3 + Vector3.right * 10 + Vector3.forward * 10, Vector3.one, Vector3.down, out m_Hit, Quaternion.identity, m_MaxDistance);
			if (m_HitDetect) {
				p1 = true;
			}

			m_HitDetect = Physics.BoxCast(v3 - Vector3.right * 10 + Vector3.forward * 10, Vector3.one, Vector3.down, out m_Hit, Quaternion.identity, m_MaxDistance);
			if (m_HitDetect) {
				p2 = true;
			}

			m_HitDetect = Physics.BoxCast(v3 + Vector3.right * 10 - Vector3.forward * 10, Vector3.one, Vector3.down, out m_Hit, Quaternion.identity, m_MaxDistance);
			if (m_HitDetect) {
				p3 = true;
			}

			m_HitDetect = Physics.BoxCast(v3 - Vector3.right * 10 - Vector3.forward * 10, Vector3.one, Vector3.down, out m_Hit, Quaternion.identity, m_MaxDistance);
			if (m_HitDetect) {
				p4 = true;
			}

			return p1 && p2 && p3 && p4;
		}
	}
}