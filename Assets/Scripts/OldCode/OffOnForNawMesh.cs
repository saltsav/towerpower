﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffOnForNawMesh : MonoBehaviour {

    static List<MeshRenderer> _renderers;

    void Awake() {
        if (_renderers == null) {
            _renderers = new List<MeshRenderer>();
        }

        MeshRenderer[] renders = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer rend in renders) {
            _renderers.Add(rend);
        }
    }

    static public void Off() {
        foreach (MeshRenderer rend in _renderers) {
            rend.gameObject.layer = 31;
        }
    }

    static public void On() {
        foreach (MeshRenderer rend in _renderers) {
            rend.gameObject.layer = 11;
        }
    }

    private void OnDestroy() {
        _renderers = null;
    }
}
