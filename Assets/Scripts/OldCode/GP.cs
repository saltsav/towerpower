﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.Networking;

public class GP {
    public static int priceForGoodAnimal = 10;

    public static float distanceAgroDog = 25;
    public static float countAnimalAttackFox = 10;

    public static float distanceAgroFox = 8;

    public static int sheepRadiusView = 25; //raduis зрения овцы
    public static int speedSheepMoveIdle = 5;//скорость покая овцы

    public static int speedDog = 12; // Стандарт 11

    public static Color colorZero = new Color(0, 0, 0, 0);
    public static float floatZero = 0.000001f; //нуль для сравнения

    public static int distanceCamera = 32;

    public static int scaleMap = 20;

    public static int sizeDogView = 10;
    public static int timeOnScared = 2;//макс время испуга овцы

    public static float timeForShowNoThanks = 1; // время до показа надписи
    public static float timeForRessurect = 4; // время на возвращение в игру

    public static Color colorWhiteZero = new Color(1, 1, 1, 0);
}
