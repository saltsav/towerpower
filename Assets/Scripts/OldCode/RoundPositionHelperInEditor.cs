﻿#if UNITY_EDITOR // only Unity editor
using Assets.Core.ScriptsHerd;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.BeatEditorScripts {
    [CustomEditor(typeof(RoundPositionHelper))]
    public class RoundPositionHelperInEditor : Editor {
        private RoundPositionHelper roundPositionHelper;

        public override void OnInspectorGUI() {
            if (roundPositionHelper == null) {
                roundPositionHelper = (RoundPositionHelper) target;
            }

            if (DrawDefaultInspector()) {
                roundPositionHelper.SetRoundPosition();
            }

        }

        private void OnSceneGUI() {
            if (roundPositionHelper == null) {
                roundPositionHelper = (RoundPositionHelper) target;
            }
            roundPositionHelper.SetRoundPosition();
        }
    }
}
#endif