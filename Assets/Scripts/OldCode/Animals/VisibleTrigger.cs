﻿using System;
using UnityEngine;

namespace Assets.Scripts.ScriptsHerd {
    public class VisibleTrigger : MonoBehaviour {

        public Action<bool> ActionVisible;

        public void OnBecameInvisible() {
            ActionVisible.SafeInvoke(false);
        }

        public void OnBecameVisible() {
            ActionVisible.SafeInvoke(true);
        }
    }
}