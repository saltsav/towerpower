﻿using System;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using Random = UnityEngine.Random;

public enum TypeAnimal {
    none,
    bigDog,
    haskyDog,
    cow,
    ovcharkaDog,
    pig,
    whiteSheep,
    brownSheep,
    blackSheep,
    badCow,
    fox
}

public enum GlobalAnimState {
    none,
    run,
    runFast,
    idle,
    barks,
    custom,
    customDelay
}

public class AnimalView : MonoBehaviour {
    [ReadOnly] public GlobalAnimState CurrentAnimState = GlobalAnimState.none;

    public Animator animalAnimator;

    public string triggerRun;
    public string triggerRunFast;
    public string triggerIdle;
    public string triggerSit;
    public string triggerBarks;
    string triggerLast = "";

    public List<string> triggersCustom;

    private void OnDisable() {
        triggerLast = "";
    }

    public void SetAnimatorState(GlobalAnimState newGlobalAnimState) {
        if (CurrentAnimState == newGlobalAnimState && newGlobalAnimState != GlobalAnimState.custom) {
            return;
        }
        if (newGlobalAnimState != GlobalAnimState.run) {
            SetAnimationSpeed(0);
        }
        CancelInvoke();
        if (triggerLast != "") animalAnimator.ResetTrigger(triggerLast);

        CurrentAnimState = newGlobalAnimState;
        switch (newGlobalAnimState) {
            case GlobalAnimState.run:
                triggerLast = triggerRun;
                break;
            case GlobalAnimState.runFast:
                triggerLast = triggerRunFast;
                break;
            case GlobalAnimState.idle:
                triggerLast = triggerIdle;
                break;
            case GlobalAnimState.custom:
                triggerLast = triggersCustom[Random.Range(0, triggersCustom.Count)];
                break;
            case GlobalAnimState.customDelay:
                triggerLast = triggerIdle;
                Invoke("SetDelayAnimation", Random.Range(0.4f, 1.8f));
                break;
            case GlobalAnimState.barks:
                triggerLast = triggerBarks;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newGlobalAnimState), newGlobalAnimState, null);
        }
        animalAnimator.SetTrigger(triggerLast);
    }

    void SetDelayAnimation() {
        triggerLast = triggersCustom[Random.Range(0, triggersCustom.Count)];
        animalAnimator.SetTrigger(triggerLast);
    }

    public void SetAnimationSpeed(float currentSpeed) {
        float scaled = currentSpeed / 20;
        scaled = Mathf.Clamp(scaled, 0, 1);
        animalAnimator.speed = 1 + (3 * scaled);
    }

    [Button]
    public void SetRun() {
        SetAnimatorState(GlobalAnimState.run);
    }

    [Button]
    public void SetRunFast() {
        SetAnimatorState(GlobalAnimState.runFast);
    }

    [Button]
    public void SetIdle() {
        SetAnimatorState(GlobalAnimState.idle);
    }

    [Button]
    public void SetCustom() {
        SetAnimatorState(GlobalAnimState.custom);
    }
}
