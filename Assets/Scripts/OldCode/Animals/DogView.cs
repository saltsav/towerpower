﻿using System;
using UnityEngine;

namespace Assets.Scripts {
	public class DogView : MonoSingleton<DogView> {
		[SerializeField] public Transform viewParent;
		[ReadOnly] public TypeAnimal currentTypeAnimal;
		private Vector3 previousPosition;

		public void StartFromController() {
			SetView(TypeAnimal.bigDog); // Ставится собака по умолчанию. Потом добавим выбор скина
		}


		public void SetView(TypeAnimal typeAnimal) {
			currentTypeAnimal = typeAnimal;

			AnimalView currentAnimalView = AnimalsModelController.Instance.GetAnimalViewByType(currentTypeAnimal);
		    currentAnimalView.transform.SetParent(viewParent);
			currentAnimalView.transform.localPosition = Vector3.zero;
		    currentAnimalView.transform.localEulerAngles = Vector3.zero;
			currentAnimalView.gameObject.SetActive(true);

			DogGO.Single.CurrentAnimalView = currentAnimalView;
		}
	}
}