﻿using System;
using Assets.Scripts.Core;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.ScriptsHerd {
    public enum StateFox {
        idle,
        hunting,
        runAway,
        despawn
    }

    public class FoxGO : MonoBehaviour {

        [SerializeField] private NavMeshAgent NavMeshAgent;
        [SerializeField] GameObject NotSpawnArea;
        [SerializeField] FoxView FoxView;
        [SerializeField] FearIconController FearIcon;

        [NonSerialized] public StateFox stateFox = StateFox.despawn;

        Vector3 _pointForMove;
        VisibleTrigger _visibleTrigger;
        AnimalView CurrentAnimalView;
        InfoOneBigBlockMap _cacheInvisibleBlock;

        bool _isRun;

        bool iAtaked;
        int countCollision;

        private void OnEnable() {
            _isRun = false;
            iAtaked = false;
            InvokeRepeating("CheckDog", 0, 0.1f);
        }

        public void OnDisable() {
            _cacheInvisibleBlock = null;
            countCollision = 0;
            NotSpawnArea.SetActive(true);
            NavMeshAgent.enabled = false;
        }

        public void CheckDog() {
            if (stateFox == StateFox.idle && countCollision > 0) {
                float disMap = MapManager.Instance.GetDistanceBetweenForMap(transform.position, DogGO.Instance.transform.position);
                float disLine = Vector3.Distance(transform.position, DogGO.Instance.transform.position);
                if (disMap * 0.8 < disLine) {
                    SetStateFox(StateFox.hunting);
                }
            }
        }

        // Начальные настройки
        public void SetStart() {
            FoxView.StartFromController();
            CurrentAnimalView = FoxView.currentAnimalView;
            NotSpawnArea.SetActive(false);
            NavMeshAgent.enabled = true;
            SetStateFox(StateFox.idle);

            _visibleTrigger = CurrentAnimalView.GetComponentInChildren<VisibleTrigger>();
            _visibleTrigger.ActionVisible += DoOnVisibleTrigger;
        }

        public void DoOnVisibleTrigger(bool var) {
            if (_isRun && !var) {
                CancelInvoke();
                _visibleTrigger.ActionVisible -= DoOnVisibleTrigger;
                gameObject.SetActive(false);
            }
        }


        // Смена статуса Лисы
        public void SetStateFox(StateFox newFoxState) {
            if (Equals(stateFox, newFoxState) || _isRun) {
                return;
            }

            CancelInvoke();
            switch (newFoxState) {
                case StateFox.idle:
                    NavMeshAgent.acceleration = 50;
                    CurrentAnimalView.SetAnimatorState(GlobalAnimState.idle);
                    break;

                case StateFox.hunting:
                    if (GoodAnimalManager.Instance.AnimalInDog.Count == 0) {
                        SetStateFox(StateFox.runAway);
                        break;
                    }

                    CurrentAnimalView.SetAnimatorState(GlobalAnimState.custom);
                    NavMeshAgent.speed = 0;

                    transform.DOLookAt(DogGO.Single.transform.position, 0.5f);
                    DOVirtual.DelayedCall(1.3f, () => {
                        if (stateFox == StateFox.hunting) {
                            CurrentAnimalView.SetAnimatorState(GlobalAnimState.run);
                            InvokeRepeating("DoOnHunting", 0, 0.1f);
                        }
                    });
                    break;

                case StateFox.runAway:
                    _isRun = true;
                    FearIcon.ShowIconWarning();

                    if (gameObject.activeSelf) {
                        CurrentAnimalView.SetAnimatorState(GlobalAnimState.run);
                        InvokeRepeating("DoOnRunAway", 0, 0.5f);
                    }

                    DOVirtual.Float(20, 150, 2.5f, (accel) => {
                        NavMeshAgent.acceleration = accel;
                    });
                    break;
            }
            stateFox = newFoxState;
        }


        // Лиса атакует
        public void DoOnHunting() {
            if (iAtaked || stateFox == StateFox.runAway) {
                return;
            }

            if (GoodAnimalManager.Instance.AnimalInDog.Count == 0) {
                SetStateFox(StateFox.runAway);
                return;
            }

            float dis = float.MaxValue;
            int index = 0;
            for (int i = 0; i < GoodAnimalManager.Instance.AnimalInDog.Count; i++) {
                float newDis = Vector3.Distance(transform.position, GoodAnimalManager.Instance.AnimalInDog[i].transform.position);
                if (newDis < dis) {
                    dis = newDis;
                    index = i;
                }
            }

            if (dis < GP.distanceAgroFox) {
                iAtaked = true;
                SoundController.Instance.PlaySound(SoundsList.Fox, 1.25f);
                GoodAnimalManager.Instance.FoxAttack(this);
                GoodAnimalCountManager.Single.UpdateCount();

                DOVirtual.DelayedCall(0.4f, () => {
                    SetStateFox(StateFox.runAway);
                });

                CurrentAnimalView.SetAnimatorState(GlobalAnimState.custom);
                NavMeshAgent.ResetPath();
            } else {
                NavMeshAgent.SetDestination(GoodAnimalManager.Instance.AnimalInDog[index].transform.position);
                NavMeshAgent.speed = GP.speedDog * 1.5f;
            }
        }

        // Лиса убегает
        public void DoOnRunAway() {
            if (_cacheInvisibleBlock == null || _cacheInvisibleBlock.isVisible) {
                _cacheInvisibleBlock = MapGenerator.Instance.GetNearestSleepPoint(transform.position);
                _pointForMove = _cacheInvisibleBlock.visibleTrigger.transform.position;
                NavMeshAgent.SetDestination(_pointForMove);
                NavMeshAgent.speed = GP.speedDog * 3f;
            }
        }

        private void Update() {
            NavMeshPath _path = NavMeshAgent.path;
            for (int i = 0; i < _path.corners.Length - 1; i++) Debug.DrawLine(_path.corners[i], _path.corners[i + 1], Color.red);
        }

        // В зону лисы вошла собака
        private void OnTriggerEnter(Collider other) {
            if (stateFox == StateFox.idle && other.tag == "DogTrigger") {
                countCollision++;
                SetStateFox(StateFox.hunting);
            }
        }

        // Из зоны лисы вышла собака 
        private void OnTriggerExit(Collider other) {
            if (stateFox == StateFox.idle && other.tag == "DogTrigger") {
                countCollision--;
                SetStateFox(StateFox.hunting);
            }
        }
    }
}