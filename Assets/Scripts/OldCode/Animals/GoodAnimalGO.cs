﻿using System;
using Assets.Scripts;
using Assets.Scripts.Core;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public enum GoodAnimalState {
    none,
    idle,
    inHerd, // Собака забрала овцу в стадо
    fearRun, // Овца потерялась и ушла из стада
    fearStay,
    inCorral // Овца в загоне
}

public class GoodAnimalGO : MonoBehaviour {

    [SerializeField] private NavMeshAgent NavMeshAgent;
    [SerializeField] private GameObject NotSpawnArea;
    [SerializeField] private GameObject CollectEffect;

    [ReadOnly] public GoodAnimalState StateGoodAnimal = GoodAnimalState.none;

    [NonSerialized] public FearIconController _fear;
    [NonSerialized] public int СounGoodAnimalCollision;
    [NonSerialized] public Vector3 pointFox;

    AnimalView CurrentAnimalView;
    PlusMoneyIcon MoneyIcons;
    private GoodAnimalGO _toCheck;
    private Vector3 _pointForMove;
    private NavMeshPath _path;
    private bool _isMove;
    private bool _isOtherGoodAnimal;
    private NavMeshHit _nawHit;
    bool _isStop;

    // При старте
    private void Awake() {
        _path = new NavMeshPath();
        _nawHit = new NavMeshHit();
    }

    public void OnEnable() {
        UpdateManager.UpdateThis += UpdateThis;
    }

    public void OnDisable() {
        _isStop = false;
        _fear = null;
        _isMove = false;
        СounGoodAnimalCollision = 0;
        NotSpawnArea.SetActive(true);
        NavMeshAgent.enabled = false;
        _isOtherGoodAnimal = false;
        UpdateManager.UpdateThis -= UpdateThis;
    }

    private void UpdateThis() {
        AnimationCheck();
    }

    // Проверка анимации
    private void AnimationCheck() {
        if (CurrentAnimalView != null) {
            if (StateGoodAnimal == GoodAnimalState.fearStay) {
                CurrentAnimalView.SetAnimatorState(GlobalAnimState.idle);
                return;
            }

            float speed = Mathf.Abs(NavMeshAgent.velocity.x) + Mathf.Abs(NavMeshAgent.velocity.z);

            if (speed > 1.2f && CurrentAnimalView.CurrentAnimState != GlobalAnimState.run) {
                CurrentAnimalView.SetAnimatorState(GlobalAnimState.run);
            } else if (speed < 1.2f && CurrentAnimalView.CurrentAnimState != GlobalAnimState.customDelay && CurrentAnimalView.CurrentAnimState != GlobalAnimState.custom) {
                StopGoodAnimal();
                CurrentAnimalView.SetAnimatorState(_isMove ? GlobalAnimState.customDelay : GlobalAnimState.custom);
            }


            // Смотреть на собаку когда стоят
            if (_isStop) {
                StopGoodAnimal();
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(DogGO.Single.transform.position - transform.position), Time.deltaTime * 4);
            }

            // Скорость анимации при беге
            if (CurrentAnimalView.CurrentAnimState == GlobalAnimState.run) {
                CurrentAnimalView.SetAnimationSpeed(speed);
            }
        }
    }


    // Начальные настройки
    public void SetStart() {
        NotSpawnArea.SetActive(false);
        NavMeshAgent.enabled = true;

        CurrentAnimalView = GetComponentInChildren<AnimalView>();
        CurrentAnimalView.SetAnimatorState(GlobalAnimState.custom);

        SetStateGoodAnimal(GoodAnimalState.idle);
    }

    public void CancelFear() {
        if (_fear != null) {
            _fear.HiddenIcon();
        }
    }

    // Смена статуса овцы
    public void SetStateGoodAnimal(GoodAnimalState newStateGoodAnimal) {
        if (Equals(StateGoodAnimal, newStateGoodAnimal)) {
            return;
        }

        CancelInvoke();
        switch (newStateGoodAnimal) {
            case GoodAnimalState.idle:
                InvokeRepeating("DoOnIdeState", 0, Random.Range(3.8f, 5.8f));
                NavMeshAgent.acceleration = 16;
                CancelFear();
                break;
            case GoodAnimalState.inHerd:
                GoodAnimalManager.Single.AnimalInDog.Add(this);
                NavMeshAgent.acceleration = 80;
                ActivateCollectEffect();
                //GameManager.Instance.CheckCompleted();

                SoundController.Single.PlaySound(SoundsList.Sheep, 0.2f);
                CancelFear();

                MoneyIcons = WorldCanvasIconController.Single.AddFinalMoneyIcon(transform);
                break;
            case GoodAnimalState.fearRun:
                СounGoodAnimalCollision = 0;

                if (_fear == null) {
                    _fear = PoolsController.Instance.poolFearIcon.Spawn(Vector3.zero, Quaternion.identity, transform).GetComponent<FearIconController>();
                    _fear.transform.localEulerAngles = Vector3.zero;
                    _fear.transform.localScale = Vector3.one;
                }
                _fear.ShowIcon();

                GoodAnimalLostRun();
                GoodAnimalCountManager.Single.LostUpdateCount();

                WorldCanvasIconController.Single.DespawnIcon(MoneyIcons.gameObject);
                break;
            case GoodAnimalState.inCorral:
                GoodAnimalManager.Single.AnimalInDog.Remove(this);
                GoodAnimalManager.Single.AnimalInCorrals.Add(this);

                InvokeRepeating("DoOnInCorralState", 0, Random.Range(2.0f, 3.8f));
                CancelFear();
                break;

            case GoodAnimalState.fearStay:
                NavMeshAgent.speed = 0;
                CurrentAnimalView.SetAnimatorState(GlobalAnimState.idle);
                _fear.PassivIcon();
                break;
        }

        StateGoodAnimal = newStateGoodAnimal;
    }

    // Эффект подбора овцы
    private void ActivateCollectEffect() {
        CollectEffect.SetActive(true);
        DOVirtual.DelayedCall(2, () => { CollectEffect.SetActive(false); });
    }


    // Овца потеряна и убегает
    private void GoodAnimalLostRun() {

        float factorRotateOnFear = Random.value > 0.5f ? 1 : -1;
        float randomAngel = Random.value * 10 * factorRotateOnFear;

        float t = Random.value + 2;
        DOVirtual.DelayedCall(t, () => {
            SetStateGoodAnimal(GoodAnimalState.fearStay);
        });
        float t1 = Random.value + 0.5f;
        /*DOVirtual.DelayedCall(t1, () => {
            catRotate = true;
        });*/

        DOVirtual.Float(0, t, t, var => {

            if (var > t1) {
                //transform.eulerAngles = new Vector3(transform.eulerAngles.x, startAngel + (var - t1) / (t - t1) * randomAngel, transform.eulerAngles.z);
                _pointForMove = transform.position + transform.forward.normalized * 2 + randomAngel * transform.right.normalized * (var - t1) / (t - t1) * 0.1f;
            } else {
                _pointForMove = transform.position + (transform.position - pointFox).normalized * 2;
            }

            if (NavMesh.SamplePosition(_pointForMove, out _nawHit, 540f, 1)) {
                _pointForMove = _nawHit.position;
                _pointForMove.y = 0;
                // Проверка рядом края
                /*if (NavMesh.FindClosestEdge(_pointForMove, out _nawHit2, 1)) {
                    // Отступ от края
                    if (_nawHit2.distance < 1) {
                        Vector3 dir = (_nawHit1.position - _pointForMove).normalized;
                        _pointForMove -= dir * Random.Range(1f, 2f);
                    }
                }*/
            }

            NavMeshAgent.SetDestination(_pointForMove);
            NavMeshAgent.speed = GP.speedDog;
        });
    }

    // В состоянии покоя
    public void DoOnIdeState() {

        float randPos = Random.Range(1.5f, 2.5f);

        _pointForMove = transform.position;

        _pointForMove.x += (Random.value > 0.5f ? -randPos : randPos) * Random.value * 2;
        _pointForMove.z += (Random.value > 0.5f ? -randPos : randPos) * Random.value * 2;

        _pointForMove = MapManager.Single.PositionOnNavMesh(_pointForMove);

        NavMeshAgent.SetDestination(_pointForMove);
        NavMeshAgent.speed = Random.Range(2, 4);
    }

    // В состоянии покоя 
    public void DoOnInCorralState() {

        //NavMeshAgent.SetDestination(currentCorral.GetCorralPoint());
        NavMeshAgent.speed = Random.Range(1, 4);
    }

    // При косании
    private void OnTriggerEnter(Collider other) {
        // С собаков, добавить в стадо
        if (other.tag == "DogTrigger" && StateGoodAnimal != GoodAnimalState.inHerd && CheckPathToDog() && StateGoodAnimal != GoodAnimalState.inCorral && StateGoodAnimal != GoodAnimalState.fearRun) {
            _isMove = true;
            SetStateGoodAnimal(GoodAnimalState.inHerd);

           // GameManager.Single.CheckCompleted();
        }

        // Если косание с другой овцой, счетчик косаний с овцами
        if (other.tag == "Sheep" && StateGoodAnimal == GoodAnimalState.inHerd) {
            _toCheck = other.GetComponent<GoodAnimalGO>();
            if (_toCheck == null || _toCheck.StateGoodAnimal == GoodAnimalState.inHerd) {
                СounGoodAnimalCollision++;
                _isOtherGoodAnimal = true;
            }
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == "DogTrigger") {
            OnTriggerEnter(other);
        }
    }

    // Завершение косания
    private void OnTriggerExit(Collider other) {
        // Овца отошла, обновляем счетчик
        if (other.tag == "Sheep" && StateGoodAnimal == GoodAnimalState.inHerd) {
            _toCheck = other.GetComponent<GoodAnimalGO>();
            if (_toCheck == null || _toCheck.StateGoodAnimal == GoodAnimalState.inHerd) {
                СounGoodAnimalCollision--;
                if (СounGoodAnimalCollision == 0) {
                    _isOtherGoodAnimal = false;
                }
            }
        }
    }

    // Обновление позиции
    public void UpdatePosition(float scaled, Vector3 toMove, bool notNear) {
        if (_isMove) {

            /*
            Доработаю момент. Сейчас если есть одна овца близко и дальние овца касаются друг друга, то все стоят. 
            Надо переделать что бы стояли только те кто спереди и позади, а остальные все равно бежали вперед.
            Сделаю что бы бы овцы знали друг о друге и сверялись, кто может еще подойти к собаке, а кто должен стоять
            */

            // Проверка на движение
            if ((!_isOtherGoodAnimal || notNear) && !DogGO.Single.isDead) {
                _isStop = false;
                NavMeshAgent.SetDestination(toMove);
                float speed = GP.speedDog * 3;
                NavMeshAgent.speed = speed - speed * scaled;
            } else {
                _isStop = true;
            }
        }
    }

    // Отменить движение овцы
    private void StopGoodAnimal() {
        NavMeshAgent.velocity = Vector3.zero;
        NavMeshAgent.ResetPath();
        NavMeshAgent.speed = 0;
    }

    // Проверка обрыва до собаки
    private bool CheckPathToDog() {
        // Вычисляем путь по прямой, и по траектории и смотрим насколько разница в путях. Если сильно большая, то обрыв. Если мелкая, то значит небольшой поворот - допустимо.
        float distanceLine = Vector3.Distance(DogGO.Single.transform.position, transform.position);
        float distanceToDog = GetDistanceToDog();
        return distanceToDog - distanceLine < 2;
    }

    // Получение расстояния к собаке
    private float GetDistanceToDog() {
        bool isPath = NavMesh.CalculatePath(transform.position, DogGO.Single.transform.position, NavMesh.AllAreas, _path);
        float distance = float.MaxValue;
        if (isPath) {
            distance = 0;
            for (int i = 0; i < _path.corners.Length - 1; i++) {
                distance += Vector3.Distance(_path.corners[i], _path.corners[i + 1]);
            }
        }

        return distance;
    }
}