﻿using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Core;
using Assets.Scripts.ScriptsHerd;
using UnityEngine;
using UnityEngine.AI;

public class GoodAnimalManager : MonoSingleton<GoodAnimalManager> {
    [SerializeField] public Transform parentAnimal;
    [ReadOnly] public List<GoodAnimalGO> AllAnimal;
    [ReadOnly] public List<GoodAnimalGO> AnimalInDog;
    [ReadOnly] public List<GoodAnimalGO> AnimalInCorrals;

    public List<GoodAnimalGO> AnimalForAttack;

    // При старте
    public void Awake() {
        AllAnimal = new List<GoodAnimalGO>();
        AnimalInDog = new List<GoodAnimalGO>();
        AnimalInCorrals = new List<GoodAnimalGO>();
    }

    public int GetAllSheep() {
        return AllAnimal.Count;
    }

    private void Start() {
        UpdateManager.UpdateThis += SheepUpdate;
    }

    // Добавление овец
    public void CreateSheep(bool isStart) {
        if (isStart) {
            AllAnimal.Clear();
            for (int i = 0; i < PlayerInfoManager.Instance.countGoodAnimal; i++) {
                GameObject sheep = PoolsController.Instance.poolAnimal.Spawn(MapManager.Single.RandomNavmeshLocation(1), Quaternion.identity, parentAnimal);
                AllAnimal.Add(sheep.GetComponent<GoodAnimalGO>());

                // Рандомный поворот
                Vector3 rotate = Vector3.zero;
                rotate.y = Random.Range(0, 360);
                sheep.transform.localEulerAngles = rotate;

                sheep.GetComponent<GoodAnimalView>().ViewSetup();

                MapManager.Single.UpdateNavMeshMap();
            }
        } else {
            // Убираем все зоны "NotSpawn"
            foreach (GoodAnimalGO sheep in AllAnimal) sheep.SetStart();
        }
    }

    // Обновление овец в стаде
    private void SheepUpdate() {
        if (DogGO.Single.isDead) return;

        // Определение самой ближней дистанции
        float nearDistance = float.MaxValue;
        foreach (GoodAnimalGO sheep in AnimalInDog) {
            if (sheep.СounGoodAnimalCollision >= 2) {
                float distance = Vector3.Distance(DogGO.Single.transform.position, sheep.transform.position);
                if (nearDistance > distance) {
                    nearDistance = distance;
                }
            }
        }

        // Передвижение овец
        foreach (GoodAnimalGO sheep in AnimalInDog) {
            float distance = Vector3.Distance(DogGO.Single.transform.position, sheep.transform.position);
            float scaled = 1f - (distance - 2f) / (12f - 2f);
            scaled = Mathf.Clamp(scaled, 0, 1);
            sheep.UpdatePosition(scaled, DogGO.Single.PointToMove.position, nearDistance > 5f);
        }
    }

    public void FoxAttack(FoxGO fox) {
        AnimalForAttack.Clear();
        foreach (GoodAnimalGO animal in AnimalInDog) {
            if (animal != null) {
                float dis = Vector3.Distance(fox.transform.position, animal.transform.position);
                if (dis < 20) {
                    AnimalForAttack.Add(animal);
                }
            }
        }

        for (int i = 0; i < GP.countAnimalAttackFox; i++) {
            float dis = float.MaxValue;
            int index = 0;

            for (int j = 0; j < AnimalForAttack.Count; j++) {
                float newDis = Vector3.Distance(fox.transform.position, AnimalForAttack[j].transform.position);
                if (newDis < dis) {
                    dis = newDis;
                    index = j;
                }
            }

            AnimalInDog.Remove(AnimalForAttack[index]);
            AnimalForAttack[index].pointFox = fox.transform.position;
            AnimalForAttack[index].SetStateGoodAnimal(GoodAnimalState.fearRun);
            AnimalForAttack.RemoveAt(index);
            if (AnimalForAttack.Count == 0 || AnimalInDog.Count == 0) {
                break;
            }
        }

        AnimalForAttack.Clear();
    }

    //
    protected override void OnDestroy() {
        UpdateManager.UpdateThis -= SheepUpdate;
        base.OnDestroy();
    }

    /*public void DestroySheep() {
		foreach (SheepGO allSheep in AllAnimal) {
			if (allSheep != null) {
				Destroy(allSheep.gameObject);
			}
		}
		AllAnimal.Clear();

		foreach (SheepGO allSheep in AnimalInDog) {
			if (allSheep != null) {
				Destroy(allSheep.gameObject);
			}
		}
		AnimalInDog.Clear();
	}*/


    //NavMeshHit _nawHit;
    //void Update() {
    //    // Путь до собаки (Debug)
    //    NavMeshPath _path = new NavMeshPath();
    //    foreach (GoodAnimalGO sheep in AllAnimal) {
    //        bool isPath = NavMesh.CalculatePath(sheep.transform.position, DogGO.Single.transform.position, NavMesh.AllAreas, _path);
    //        for (int i = 0; i < _path.corners.Length - 1; i++) Debug.DrawLine(_path.corners[i], _path.corners[i + 1], Color.red);
    //    }

    //    //// Видимость до собаки (Debug)
    //    //foreach (GoodAnimalGO sheep in AnimalInDog) {
    //    //    bool blocked = NavMesh.Raycast(sheep.transform.position, DogGO.Single.transform.position, out _nawHit, NavMesh.AllAreas);
    //    //    Debug.DrawLine(sheep.transform.position, DogGO.Single.PointToMove.position, blocked ? Color.red : Color.green);
    //    //    if (blocked) Debug.DrawRay(_nawHit.position, Vector3.up, Color.red);
    //    //}
    //}
}