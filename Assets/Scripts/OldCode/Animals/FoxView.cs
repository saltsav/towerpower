﻿using System;
using UnityEngine;

namespace Assets.Scripts {
    public class FoxView : MonoBehaviour {
        [SerializeField] public Transform viewParent;
        [ReadOnly] public TypeAnimal currentTypeAnimal;
        private Vector3 previousPosition;
        [ReadOnly] public AnimalView currentAnimalView;

        public void StartFromController() {
            SetView(TypeAnimal.fox); // Ставится лиса по умолчанию. Потом добавим выбор скина
        }

        public void SetView(TypeAnimal typeAnimal) {
            currentTypeAnimal = typeAnimal;

            currentAnimalView = AnimalsModelController.Instance.GetAnimalViewByType(currentTypeAnimal);
            currentAnimalView.transform.SetParent(viewParent);
            currentAnimalView.transform.localPosition = Vector3.zero;
            currentAnimalView.transform.localEulerAngles = Vector3.zero;
            currentAnimalView.gameObject.SetActive(true);
        }
    }
}