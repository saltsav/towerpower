﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts {
	public class GoodAnimalView : MonoBehaviour {

		[SerializeField] TypeAnimal[] SpeepViews;
		[SerializeField] public Transform viewParent;

		[ReadOnly] public TypeAnimal currentTypeAnimal;
		private Vector3 previousPosition;
		[ReadOnly] public float realSpeed;
		private float _timeForChangeState;

		public void ViewSetup() {
			SetView(SpeepViews[Random.Range(0, SpeepViews.Length)]);
		}

		public void SetView(TypeAnimal typeAnimal) {
			currentTypeAnimal = typeAnimal;

			AnimalView currentAnimalView = AnimalsModelController.Instance.GetAnimalViewByType(currentTypeAnimal);
		    currentAnimalView.transform.SetParent(viewParent);
			currentAnimalView.transform.localPosition = Vector3.zero;
		    currentAnimalView.transform.localEulerAngles = Vector3.zero;
			currentAnimalView.gameObject.SetActive(true);
		}
	}
}