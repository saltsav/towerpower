﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Core;
using Assets.Scripts.ScriptsHerd;
using DG.Tweening;
using UnityEngine;

public class DogGO : MonoSingleton<DogGO> {

    [SerializeField] public Rigidbody MainRigidbody;
    [SerializeField] public Transform PointToMove;
    [SerializeField] private GameObject WaterEffects;
    [SerializeField] public GameObject TargetToCamera;

    [NonSerialized] public float DogSpeed;
    [NonSerialized] public bool isDead;
    [NonSerialized] public AnimalView CurrentAnimalView;

    // Собака
    Vector3 _dogAngles = Vector3.zero;
    Vector3 _newVelocity;
    float _currentPowerMove;
    bool _isTouch;

    // Управление
    Vector3 _touchVector = Vector3.zero;
    Vector3 _startPosition;
    Vector3 _currentPosition;
    readonly float _minSpeedPosition = 0.01f;
    readonly float _maxSpeedPosition = 0.045f;
    Vector2 _startTouch;

    Sequence _sequenceBarks;

    public List<Vector3> lastPosition = new List<Vector3>();

    public float timeForSavePosition;

    // Для рейкаста
    Vector3 _rayPosDown;
    RaycastHit[] _hit;
    float _startY;
    int _mask;

    // При старте
    private void Start() {
        _mask = 1 << LayerMask.NameToLayer("floor");
        _hit = new RaycastHit[1];
        _startY = transform.position.y;

        _touchVector.z = 550;
        //GameManager.Single.LevelCompletedAction += LevelCompleted;
    }

    public void SetPosition(Vector3 v3) {
        transform.parent.position = v3;
    }

    // Обновление позиции
    private void FixedUpdateThis() {

        ////  Проверка пола под ногами
        //if (_isTouch && _startY - transform.position.y > 0.1f) {
        //    _rayPosDown = transform.forward * 1.6f;
        //    if (Physics.RaycastNonAlloc(transform.position + _rayPosDown, Vector3.down, _hit, 2, _mask) == 0
        //        && Physics.RaycastNonAlloc(transform.position + transform.forward, (transform.forward + Vector3.down) / 2, _hit, 3, _mask) != 0
        //        && Physics.RaycastNonAlloc(transform.position - _rayPosDown, Vector3.down, _hit, 2, _mask) == 0) {
        //        _currentPowerMove = 0;
        //        _isTouch = false;
        //    }
        //}

        if (_isTouch) {
            _rayPosDown = transform.forward * 1.6f;
            if (Physics.RaycastNonAlloc(transform.position + _rayPosDown, Vector3.down, _hit, 2, _mask) == 0) {
                MainRigidbody.velocity = Vector3.zero;
            } else {

                // Больше масса = быстрее падение
                //MainRigidbody.AddForce(Physics.gravity * (MainRigidbody.mass * MainRigidbody.mass));

                // Ускорение с сохранением velocity.y
                DogSpeed = _currentPowerMove * GP.speedDog;
                _newVelocity = transform.forward * DogSpeed * Time.fixedDeltaTime * 90f;
                _newVelocity.y = MainRigidbody.velocity.y;
                MainRigidbody.velocity = _newVelocity;
            }
        }

        //CheckDead();
    }

    // Проверка смерти
    private void CheckDead() {
        if (!isDead && transform.localPosition.y < -3) {
            DeadActivated(true);
        } else {
            SaveGoodPos();
        }
    }

    public void SaveGoodPos() {
        /*if (MainLogic.Instance.GetStateGame() != StateGame.play) {
            return;
        }*/

        timeForSavePosition -= Time.fixedDeltaTime;
        if (timeForSavePosition < 0) {
            timeForSavePosition = 0.1f;
            if (transform.position.y > 0.3f && MapManager.Single.CheckFloorUnderPoint(transform.position, "Earth")) {
                lastPosition.Add(transform.position);
                if (lastPosition.Count > 10) {
                    while (true) {
                        lastPosition.RemoveAt(0);
                        if (lastPosition.Count < 5) {
                            break;
                        }
                    }
                }
            }
        }
    }

    public void ResurectDog() {
        isDead = false;
        transform.position = lastPosition[Mathf.Clamp(lastPosition.Count - 8, 0, lastPosition.Count - 1)];
        transform.localEulerAngles = Vector3.zero;
        timeForSavePosition = 0;
        DogView.Single.viewParent.localEulerAngles = Vector3.zero;
        lastPosition.Clear();
       // MainLogic.Single.CurrentStateGame = StateGame.play;
    }

    public void DogTachedBadCow() {
        /*if (MainLogic.Instance.GetStateGame() == StateGame.play) {
            DeadActivated();
        }*/
    }

    // Активация смерти
    private void DeadActivated(bool isWater = false) {
        isDead = true;

        if (isWater) {
            WaterEffects.SetActive(true);
            //MainLogic.Single.CurrentStateGame = StateGame.fail;
            SoundController.Single.PlaySound(SoundsList.Fail, 0.25f);
            _currentPowerMove = 0;
            AnimationCheck(0);

            // Анимация собаки
            DOTween.Sequence().Append(DogView.Single.viewParent.DOLocalMoveY(-4, 0.4f).SetEase(Ease.OutQuad))
                .Append(DogView.Single.viewParent.DOLocalMoveY(-0.7f, 0.4f).SetEase(Ease.OutQuad))
                .Append(DogView.Single.viewParent.DOLocalMoveY(-1.2f, 0.2f).SetEase(Ease.OutQuad))
                .Append(DogView.Single.viewParent.DOLocalMoveY(-1.0f, 0.1f).SetEase(Ease.OutQuad))
                .AppendCallback(() => { WaterEffects.SetActive(false); });
            DogView.Single.viewParent.DOLocalRotate(new Vector3(-20, 0, 0), 0.6f).SetEase(Ease.InQuad);

            // Показ окна
          //  DOVirtual.DelayedCall(1.15f, () => { MainLogic.Instance.ShowContinueModal(); });
        } else {
           // MainLogic.Instance.ShowContinueModal();
        }
    }

    // При нажатии
    private void IsTouch(Vector2 pos, bool status) {

        _isTouch = status;
        GetViewportPosition(pos, out _startPosition);
        TouchPointController.Single.UpdateStatus(status);

        if (!status) {
            DOTween.Kill(MainRigidbody);
            _currentPowerMove = 0;
            PlayBark();
        } else {
            _startTouch = pos;
        }

     /*   if (MainLogic.Instance.GetStateGame() == StateGame.play) {
            AnimationCheck(0);
        }*/
    }

    public void PlayBark() {
        if (/*MainLogic.Instance.GetStateGame() == StateGame.play &&*/ !isDead && transform.position.y > 0) {
            if (CurrentAnimalView.CurrentAnimState != GlobalAnimState.barks) {
                CurrentAnimalView.SetAnimatorState(GlobalAnimState.barks);

                _sequenceBarks.Kill();
                _sequenceBarks = DOTween.Sequence()
                    .AppendInterval(0.2f)
                    .AppendCallback(() => {
                        //EnemyController.Instance.DogAttack();
                        if (CurrentAnimalView.CurrentAnimState == GlobalAnimState.barks) {
                            SoundController.Instance.PlaySound(SoundsList.Barks, 0.7f);
                        }
                    })
                    .AppendInterval(0.3f)
                    .AppendCallback(() => {
                        //EnemyController.Instance.DogAttack();
                        if (CurrentAnimalView.CurrentAnimState == GlobalAnimState.barks) {
                            SoundController.Instance.PlaySound(SoundsList.Barks, 0.55f);
                        }
                    }).AppendInterval(0.3f)
                    .AppendCallback(() => {
                        if (CurrentAnimalView.CurrentAnimState == GlobalAnimState.barks) {
                            CurrentAnimalView.SetAnimatorState(GlobalAnimState.idle);
                        }
                    });
            }
        }
    }


    // При движении пальца
    private void IsMove(Vector2 pos) {
        if (_isTouch && !isDead /*&&/* MainLogic.Instance.GetStateGame() == StateGame.play*/) {
            GetViewportPosition(pos, out _currentPosition);

            TouchPointController.Single.UpdateLine(_startTouch, pos);

            // Множитель скорости от 0 до 1 в зависимости от расстояния от центра нажатия
            float distance = Vector2.Distance(_startPosition, _currentPosition);
            float scaled = (distance - _minSpeedPosition) / (_maxSpeedPosition - _minSpeedPosition);
            scaled = Mathf.Clamp(scaled, 0, 1);

            _currentPowerMove = scaled;

            AnimationCheck(distance);

            // Поворот
            if (_currentPowerMove > 0) {
                float angle = Mathf.Atan2(_currentPosition.x - _startPosition.x, _currentPosition.y - _startPosition.y) * 180 / Mathf.PI;
                _dogAngles.y = angle;
                MainRigidbody.DORotate(_dogAngles, 0.1f + 0.3f * (1f - scaled));
            }
        }
    }

    //Завершение уровня
    private void LevelCompleted() {
        CurrentAnimalView.SetAnimatorState(GlobalAnimState.custom);
        _currentPowerMove = 0;
    }

    // Проверка анимации
    private void AnimationCheck(float distance) {

        if (distance > _minSpeedPosition) {
            DOTween.Kill(TargetToCamera.transform);
            TargetToCamera.transform.DOLocalMoveZ(2.5f, 1.5f);
            CurrentAnimalView.SetAnimatorState(GlobalAnimState.run);
        } else {
            DOTween.Kill(TargetToCamera.transform);
            TargetToCamera.transform.DOLocalMoveZ(0, 0.3f);
            if (CurrentAnimalView.CurrentAnimState != GlobalAnimState.barks) {
                CurrentAnimalView.SetAnimatorState(GlobalAnimState.idle);
            }
        }
    }

    // Поучение позиции в окне
    private void GetViewportPosition(Vector2 pos, out Vector3 wordPos) {
        _touchVector.x = pos.x;
        _touchVector.y = pos.y;
        wordPos = Constant.GetMainCamera.ScreenToViewportPoint(_touchVector);
    }

    private void OnEnable() {
        ControlManager.IsTouch += IsTouch;
        ControlManager.IsMove += IsMove;
        UpdateManager.FixedUpdateThis += FixedUpdateThis;
    }

    private void OnDisable() {
        isDead = false;
        ControlManager.IsTouch -= IsTouch;
        ControlManager.IsMove -= IsMove;
        UpdateManager.FixedUpdateThis -= FixedUpdateThis;
    }


}