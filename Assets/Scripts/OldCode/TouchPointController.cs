﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class TouchPointController : MonoSingleton<TouchPointController> {

    [SerializeField] Canvas ParentCanvas;
    [SerializeField] CanvasGroup ToAlpha;
    [SerializeField] RectTransform Point;
    [SerializeField] RectTransform ToStrech;

    bool _isMove;

    // При старте
    void Start() {
        ToAlpha.alpha = 0;
    }

    // Обновление статуса
    public void UpdateStatus(bool status) {
        if (_isMove && !status) {
            _isMove = false;
            ToAlpha.DOFade(0, 0.2f);
        }
    }

    // Обновление линии
    public void UpdateLine(Vector2 posStart, Vector2 posNew) {
        if (!_isMove) {
            _isMove = true;
            ToAlpha.DOFade(1, 0.2f);
            transform.position = RectTransformUtility.PixelAdjustPoint(posStart, transform, ParentCanvas); ;
        }

        Vector2 dir = posNew - posStart;
        dir = new Vector3(dir.x, dir.y, 0.0f);
        Point.transform.rotation = Quaternion.FromToRotation(Vector3.up, dir);
        //ToStrech.sizeDelta = new Vector2(0, Vector2.Distance(posStart, posNew));
    }
}
