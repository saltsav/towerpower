﻿using System.Collections.Generic;
using Assets.Scripts.ScriptsHerd;
using EasyButtons;
using UnityEngine;

public class InfoOneBigBlockMap : MonoBehaviour {
    [ReadOnly] public int index;
    [ReadOnly] public int x;
    [ReadOnly] public int z;
    [ReadOnly] public TypeBigBlockMap typeBigBlockMap;
    [ReadOnly] public BlockMapHelp blockMapHelp;
    [ReadOnly] public int finalRotateAngel;
    private int[,] array0 = new int[6, 6]; //размер большого блока 6
    private int[,] arrayMinus90 = new int[6, 6]; //размер большого блока 6
    private int[,] arrayMinus180 = new int[6, 6]; //размер большого блока 6
    private int[,] arrayMinus270 = new int[6, 6]; //размер большого блока 6

    public List<TypeConnectionSide> connectArray0 = new List<TypeConnectionSide>();
    public List<TypeConnectionSide> connectArrayMinus90 = new List<TypeConnectionSide>();
    public List<TypeConnectionSide> connectArrayMinus180 = new List<TypeConnectionSide>();
    public List<TypeConnectionSide> connectArrayMinus270 = new List<TypeConnectionSide>();

    public VisibleTrigger visibleTrigger;
    public bool isVisible;
    [ReadOnly] public List<BlockConnector> listBlockConnectors;

    public List<Bridges> listBridges;

    public void Start() {
        visibleTrigger = GetComponentInChildren<VisibleTrigger>();
        visibleTrigger.ActionVisible += (var) => { isVisible = var; };
    }

    public void SetupBridges() {
        foreach (Bridges bridge in listBridges) {
            bridge.Setup(this);
        }
    }

    public void OffConnectors() {
        foreach (BlockConnector blockConnector in listBlockConnectors) {
            blockConnector.Setup();
            MapGenerator.Instance.listAllBlockConnector.Add(blockConnector);
            blockConnector.gameObject.SetActive(false);
        }
    }

    public BlockConnector GetConnectorByIndex(int indexBlock) {
        foreach (BlockConnector blockConnector in listBlockConnectors) {
            if (indexBlock == blockConnector.index) {
                return blockConnector;
            }
        }

        return null;
    }



    public void RotateForCurrentAngel(int newAngel) { //вращение ПОСЛЕ инстанциации
        transform.Rotate(new Vector3(0, newAngel, 0));
        finalRotateAngel = newAngel;
    }

    public int AddBlockConnector(BlockConnector blockConnector) {
        bool flag = false;
        int i;
        for (i = 0; i < listBlockConnectors.Count; i++) {
            if (listBlockConnectors[i] == blockConnector) {
                flag = true;
                break;
            }
        }

        ClearMissingBlockConnector();
        if (flag) {
            return i + 1;
        }

        listBlockConnectors.Add(blockConnector);


        return listBlockConnectors.Count;
    }

    [Button]
    public void UpdateInfoClocksArray() {
        ClearArrayClocks();
        foreach (BlockConnector block in listBlockConnectors) {
            if (block == null) {
                Debug.LogError("UpdateInfoClocksArray error. Bad block type = " + typeBigBlockMap);
                break;
            }

            array0[block.x, 5 - block.z] = block.index;
        }

        RotateArrayClocksCounterclockwise(array0, arrayMinus90);
        RotateArrayClocksCounterclockwise(arrayMinus90, arrayMinus180);
        RotateArrayClocksCounterclockwise(arrayMinus180, arrayMinus270);

        AddConnectList(array0, connectArray0);
        AddConnectList(arrayMinus90, connectArrayMinus90);
        AddConnectList(arrayMinus180, connectArrayMinus180);
        AddConnectList(arrayMinus270, connectArrayMinus270);
    }


    public void AddConnectList(int[,] clockArray, List<TypeConnectionSide> listTypeConnectionSide) {
        listTypeConnectionSide.Clear();
        int sizeBlockMap = 6;
        for (int i = 0; i < sizeBlockMap; i++) {
            if (clockArray[0, i] != 0) {
                listTypeConnectionSide.Add(TypeConnectionSide.left);
                break;
            }
        }

        for (int i = 0; i < sizeBlockMap; i++) {
            if (clockArray[sizeBlockMap - 1, i] != 0) {
                listTypeConnectionSide.Add(TypeConnectionSide.right);
                break;
            }
        }


        for (int i = 0; i < sizeBlockMap; i++) {
            if (clockArray[i, 0] != 0) {
                listTypeConnectionSide.Add(TypeConnectionSide.up);
                break;
            }
        }

        for (int i = 0; i < sizeBlockMap; i++) {
            if (clockArray[i, sizeBlockMap - 1] != 0) {
                listTypeConnectionSide.Add(TypeConnectionSide.down);
                break;
            }
        }

        if (listTypeConnectionSide.Count == 0) {
            Debug.LogError(typeBigBlockMap + " listTypeConnectionSide not found");
        }
    }

    [Button]
    public void Show() {
        UpdateInfoClocksArray();
        ShowArray(array0);
        ShowArray(arrayMinus90);
        ShowArray(arrayMinus180);
        ShowArray(arrayMinus270);
    }


    public void ShowArray(int[,] array) {
        int sizeBlockMap = 6;
        string s = string.Empty;

        int x = 0;
        int z = 0;
        int f = 0;
        for (int n = 0; n < array.Length; n++) {
            s += array[x, z] + " \t";
            x++;
            if (x == sizeBlockMap) {
                x = 0;
                z++;
                s += " \n";
            }
        }

        Debug.Log(s);
    }


    public void ClearArrayClocks() {
        int sizeBlockMap = 6;
        array0 = new int[sizeBlockMap, sizeBlockMap];
        arrayMinus90 = new int[sizeBlockMap, sizeBlockMap];
        arrayMinus180 = new int[sizeBlockMap, sizeBlockMap];
        arrayMinus270 = new int[sizeBlockMap, sizeBlockMap];
        int x = 0;
        int z = 0;
        //int s = 0;//для теста
        for (int n = 0; n < array0.Length; n++) {
            array0[x, z] = 0;
            arrayMinus90[x, z] = 0;
            arrayMinus180[x, z] = 0;
            arrayMinus270[x, z] = 0;
            //s++;

            x++;
            if (x == sizeBlockMap) {
                x = 0;
                z++;
            }
        }
    }

    public void RotateArrayClocksCounterclockwise(int[,] array, int[,] arrayCounterclockwise) {
        int tmp;
        int n = (int) Mathf.Sqrt(array.Length);
        for (int i = 0; i < n / 2; i++) {
            for (int j = i; j < n - 1 - i; j++) {
                tmp = array[i, j];
                arrayCounterclockwise[i, j] = array[n - j - 1, i];
                arrayCounterclockwise[n - j - 1, i] = array[n - i - 1, n - j - 1];
                arrayCounterclockwise[n - i - 1, n - j - 1] = array[j, n - i - 1];
                arrayCounterclockwise[j, n - i - 1] = tmp;
            }
        }
    }

    public void ClearMissingBlockConnector() {
        while (true) {
            bool flag = true;
            for (int i = 0; i < listBlockConnectors.Count; i++) {
                if (listBlockConnectors[i] == null) {
                    listBlockConnectors.RemoveAt(i);
                    flag = false;
                    break;
                }
            }

            if (flag) {
                break;
            }
        }
    }

    public int[,] GetArrayClocksByAngel(int angel) {
        switch (angel) {
            case 0:
                return array0;
            case -90:
                return arrayMinus90;
            case -180:
                return arrayMinus180;
            case -270:
                return arrayMinus270;
        }

        Debug.LogError(angel + " bad angel");
        return null;
    }

    public int testAngel = 0;
    public TypeConnectionSide TESTTypeConnectionSide;

    [Button]
    public void TESTGetLineByAngelAndSide() {
        List<int> f = GetLineByAngelAndSide(testAngel, TESTTypeConnectionSide);

        string s = string.Empty;
        foreach (int i in f) {
            s += i + " ";
        }

        Debug.Log(s);
    }

    [Button]
    public void TESTGetIndexConnectorByAngelAndSide() {
        List<int> f = GetIndexConnectorByAngelAndSide(testAngel, TESTTypeConnectionSide);

        string s = string.Empty;
        foreach (int i in f) {
            s += i + " ";
        }

        Debug.Log(s);
    }

    [EasyButtons.Button]
    public void OffNotUsedBridges() {
        bool needUp = false;
        bool needLeft = false;
        bool needRight = false;
        bool needDown = false;

        for (int i = 0; i < blockMapHelp.usedSides.Count; i++) {
            switch (blockMapHelp.usedSides[i]) {
                case TypeConnectionSide.up:
                    needUp = true;
                    break;
                case TypeConnectionSide.down:
                    needDown = true;
                    break;
                case TypeConnectionSide.left:
                    needLeft = true;
                    break;
                case TypeConnectionSide.right:
                    needRight = true;
                    break;
            }
        }

        if (!needUp) {
            List<int> offIndex = GetIndexConnectorByAngelAndSide(finalRotateAngel, TypeConnectionSide.up);
            foreach (int i in offIndex) {
                if (i != 0) {
                    GetConnectorByIndex(i).currentBridge.OffAll();
                }
            }
        }
        if (!needDown) {
            List<int> offIndex = GetIndexConnectorByAngelAndSide(finalRotateAngel, TypeConnectionSide.down);
            foreach (int i in offIndex) {
                if (i != 0) {
                    GetConnectorByIndex(i).currentBridge.OffAll();
                }
            }
        }
        if (!needRight) {
            List<int> offIndex = GetIndexConnectorByAngelAndSide(finalRotateAngel, TypeConnectionSide.right);
            foreach (int i in offIndex) {
                if (i != 0) {
                    GetConnectorByIndex(i).currentBridge.OffAll();
                }
            }
        }
        if (!needLeft) {
            List<int> offIndex = GetIndexConnectorByAngelAndSide(finalRotateAngel, TypeConnectionSide.left);
            foreach (int i in offIndex) {
                if (i != 0) {
                    GetConnectorByIndex(i).currentBridge.OffAll();
                }
            }
        }
    }

    public List<int> GetLineByAngelAndSide(int angel, TypeConnectionSide typeConnectionSide) {
        //UpdateInfoClocksArray();
        List<int> listCount = new List<int>();

        switch (angel) {
            case 0:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[5, i]);
                        }

                        break;
                }

                break;
            case -90:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[5, i]);
                        }

                        break;
                }

                break;
            case -180:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[5, i]);
                        }

                        break;
                }

                break;
            case -270:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[5, i]);
                        }

                        break;
                }

                break;
        }

        List<int> listCount1 = new List<int>();
        for (int i = 0; i < listCount.Count; i++) {
            if (listCount[i] != 0) {
                listCount1.Add(i);
            }
        }

        return listCount1;
    }


    public List<int> GetIndexConnectorByAngelAndSide(int angel, TypeConnectionSide typeConnectionSide) {
        UpdateInfoClocksArray();
        List<int> listCount = new List<int>();

        switch (angel) {
            case 0:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(array0[5, i]);
                        }

                        break;
                }

                break;
            case -90:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus90[5, i]);
                        }

                        break;
                }

                break;
            case -180:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus180[5, i]);
                        }

                        break;
                }

                break;
            case -270:
                switch (typeConnectionSide) {
                    case TypeConnectionSide.up:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[i, 0]);
                        }

                        break;
                    case TypeConnectionSide.down:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[i, 5]);
                        }

                        break;
                    case TypeConnectionSide.left:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[0, i]);
                        }

                        break;
                    case TypeConnectionSide.right:
                        for (int i = 0; i < 6; i++) {
                            listCount.Add(arrayMinus270[5, i]);
                        }

                        break;
                }

                break;
        }

        return listCount;
    }
}

public enum TypeBigBlockMap {
    type1,
    type2,
    type3,
    type4,
    type5,
    type6,
    type7,
    type8,
    type9,
    type10,
    type11,
    type12,
    type13,
    type14,
    type15,
    type16,
    type17,
    type18,
    type19,
    type20,
    type21,
    type22,
    type23,
    type24,
    type25,
    type26,
    type27,
    type28
}