﻿#if UNITY_EDITOR // only Unity editor
using UnityEditor;

namespace Assets.Scripts {
    [CustomEditor(typeof(BlockConnector))]
    public class BlockConnectorInEditor : Editor {
        private BlockConnector blockConnector;

        public override void OnInspectorGUI() {
            if (blockConnector == null) {
                blockConnector = (BlockConnector) target;
            }

            if (DrawDefaultInspector()) {
                blockConnector.UpdateInfo();
            }

        }

        private void OnSceneGUI() {
            if (blockConnector == null) {
                blockConnector = (BlockConnector) target;
            }

            blockConnector.UpdateInfo();
        }
    }
}
#endif