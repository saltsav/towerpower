﻿using Assets.Scripts.Core;
using System;
using UnityEngine;

namespace Assets.Scripts {
    public class CameraLook : MonoSingleton<CameraLook> {

        [NonSerialized] public bool Enabled = true;

        public Transform pointIndicator;
        public float distanceCamera => GP.distanceCamera;
        public Vector3 offSetAngles = new Vector3(50, -90, 0);
        private Vector3 clickPoint = new Vector3(0, 0, 0);

        Transform _target;

        // При старте
        void Start() {
            SetCameraSize();
            _target = DogGO.Single.transform;
        }

        // Установка размеров камеры
        void SetCameraSize() {
            float scaledValue = (Constant.GetMainCamera.aspect - 0.5624495f) / (0.4471154f - 0.5624495f);
            scaledValue = scaledValue < 0 ? 0 : scaledValue;
            scaledValue = scaledValue > 1 ? 1 : scaledValue;
            float size = (Constant.GetMainCamera.fieldOfView + (13.3f * scaledValue));
            Constant.GetMainCamera.fieldOfView = size;
        }

        public void OnEnable() {
            UpdateManager.UpdateThis += OnUpdateFromManager;
            Constant.GetMainCamera.transform.localEulerAngles = offSetAngles;
            Enabled = true;
        }

        public void OnDisable() {
            UpdateManager.UpdateThis -= OnUpdateFromManager;
        }

        public void OnUpdateFromManager() {
            if (Enabled) {
                if (_target == null) {
                    return;
                }
                Constant.GetMainCamera.transform.position = _target.position;
                Constant.GetMainCamera.transform.Translate(Vector3.back * distanceCamera);
            }
        }
    }
}