﻿using System.Collections.Generic;
using Assets.Maze;
using Assets.Scripts;
using Assets.Scripts.Core;
using Assets.Scripts.ScriptsHerd;
using EasyButtons;
using UnityEngine;

//стороны конекта блока
public enum TypeConnectionSide {
    up,
    down,
    left,
    right
}

public class MapGenerator : MonoSingleton<MapGenerator> {
    public List<InfoOneBigBlockMap> prefabsInfoOneBigBlockMap;
    [ReadOnly] public List<InfoOneBigBlockMap> listInfoOneBigBlockMap; //лист cгенерированной карты
    public Transform parentInfoOneBigBlockMap; //парент для блоков карты
    [ReadOnly] public List<BlockMapHelp> arrayBlockMapHelp = new List<BlockMapHelp>(); //лист вспомогательных объектов для генерации карты


    public BlockMapHelp firstBlockMapHelp = new BlockMapHelp(); //первый объект карты
    public BlockMapHelp lastBlockMapHelp = new BlockMapHelp(); //последний объект карты

    public List<Bridges> listAllBridges;
    public List<BlockConnector> listAllBlockConnector;

    [Button]
    public void GenerationFullMap() {
        //Debug.Log("Длина " + PlayerInfoManager.Instance.countBlocks);
        int o = 0;
        while (true) {
            //Debug.Log("попытка " + o);
            Reset();
            GenerationMaze.Instance.GenerationBaseMaze();
            GenerationBlocksInfo();
            if (CheckMapForGoodParams()) {
                break;
            }

            o++;
        }

        CreateBlocksMap();
        SelectionBridges();
        DogView.Instance.StartFromController();
    }

    public void Reset() {
        lastBlockMapHelp.iHaveInfo = false;
        listAllBridges.Clear();
    }

    [Button]
    public void SelectionBridges() {
        for (int j = 0; j < listAllBridges.Count; j++) {
            listAllBridges[j].bridge.name = "bridge" + j;
            listAllBridges[j].index = j;
            listAllBridges[j].iUsed = false;
        }

        foreach (Bridges bridge1 in listAllBridges) {
            foreach (Bridges bridge2 in listAllBridges) {
                if (bridge1 != bridge2 && !bridge1.iUsed && !bridge2.iUsed) {
                    float deltaX = Mathf.Abs(bridge1.x) - Mathf.Abs(bridge2.x);
                    float deltaZ = Mathf.Abs(bridge1.z) - Mathf.Abs(bridge2.z);

                    if (Mathf.Abs(deltaX) < 0.1f && Mathf.Abs(deltaZ) < 10 ||
                        Mathf.Abs(deltaZ) < 0.1f && Mathf.Abs(deltaX) < 10) {
                        bridge1.SetActiveBridges(true);
                        bridge2.SetActiveBridges(false);
                        bridge1.iUsed = true;
                        bridge2.iUsed = true;
                    }
                }
            }
        }

        foreach (InfoOneBigBlockMap infoOneBigBlockMap in listInfoOneBigBlockMap) {
            infoOneBigBlockMap.OffNotUsedBridges();
        }
    }

    public bool CheckMapForGoodParams() {
        List<CellMaze> lineMazeArray = GenerationMaze.Instance.lineMazeArray;
        int deltaX = Mathf.Abs(lineMazeArray[0].x) - Mathf.Abs(lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].x);
        int deltaZ = Mathf.Abs(lineMazeArray[0].z) - Mathf.Abs(lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].z);
        if ((deltaX == 0 && Mathf.Abs(deltaZ) == 2) || (deltaZ == 0 && Mathf.Abs(deltaX) == 2)) {
            //Debug.LogError("false BAD " + lineMazeArray[0].x + " " +  lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].x + " " + lineMazeArray[0].z + " " + lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].z);
            return false;
        }
        //Debug.LogError("true BAD " + lineMazeArray[0].x + " " +  lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].x + " " + lineMazeArray[0].z + " " + lineMazeArray[PlayerInfoManager.Instance.countBlocks - 1].z);
        return true;
    }

    [Button]
    public void GenerationBlocksInfo() { //подборка блоков
        List<CellMaze> lineMazeArray = GenerationMaze.Instance.lineMazeArray;
        arrayBlockMapHelp.Clear();
        for (int p = 0; p < PlayerInfoManager.Instance.countBlocks; p++) {
            BlockMapHelp blockMapHelp = null;
            blockMapHelp = GetBlockMapHelpByNeedParams(lineMazeArray[p]);
            if (blockMapHelp == null) {
                break;
            }

            if (p == 0) { //первый и последний блок специальной формы
                blockMapHelp.typeBigBlockMap = TypeBigBlockMap.type23;
            }

            if (p == PlayerInfoManager.Instance.countBlocks - 1) { //первый и последний блок специальной формы
                blockMapHelp.typeBigBlockMap = TypeBigBlockMap.type22;
                //blockMapHelp.RotateLastBlockForBefore();
            }

            blockMapHelp.x = (lineMazeArray[p].z - 1) / 2; //пересчёт ген лабиринта в условные X Z
            blockMapHelp.z = (lineMazeArray[p].x - 1) / 2;

            if (p == 1) { //первый блок насильно разварачивается относительно второго
                arrayBlockMapHelp[0].RotateThisBlockFor(blockMapHelp.cellMaze.x, blockMapHelp.cellMaze.z);
            }

            arrayBlockMapHelp.Add(blockMapHelp);
            lastBlockMapHelp = blockMapHelp;
            lastBlockMapHelp.iHaveInfo = true;
        }
    }

    [Button]
    public void CreateBlocksMap() { //генерация блоков
        ClearAllInfoOneBigBlockMap();
        for (int n = 0; n < arrayBlockMapHelp.Count; n++) {
            if (arrayBlockMapHelp[n] != null) {
                InfoOneBigBlockMap newInfoOneBigBlockMap = GetInfoOneBigBlockMapByType(arrayBlockMapHelp[n].typeBigBlockMap);
                newInfoOneBigBlockMap.index = n;
                listInfoOneBigBlockMap.Add(newInfoOneBigBlockMap);
                newInfoOneBigBlockMap.blockMapHelp = arrayBlockMapHelp[n];
                newInfoOneBigBlockMap.RotateForCurrentAngel(arrayBlockMapHelp[n].finalRotate);
                newInfoOneBigBlockMap.transform.SetParent(parentInfoOneBigBlockMap);
                newInfoOneBigBlockMap.transform.localScale = Vector3.one;
                newInfoOneBigBlockMap.x = arrayBlockMapHelp[n].x;
                newInfoOneBigBlockMap.z = -arrayBlockMapHelp[n].z;
                newInfoOneBigBlockMap.transform.localPosition = new Vector3(newInfoOneBigBlockMap.x * 72, 0, newInfoOneBigBlockMap.z * 72);

                /*if (newInfoOneBigBlockMap.badCow != null && Random.value > 0.2f) {
                    newInfoOneBigBlockMap.badCow.SetActive(true);
                    newInfoOneBigBlockMap.badCow.GetComponent<BadCow>().enabled = true;
                }*/

                if (n == 0) {
                    DogGO.Instance.SetPosition(newInfoOneBigBlockMap.transform.position);
                }

                newInfoOneBigBlockMap.SetupBridges();
                newInfoOneBigBlockMap.OffConnectors();
            }
        }
    }

    public BlockMapHelp GetBlockMapHelpByNeedParams(CellMaze cellMaze) {
        List<BlockMapHelp> listBlockMapHelp = new List<BlockMapHelp>();
        string d = string.Empty;
        foreach (TypeConnectionSide t in cellMaze.listTypeConnectionSide) {
            d += t + " ";
        }

        //идёт перебор всех блоков вращая каждый 4 раза и стыкуя с текущим. 

        foreach (InfoOneBigBlockMap infoOneBigBlockMap in prefabsInfoOneBigBlockMap) {
            if (infoOneBigBlockMap.typeBigBlockMap == TypeBigBlockMap.type22) {
                continue;
            }

            infoOneBigBlockMap.UpdateInfoClocksArray();

            for (int i = 0; i < 4; i++) {
                List<TypeConnectionSide> connectArray = new List<TypeConnectionSide>();
                if (i == 0) {
                    connectArray = infoOneBigBlockMap.connectArray0;
                }

                if (i == 1) {
                    connectArray = infoOneBigBlockMap.connectArrayMinus90;
                }

                if (i == 2) {
                    connectArray = infoOneBigBlockMap.connectArrayMinus180;
                }

                if (i == 3) {
                    connectArray = infoOneBigBlockMap.connectArrayMinus270;
                }

                if (!lastBlockMapHelp.iHaveInfo) {
                    if (CheckConnects(cellMaze.listTypeConnectionSide, connectArray)) {
                        listBlockMapHelp.Add(
                            new BlockMapHelp {
                                infoOneBigBlockMap = infoOneBigBlockMap,
                                typeBigBlockMap = infoOneBigBlockMap.typeBigBlockMap,
                                finalRotate = i * -90,
                                listType = cellMaze.listTypeConnectionSide
                            });
                    }
                } else {
                    lastBlockMapHelp = GetBlockMapHelpByCellMazeXandY(cellMaze.beforeCellMazeX, cellMaze.beforeCellMazeZ);
                    if (lastBlockMapHelp.typeBigBlockMap != infoOneBigBlockMap.typeBigBlockMap && CheckConnects(cellMaze.listTypeConnectionSide, connectArray)) {
                        lastBlockMapHelp.infoOneBigBlockMap.UpdateInfoClocksArray();
                        TypeConnectionSide t1 = GetBeforeBlockSide(cellMaze.x, cellMaze.z, cellMaze.beforeCellMazeX, cellMaze.beforeCellMazeZ);
                        TypeConnectionSide t2 = GetBeforeBlockSide(cellMaze.beforeCellMazeX, cellMaze.beforeCellMazeZ, cellMaze.x, cellMaze.z);
                        List<int> current = lastBlockMapHelp.infoOneBigBlockMap.GetLineByAngelAndSide(lastBlockMapHelp.finalRotate, t2);
                        List<int> next = infoOneBigBlockMap.GetLineByAngelAndSide(i * -90, t1);
                        if (CheckPointOnSide(next, current)) {
                            lastBlockMapHelp.AddForUsedSideList(t2);
                            listBlockMapHelp.Add(
                                new BlockMapHelp {
                                    infoOneBigBlockMap = infoOneBigBlockMap,
                                    typeBigBlockMap = infoOneBigBlockMap.typeBigBlockMap,
                                    finalRotate = i * -90,
                                    listType = cellMaze.listTypeConnectionSide,
                                    usedSides = new List<TypeConnectionSide> { t1 }
                                });
                        }
                    }
                }
            }
        }

        if (listBlockMapHelp.Count == 0) {
            string s = string.Empty;
            foreach (TypeConnectionSide typeConnectionSide in cellMaze.listTypeConnectionSide) {
                s += typeConnectionSide + " ";
            }

            //может быть так что не найдётся подходящего блока
            Debug.LogError("GetBlockMapHelpByNeedParams NOT FOUND " + s + " lastBlockMapHelp " + lastBlockMapHelp.typeBigBlockMap + " " + lastBlockMapHelp.finalRotate + " " +
                           GetBeforeBlockSide(cellMaze.x, cellMaze.z, cellMaze.beforeCellMazeX, cellMaze.beforeCellMazeZ));
            return null;
        }

        int r = Random.Range(0, listBlockMapHelp.Count);
        listBlockMapHelp[r].cellMaze = cellMaze;
        //Debug.LogError(" ВЫБРАЛ " + r + " " + listBlockMapHelp[r].typeBigBlockMap +  " поворот = " + listBlockMapHelp[r].finalRotate + " из " + listBlockMapHelp.Count);
        return listBlockMapHelp[r];
    }

    public BlockMapHelp GetBlockMapHelpByCellMazeXandY(int x, int z) {
        foreach (BlockMapHelp blockMapHelp in arrayBlockMapHelp) {
            if (blockMapHelp.cellMaze.x == x && blockMapHelp.cellMaze.z == z) {
                return blockMapHelp;
            }
        }

        Debug.LogError("Bad GetBlockMapHelpByCellMazeXandY " + x + " " + z);
        return null;
    }

    public TypeConnectionSide GetBeforeBlockSide(int x1, int z1, int x2, int z2) {
        if (x1 == x2 && z1 == z2) {
            Debug.LogError(" ERROR GetBeforeBlockSide1");
            return TypeConnectionSide.up;
        }

        if (x1 == x2) {
            if (z1 < z2) {
                return TypeConnectionSide.right;
            }

            return TypeConnectionSide.left;
        }

        if (z1 == z2) {
            if (x1 < x2) {
                return TypeConnectionSide.down;
            }

            return TypeConnectionSide.up;
        }

        Debug.LogError(" ERROR GetBeforeBlockSide2");
        return TypeConnectionSide.up;
    }

    public string GetStrFromArray(List<int> current) {
        string d = string.Empty;
        foreach (int t in current) {
            d += t + " ";
        }

        return d;
    }

    public bool CheckPointOnSide(List<int> s1, List<int> s2) {
        for (int i = 0; i < s1.Count; i++) {
            for (int j = 0; j < s2.Count; j++) {
                if (s1[i] == s2[j]) {
                    return true;
                }
            }
        }

        return false;
    }

    public bool CheckConnects(List<TypeConnectionSide> listNeedConnects, List<TypeConnectionSide> listHaveConnects) {
        if (listNeedConnects.Count != listHaveConnects.Count) {
            return false;
        }

        bool flagBig = true;
        foreach (TypeConnectionSide needConnectionSide in listNeedConnects) {
            bool flag = false;
            foreach (TypeConnectionSide haveConnect in listHaveConnects) {
                if (haveConnect == needConnectionSide) {
                    flag = true;
                    break;
                }
            }

            if (!flag) {
                flagBig = false;
                break;
            }
        }

        return flagBig;
    }

    public InfoOneBigBlockMap GetInfoOneBigBlockMapByType(TypeBigBlockMap typeBigBlockMap) {
        GameObject go = PoolsController.Instance.GetLocalBlocksFromPoolByType(typeBigBlockMap);

        if (go == null) {
            Debug.LogError("Bad GetInfoOneBigBlockMapByType " + typeBigBlockMap);
            return null;
        }

        return go.GetComponent<InfoOneBigBlockMap>();
    }

    public InfoOneBigBlockMap GetInfoOneBigBlockMapByXandY(int x, int z) {
        foreach (InfoOneBigBlockMap infoOneBigBlockMap in listInfoOneBigBlockMap) {
            if (infoOneBigBlockMap != null && infoOneBigBlockMap.x == x && infoOneBigBlockMap.z == z) {
                return infoOneBigBlockMap;
            }
        }

        return null;
    }

    [Button]
    public void ClearAllInfoOneBigBlockMap() {
        foreach (InfoOneBigBlockMap infoOneBigBlockMap in listInfoOneBigBlockMap) {
            if (infoOneBigBlockMap != null) {
                Destroy(infoOneBigBlockMap.gameObject);
            }
        }

        listInfoOneBigBlockMap.Clear();
    }


    // Поиски невидимой точки в зависимости от позиции собаки
    public InfoOneBigBlockMap GetNearestSleepPoint(Vector3 animalPosition) {

        float dis = float.MaxValue;
        int index = -1;
        int reserveIndex = -1;

        float dogToStart = 0;
        float dogToFinish = 0;

        for (int i = 0; i < listInfoOneBigBlockMap.Count; i++) {
            if (!listInfoOneBigBlockMap[i].isVisible) {

                float newDis = MapManager.Single.GetDistanceBetweenForMap(listInfoOneBigBlockMap[i].transform.position, animalPosition);
                float disDog = MapManager.Single.GetDistanceBetweenForMap(listInfoOneBigBlockMap[i].transform.position, DogGO.Single.transform.position);

                // Сохранение дистанции до финиша и старта
                if (i == 0) dogToStart = disDog;
                if (i == listInfoOneBigBlockMap.Count - 1) dogToFinish = disDog;

                // Поиск точки которая от собаки
                if (newDis < dis && newDis < disDog) {
                    index = i;
                    dis = newDis;
                } else {

                    // Если есть хоть один невидимый и нет от собаки, то гоним к нему 
                    reserveIndex = i;
                }
            }
        }

        // Гоним либо к невидимому через собаку, либо в противоположный край
        if (index == -1) {
            if (reserveIndex == -1) {
                index = dogToStart > dogToFinish ? 0 : listInfoOneBigBlockMap.Count - 1;
            } else {
                index = reserveIndex;
            }
        }

        return listInfoOneBigBlockMap[index];
    }
}