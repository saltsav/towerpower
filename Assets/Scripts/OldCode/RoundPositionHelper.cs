﻿using UnityEngine;

namespace Assets.Core.ScriptsHerd {
    public class RoundPositionHelper : MonoBehaviour {
        public bool useRoundY;
        public bool useRound;
        public int round = 4;

        public void SetRoundPosition() {
            if (!useRound) {
                return;
            }

            transform.localPosition = new Vector3(GetRound(transform.localPosition.x, round), useRoundY ? GetRound(transform.localPosition.y, 1) :transform.localPosition.y, GetRound(transform.localPosition.z, round));
        }

        public float GetRound(float x, float r) {
            return (int) (x / r) * r;
        }
    }
}