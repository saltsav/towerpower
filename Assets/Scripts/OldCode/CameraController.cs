﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoSingleton<CameraController> {

    [NonSerialized] public bool Enabled = true;
    Transform _target;
    Vector3 _offset;
    Vector3 _newPosition;

    // При старте
    void Start() {
        UpdateManager.LateUpdateThis += LateUpdateThis;
        _target = DogGO.Single.TargetToCamera.transform;
        SetCameraSize();

        // Начальная позиция
        transform.position = _target.position;
        Constant.GetMainCamera.transform.Translate(Vector3.back * GP.distanceCamera);

        // Разница
        _offset = transform.position - _target.transform.position;
        Enabled = false;
    }

    // Установка размеров камеры под экраны
    void SetCameraSize() {
        float scaledValue = (Constant.GetMainCamera.aspect - 0.5624495f) / (0.4471154f - 0.5624495f);
        scaledValue = scaledValue < 0 ? 0 : scaledValue;
        scaledValue = scaledValue > 1 ? 1 : scaledValue;
        float size = (Constant.GetMainCamera.fieldOfView + (13.3f * scaledValue));
        Constant.GetMainCamera.fieldOfView = size;
    }

    // Обновление позиции
    public void LateUpdateThis() {
        if (Enabled && _target != null) {

            _newPosition = _target.transform.position + _offset;

            // Небольшая задержка для плавного эффекта
            transform.position = Vector3.Lerp(transform.position, _newPosition, Time.deltaTime * 14);
        }
    }

    //
    protected override void OnDestroy() {
        UpdateManager.LateUpdateThis -= LateUpdateThis;
        base.OnDestroy();
    }
}
