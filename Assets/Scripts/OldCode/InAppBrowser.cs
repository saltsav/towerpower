﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class InAppBrowser {
#if UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern void OpenBrowserNative(string url);
#endif

	public static void OpenBrowser(string url) {


		if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IPHONE
			OpenBrowserNative(url);
#endif
		} else {
			Application.OpenURL(url);
		}
	}
}
