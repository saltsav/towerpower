﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
	public class OptionsController : MonoSingleton<OptionsController> {
		public Button btnOpenOptions;
		public Button btnReset;

		public bool optionsIsOpen;

		public GameObject panel;

		public Button btnChangeTypeMove;
		public bool typeMove;
		public Text textTypeMove;

		public Button btnShowStick;
		public bool typeShowStick;
		public Text textStick;

		public Button btnShowSphere;

		private bool _typeShowSphere;

		public bool typeShowSphere {
			get { return _typeShowSphere; }
			set {
				_typeShowSphere = value;
				if (actionShowSphere != null) {
					actionShowSphere(value);
				}
			}
		}

		public Text textSphere;
		public Action<bool> actionShowSphere;

		public void Start() {
			btnReset.onClick.RemoveAllListeners();
			btnReset.onClick.AddListener(() => {
			    PlayerInfoManager.Instance.SaveCoinCount(0);
				//GameManager.Single.RestartGame();
			});
			panel.SetActive(true);
			panel.SetActive(optionsIsOpen);
			btnOpenOptions.onClick.RemoveAllListeners();
			btnOpenOptions.onClick.AddListener(() => {
				optionsIsOpen = !optionsIsOpen;
				panel.SetActive(optionsIsOpen);
			});

			btnChangeTypeMove.onClick.RemoveAllListeners();
			btnChangeTypeMove.onClick.AddListener(() => {
				typeMove = !typeMove;
				if (typeMove) {
					textTypeMove.text = "Сменить тип управления (1)";
				} else {
					textTypeMove.text = "Сменить тип управления (2)";
				}
				PlayerPrefs.SetInt("typeMove", typeMove ? 1 : 0);
			});
			typeMove = PlayerPrefs.GetInt("typeMove", 0) != 0;
			if (typeMove) {
				textTypeMove.text = "Сменить тип управления (1)";
			} else {
				textTypeMove.text = "Сменить тип управления (2)";
			}

			btnShowStick.onClick.RemoveAllListeners();
			btnShowStick.onClick.AddListener(() => {
				typeShowStick = !typeShowStick;
				if (typeShowStick) {
					textStick.text = "Отображать стик (Да)";
				} else {
					textStick.text = "Отображать стик (Нет)";
				}
				PlayerPrefs.SetInt("typeShowStick", typeShowStick ? 1 : 0);
			});
			typeShowStick = PlayerPrefs.GetInt("typeShowStick", 0) != 0;
			if (typeShowStick) {
				textStick.text = "Отображать стик (Да)";
			} else {
				textStick.text = "Отображать стик (Нет)";
			}




			btnShowSphere.onClick.RemoveAllListeners();
			btnShowSphere.onClick.AddListener(() => {
				typeShowSphere = !typeShowSphere;
				if (typeShowSphere) {
					textSphere.text = "Отображать сферы (Да)";
				} else {
					textSphere.text = "Отображать сферы (Нет)";
				}
				PlayerPrefs.SetInt("typeShowSphere", typeShowSphere ? 1 : 0);
			});
			typeShowSphere = PlayerPrefs.GetInt("typeShowSphere", 0) != 0;
			if (typeShowSphere) {
				textSphere.text = "Отображать сферы (Да)";
			} else {
				textSphere.text = "Отображать сферы (Нет)";
			}
		}
	}
}