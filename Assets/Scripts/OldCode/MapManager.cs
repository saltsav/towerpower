﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Assets.Core.ScriptsHerd;
using Assets.Scripts;
using Assets.Scripts.ScriptsHerd;

public struct MapEdgePosition {
    public float EdgeX_Min;
    public float EdgeX_Max;
    public float EdgeZ_Min;
    public float EdgeZ_Max;
}

public class MapManager : MonoSingleton<MapManager> {
    //public GameObject map1;
    //public GameObject map2;
    [SerializeField] public NavMeshSurface NavMeshSurface;

    RaycastHit[] _hitFloor;
    LayerMask _layer;
    private NavMeshPath _path;
    private NavMeshHit _nawHit;

    public void Awake() {
        _path = new NavMeshPath();
    }


    public void StartLevel() {
        CreateAllEntity();
        GoodAnimalCountManager.Single.SetCount();
    }

    // Отмена обновления меша
    public void StopUpdateNavMesh() {
        CancelInvoke();
    }

    // Создание NavMesh
    public void BuildNavMeshMap() {
        NavMeshSurface.BuildNavMesh();
    }

    // Обновление NavMesh
    public void UpdateNavMeshMap() {
        NavMeshSurface.UpdateNavMeshSync(NavMeshSurface.navMeshData);
    }

    public void GenerationMap() {
        MapGenerator.Instance.GenerationFullMap();
        _hitFloor = new RaycastHit[4];
        _layer = LayerMask.GetMask("floor");
    }

    //создание овец и монеток
    public void CreateAllEntity() {
        OffOnForNawMesh.Off();
        BuildNavMeshMap();
        CoinControllers.Instance.GenerationCoins(true);
        //EnemyController.Instance.GenerationEnemy(true);
        UpdateNavMeshMap();
        GoodAnimalManager.Single.CreateSheep(true);

        //EnemyController.Instance.GenerationEnemy(false);
        CoinControllers.Instance.GenerationCoins(false);
        GoodAnimalManager.Single.CreateSheep(false);

        OffOnForNawMesh.On();
        UpdateNavMeshMap();
    }

    // Проверка пола
    public bool CheckFloorUnderPoint(Vector3 pos, string checkTag = "") {
        if (Physics.BoxCastNonAlloc(pos, Vector3.one, Vector3.down, _hitFloor, Quaternion.identity, 5f, _layer) > 0) {
            if (checkTag == "" || _hitFloor[0].transform.gameObject.tag == checkTag) {
                return true;
            }
        }
        return false;
    }

    // Получение расстояния к между двумя точками по карте
    public float GetDistanceBetweenForMap(Vector3 pos1, Vector3 pos2) {
        bool isPath = NavMesh.CalculatePath(pos1, pos2, NavMesh.AllAreas, _path);
        float distance = float.MaxValue;
        if (isPath) {
            distance = 0;
            for (int i = 0; i < _path.corners.Length - 1; i++) {
                distance += Vector3.Distance(_path.corners[i], _path.corners[i + 1]);
            }
        }

        return distance;
    }

    // Рандомная позиция на NavMesh
    int _count;
    Mesh _mesh;
    public Vector3 RandomNavmeshLocation(int mask) {

        // NavMesh в Mesh
        if (_mesh == null) {
            NavMeshTriangulation triangulatedNavMesh = NavMesh.CalculateTriangulation();
            _mesh = new Mesh();
            _mesh.vertices = triangulatedNavMesh.vertices;

            NavMeshSurface.voxelSize = 0.6f;
            NavMeshSurface.tileSize = 30;
        }

        Vector3 point1 = _mesh.vertices[UnityEngine.Random.Range(0, _mesh.vertexCount)];
        Vector3 randomDirection = point1;

        //// Рандомная позиция
        Vector3 finalPosition = Vector3.zero;

        // Поиск края
        if (NavMesh.SamplePosition(randomDirection, out _nawHit, 540f, mask)) {
            finalPosition = _nawHit.position;
            finalPosition.y = 0;

            // Проверка рядом края
            if (NavMesh.FindClosestEdge(finalPosition, out _nawHit, mask)) {

                // Отступ от края
                if (_nawHit.distance < 1.5f && _count < 10) {
                    _count++;
                    finalPosition = RandomNavmeshLocation(mask);
                }
                _count = 0;
            }
        }

        return finalPosition;
    }


    // Позиция к краю
    public Vector3 GetEdgePosition(Vector3 pos) {
        if (NavMesh.FindClosestEdge(pos, out _nawHit, 1)) {
            return _nawHit.position;
        }
        return Vector3.zero;
    }


    // Позиция на NavMesh
    public Vector3 PositionOnNavMesh(Vector3 pos) {

        Vector3 finalPosition = Vector3.zero;

        if (NavMesh.SamplePosition(pos, out _nawHit, 10, 1)) {
            finalPosition = _nawHit.position;
        }

        return finalPosition;
    }


}
