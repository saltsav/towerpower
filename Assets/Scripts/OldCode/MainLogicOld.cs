﻿/*using Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts {
    public enum StateGame {
        none,
        mainMenu,
        play,
        pause,
        result,
        completed,
        fail

    }
    public class MainLogic : MonoSingleton<MainLogic> {

        public StateGame CurrentStateGame = StateGame.none;

        public StateGame GetStateGame() {
            return CurrentStateGame;
        }

        public void SetStateGame(StateGame newStateGame) {
            CurrentStateGame = newStateGame;
        }

        public void Awake() {
            MapManager.Instance.GenerationMap();
            SetStateGame(StateGame.mainMenu);
            MapManager.Instance.StartLevel();
            GUIFadeBack.Instance.SetDisable();
            GUIStartModal.Instance.StartFromController(false);
        }


        public void StartGame() {
            CoinCountManager.Instance.StartFromController();
            GUIUpStars.Instance.StartFromController();
            //GUIStartModal.Instance.StartFromController(true);
            GUIMainMenu.Single.HiddenMenu();
            SetStateGame(StateGame.play);
            CameraController.Single.Enabled = true;
        }

        public void ShowContinueModal() {
            SetStateGame(StateGame.pause);
            DogGO.Instance.MainRigidbody.isKinematic = true;
            GUIUpStars.Instance.SetActiveViewBlockStars(false);
            GUIFadeBack.Instance.SetEnable();
            GUIContinueModal.Instance.PlayerIsDeath();
        }

        public void ResurectDog() {
            DogGO.Instance.MainRigidbody.isKinematic = false;
            DogGO.Instance.ResurectDog();
            GUIUpStars.Instance.SetActiveViewBlockStars(true);
            GUIFadeBack.Instance.SetDisable();
            GUIContinueModal.Instance.ResumeGame();
            SetStateGame(StateGame.play);
        }

        [EasyButtons.Button]
        public void ResetGame() {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        [EasyButtons.Button]
        public void ClearAllPlayerPrefs() {
            PlayerPrefs.DeleteAll();
        }

        public void Update() {
            if (Input.GetKeyUp(KeyCode.Z)) {
                ResetGame();
            }
        }
    }
}*/