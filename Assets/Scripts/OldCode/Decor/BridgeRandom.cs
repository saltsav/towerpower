﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeRandom : MonoBehaviour {

    [SerializeField] GameObject[] Bridges;

    private void OnEnable() {
        foreach (GameObject go in Bridges) go.SetActive(false);
        Bridges[Random.Range(0, Bridges.Length)].SetActive(true);
    }
}
