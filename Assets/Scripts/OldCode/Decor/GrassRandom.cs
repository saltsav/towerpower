﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassRandom : MonoBehaviour {

    [SerializeField] GameObject[] Grasses;
    [SerializeField] bool isRandom = true;
    [SerializeField] int PercentShow = 70;

    private void OnEnable() {

        if (Random.Range(0, 101) < PercentShow) {
            transform.GetChild(0).gameObject.SetActive(true);

            if (isRandom) {
                foreach (GameObject go in Grasses) go.SetActive(false);

                int num = Random.Range(0, Grasses.Length);
                Grasses[num].SetActive(true);

                // Рандомная ротация
                Vector3 newRotate = Grasses[num].transform.localEulerAngles;
                newRotate.y = Random.Range(0, 360);
                Grasses[num].transform.localEulerAngles = newRotate;
            }
        } else {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
