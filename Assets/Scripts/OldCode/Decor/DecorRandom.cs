﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorRandom : MonoBehaviour {

    [SerializeField] GameObject[] Decors;
    [SerializeField] int PercentShow = 70;

    private void OnEnable() {

        // Рандом, показывать или нет
        if (Random.Range(0, 101) < PercentShow) {

            // Включение
            transform.GetChild(0).gameObject.SetActive(true);

            // Отключение всех декоров
            foreach (GameObject go in Decors) go.SetActive(false);

            // Выбор рандомного префаба из массива
            int num = Random.Range(0, Decors.Length);
            Decors[num].SetActive(true);

            // Рандомная ротация
            Vector3 newRotate = Decors[num].transform.localEulerAngles;
            newRotate.y = Random.Range(0, 360);
            Decors[num].transform.localEulerAngles = newRotate;

        } else {

            // Выключение
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
