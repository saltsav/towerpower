﻿using System.Collections.Generic;
using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts.ScriptsHerd {
    public class EnemyControllerOld : MonoSingleton<EnemyControllerOld> {
        public List<FoxGO> listFox;

        public void GenerationEnemy(bool isStart) {
            if (isStart) {
                for (int i = 0; i < PlayerInfoManager.Instance.countBadAnimal; i++) {

                    GameObject goFox = PoolsController.Instance.poolFox.Spawn(MapManager.Single.RandomNavmeshLocation(1), Quaternion.identity, GoodAnimalManager.Instance.parentAnimal);
                    FoxGO fox = goFox.GetComponent<FoxGO>();
                    fox.transform.position = new Vector3(fox.transform.position.x, 2f, fox.transform.position.z);
                    listFox.Add(fox);

                    // Рандомный поворот
                    Vector3 rotate = Vector3.zero;
                    rotate.y = Random.Range(0, 360);
                    fox.transform.localEulerAngles = rotate;

                    MapManager.Single.UpdateNavMeshMap();
                }
            } else {
                foreach (FoxGO fox in listFox) fox.SetStart();
            }
        }

        public void DogAttack() {
            foreach (FoxGO foxGo in listFox) {
                if (foxGo != null && foxGo.stateFox != StateFox.runAway && foxGo.stateFox != StateFox.despawn) {
                    float disMap = MapManager.Instance.GetDistanceBetweenForMap(DogGO.Instance.transform.position, foxGo.transform.position);
                    float disLine = Vector3.Distance(DogGO.Instance.transform.position, foxGo.transform.position);
                    if (disMap * 0.5 < disLine && disLine < GP.distanceAgroDog) {

                        //foxGo.FearIcon.ShowIconWarning();
                        foxGo.SetStateFox(StateFox.runAway);
                    }
                }
            }
        }
    }
}