﻿using DPPoolObject;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolsCheck : MonoBehaviour {
    static bool _isAdd;

    [SerializeField] GameObject Pools;

    // Провека пула на добавление
    void Awake() {
        if (!_isAdd) {
            _isAdd = true;
            Instantiate(Pools);
        }
    }
}
