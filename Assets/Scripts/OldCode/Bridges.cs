﻿using System;
using UnityEngine;

namespace Assets.Scripts.ScriptsHerd {
    [Serializable]
    public class Bridges {
        private InfoOneBigBlockMap _infoOneBigBlockMap;
        public int index;
        public GameObject bridge;
        public GameObject blockEarth;

        [ReadOnly] public bool iUsed;

        [ReadOnly] public float x;
        [ReadOnly] public float z;

        public void Setup(InfoOneBigBlockMap infoOneBigBlockMap) {
            _infoOneBigBlockMap = infoOneBigBlockMap;
            x = bridge.transform.position.x;
            z = bridge.transform.position.z;
            MapGenerator.Instance.listAllBridges.Add(this);
            SetActiveBridges(true);
        }

        public void SetActiveBridges(bool var) {
            bridge.SetActive(var);
            blockEarth.SetActive(!var);
            
        }

        public void OffAll() {
            if (bridge != null) {
                bridge.SetActive(false);
                blockEarth.SetActive(false);
            }
            
        }
    }
}