﻿using System;
using UnityEngine;

namespace Assets.Scripts.Core {
    public enum StateInApplication {
        none,
        mainMenu,
        setting,
        dragToStart,
        game,
        pause,
        continueGame,
        tapThemAll,
        result,
        modalNotMoney,
        modalNotInternet,
        modalExit,
        mobalNoAds
    }

    public class AndroidBackButton : MonoSingleton<AndroidBackButton>{
        public StateInApplication currentStateInApplication = StateInApplication.none;

        private float currentTime = 0.5f; // время для задержки между нажатиями
        private readonly float time = 0.5f;

        public void SetStateInApplication(StateInApplication stateInApplication) {
            currentStateInApplication = stateInApplication;
            //Debug.Log(" currentStateInApplication " + currentStateInApplication);
        }

#if UNITY_ANDROID
        public void OnEnable() {
            UpdateManager.UpdateThis += UpdateFromManager;
        }

        public void OnDisable() {
            UpdateManager.UpdateThis -= UpdateFromManager;
        }
#endif
        public void UpdateFromManager() {
            if (currentTime > 0) {
                currentTime -= Time.deltaTime;
                return;
            }

            currentTime = 0;

            if (Input.GetKeyUp(KeyCode.Escape)) {
                currentTime = time;
                StateInApplication stateInApplication = currentStateInApplication;
                SetStateInApplication(StateInApplication.none);
                switch (stateInApplication) {
                    case StateInApplication.none:
                        break;
                    case StateInApplication.mainMenu:
                        //GUIExitGame.Instance.SetActiveViewBlock(true);
                        break;
                    case StateInApplication.setting:
                        GUIOptions.Instance.btnClose.onClick.Invoke();
                        break;
                    case StateInApplication.dragToStart:
                        //GUIUpStars.inst.btnPause.onClick.Invoke();//назад при состоянии "палец на экране"
                        break;
                    case StateInApplication.game:
                        //GUIUpStars.Instance.btnPause.onClick.Invoke();
                        break;
                    case StateInApplication.pause:
                        //GUIModalPause.Instance.btnYes.onClick.Invoke();
                        break;
                    case StateInApplication.continueGame:
                        //GUIContinueModal.Instance.btnNoThanks.onClick.Invoke();
                        break;
                    case StateInApplication.tapThemAll:
                        //GameView.Instance.TextTapButton.onClick.Invoke();
                        break;
                    case StateInApplication.result:
                        //GUIResultWindow.Instance.btnGoMainMenu.onClick.Invoke();
                        break;
                    case StateInApplication.modalNotMoney:
                        //GUINotMoney.Instance.btnClose.onClick.Invoke();
                        break;
                    case StateInApplication.modalNotInternet:
                        //GUIBadInternet.Instance.btnClose.onClick.Invoke();
                        break;
                    case StateInApplication.modalExit:
                        //GUIExitGame.Instance.btnNo.onClick.Invoke();
                        break;
                    case StateInApplication.mobalNoAds:
                       // GUINoAds.Instance.btnYes.onClick.Invoke();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        // Код выхода из игры:
        public void FullExit() {
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
        }
    }
}