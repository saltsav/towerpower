﻿using Assets.Core.ScriptsHerd;
using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts {
	public class MapController : MonoSingleton<MapController> {
		public Transform mapTransform;

		[ReadOnly] public int currentScale;

		[ReadOnly] public bool mapIsCreate;

		public void Start() {
			currentScale = GP.scaleMap;
			mapTransform.localScale = new Vector3((float) currentScale / 10, 1, (float) currentScale / 10);
			mapIsCreate = true;
			//FieldController.Instance.CalculationDis();
		}

		public void Update() {
			if (OptionsController.Instance.optionsIsOpen) {
				return;
			}

			if (currentScale != GP.scaleMap) {
				OptionsController.Instance.btnReset.onClick.Invoke();
			}
		}
	}
}