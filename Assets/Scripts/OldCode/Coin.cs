﻿using Assets.Scripts.Core;
using Assets.Scripts.ScriptsHerd;
using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts {
    public class Coin : MonoBehaviour {

        [SerializeField] GameObject CollectEffect;
        [SerializeField] VisibleTrigger Trigger;

        private float _speedRotate = 90;
        public GameObject NotSpawnArea;
        public bool iTaked;

        Vector3 _startLocalScale;

        // При старте
        private void Awake() {
            Trigger.ActionVisible += VisibleStatus;
        }

        // Винно или нет
        private void VisibleStatus(bool isVisible) {
            if (isVisible) {
                UpdateManager.UpdateThis += OnUpdateFromManager;
            } else {
                UpdateManager.UpdateThis -= OnUpdateFromManager;
            }
        }

        public void OnEnable() {
            _startLocalScale = transform.localScale;
        }

        public void OnDisable() {
            iTaked = false;
            _speedRotate = 90;
            transform.localScale = _startLocalScale;
            NotSpawnArea.SetActive(true);
            CollectEffect.SetActive(false);
        }

        // Начальные настройки
        public void SetStart() {
            NotSpawnArea.SetActive(false);
        }

        public void OnUpdateFromManager() {
            transform.localEulerAngles += Vector3.up * Time.deltaTime * _speedRotate;
        }
        public void OnTriggerEnter(Collider otherCollider) {
            if (iTaked) {
                return;
            }

            if (otherCollider.gameObject.name == "DistanceVolume") {
                CoinControllers.Instance.TakeThisCoin(1);
                Hide();
            }
        }

        [EasyButtons.Button]
        public void Hide() {
            iTaked = true;
            _speedRotate *= 8;
            SoundController.Single.PlaySound(SoundsList.Money, 0.25f);

            ActivateCollectEffect();
            transform.DOLocalMove(transform.localPosition + Vector3.up * 7, 0.45f).SetEase(Ease.OutSine);
            DOTween.Sequence()
                .AppendInterval(0.15f)
                .Append(transform.DOScale(Vector3.zero, 0.3f))
                .AppendInterval(2f)
                .AppendCallback(() => {
                    PoolsController.Instance.poolCoins.Despawn(gameObject);
                });
        }

        void ActivateCollectEffect() {
            CollectEffect.SetActive(true);
            DOVirtual.DelayedCall(2, () => { CollectEffect.SetActive(false); });
        }
    }
}