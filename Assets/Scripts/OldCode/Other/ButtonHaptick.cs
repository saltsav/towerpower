﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHaptick : MonoBehaviour, IPointerDownHandler {

	public void OnPointerDown(PointerEventData evd) {
		HapticController.UseHaptic(HapticList.ImpactLight);
	}
}
