﻿using UnityEngine;
using UnityEngine.UI;
using TapticPlugin;
using Assets.Scripts.Core;

public enum HapticList {
	NotificationSuccess,
	NotificationWarning,
	NotificationError,
	ImpactLight,
	ImpactMedium,
	ImpactHeavy,
	Selection,
	ImpactHeavyWithoutVibro
}

public static class HapticController {

	// Включение тактика
	public static void UseHaptic(HapticList feedback) {

		if (GUIOptions.isEnableHaptic) {

			if (TapticManager.IsSupport()) {
				switch (feedback) {
					case HapticList.NotificationSuccess:
						TapticManager.Notification(NotificationFeedback.Success);
						break;
					case HapticList.NotificationWarning:
						TapticManager.Notification(NotificationFeedback.Warning);
						break;
					case HapticList.NotificationError:
						TapticManager.Notification(NotificationFeedback.Error);
						break;
					case HapticList.ImpactLight:
						TapticManager.Impact(ImpactFeedback.Light);
						break;
					case HapticList.ImpactMedium:
						TapticManager.Impact(ImpactFeedback.Medium);
						break;

					case HapticList.ImpactHeavyWithoutVibro:
					case HapticList.ImpactHeavy:
						TapticManager.Impact(ImpactFeedback.Heavy);
						break;
					case HapticList.Selection:
						TapticManager.Selection();
						break;
				}
			} else {
				switch (feedback) {
					case HapticList.NotificationError:
					case HapticList.ImpactHeavy:
						Handheld.Vibrate();
						break;
				}
			}
		}
	}
}