﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameSettings", menuName = "Settings/GameSettings", order = 0)]
public class GameSettings : ScriptableObject {
    [Header("Урон башен")]
    public float towerPower;
 
    [Header("Скорострельность башен")]
    public float speedAttack;
    
}
