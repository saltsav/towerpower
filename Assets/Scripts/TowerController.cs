﻿using System.Collections.Generic;

namespace Assets.Scripts {
    public class TowerController : MonoSingleton<TowerController> {
        public List<Tower> towerList;
        
        public void UpdateAll() {
            foreach (Tower tower in towerList) {
                tower.UpdateEnemyList();
            }
        }
    }
}