﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts {
    public class TestClass  : MonoBehaviour {
        public Rigidbody rigidbody;
        public Transform tr;
        public Vector3 v3;

        public void Start() {
            rigidbody = GetComponent<Rigidbody>();
        }

       /* public void Update() {

            v3 = (transform.position - tr.position);
            transform.eulerAngles = v3 ;
            //transform.DORotateQuaternion(Quaternion.Euler(tr.position - transform.position), 0.5f);
        }*/
       public void FixedUpdate() {
           rigidbody.AddForce(Physics.gravity * 10);
       }
    }
}