﻿namespace Assets.Scripts {
    public class MainLogic : MonoSingleton<MainLogic> {
        public GameSettings gs;

        public void Start() {
            PathController.Instance.Setup();

            EnemyController.Instance.Setup();
        }
    }
}